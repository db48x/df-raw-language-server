use colored::*;
use df_ls_structure::DFRaw;
use lsp_types::Diagnostic;

/// Check the source of errors and warnings
pub fn check_source(source: &[u8], debug: bool) -> Vec<Diagnostic> {
    use std::time::Instant;
    let now = Instant::now();
    // Convert source from CP437 to UTF-8
    let source_bytes = df_cp437::convert_cp437_to_utf8(source);
    let source = String::from_utf8(source_bytes).expect("Non UTF-8 characters found");
    // 1: Do Lexical Analysis (Tokenizer)
    print_in_hline("Start Lexical Analysis", debug);
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(&source);
    if debug {
        df_ls_lexical_analysis::print_ast(&tree, &source);
        println!("Diagnostics Lexer: {:#?}", diagnostic_list_lexer);
    }
    print_in_hline("End Lexical Analysis", debug);
    // 2: Do Syntax Analysis
    print_in_hline("Start Syntax Analysis", debug);
    let (structure, diagnostic_list_syntax): (DFRaw, Vec<Diagnostic>) =
        df_ls_syntax_analysis::do_syntax_analysis(&tree, &source);
    if debug {
        println!("\nStruct: {:#?}\n", structure);
        println!("Diagnostics syntax: {:#?}", diagnostic_list_syntax);
        df_ls_syntax_analysis::print_source_with_diagnostics(&source, &diagnostic_list_syntax);
    }
    print_in_hline("End Syntax Analysis", debug);

    // Disabled for initial release
    // 3: Do Semantic Analysis
    // print_in_hline("Start Semantic Analysis", debug);
    // let diagnostic_list_semantics = df_ls_semantic_analysis::do_semantic_analysis(&structure);
    // if debug {
    //     println!("Diagnostics semantics: {:#?}", diagnostic_list_semantics);
    // }
    // print_in_hline("End Semantic Analysis", debug);

    log::info!("This took: {} millisec", now.elapsed().as_millis());
    vec![
        diagnostic_list_lexer,
        diagnostic_list_syntax,
        // diagnostic_list_semantics,
    ]
    .concat()
}

fn print_in_hline(text: &str, debug: bool) {
    if debug {
        println!(
            "{}",
            format!("-------------------{}-------------------", text).bright_cyan()
        );
    }
}
#[cfg(test)]
mod tests {
    use super::*;
    use crate::simple_logger;
    use log::LevelFilter;

    #[test]
    fn test_basic_df_raw() {
        use pretty_assertions::assert_eq;
        // Setup logger and log level
        log::set_logger(&simple_logger::LOGGER).unwrap();
        log::set_max_level(LevelFilter::Trace);

        let source = "creature_domestic

        [OBJECT:CREATURE]
        
        [CREATURE:DOG]
            [CASTE:FEMALE]
                [FEMALE]
            [CASTE:MALE]
                [MALE]
            [SELECT_CASTE:ALL]
                [NATURAL]
        ";
        let diagnostic_list = check_source(source.as_bytes(), true);
        println!("Diagnostics: {:#?}", diagnostic_list);
        assert_eq!(diagnostic_list, vec![]);
    }

    #[test]
    fn test_basic_cp437_df_raw() {
        use pretty_assertions::assert_eq;
        let source = b"descriptor_color_standard

        [OBJECT:DESCRIPTOR_COLOR]
        
        Simple test\x9ctest
        
        [COLOR:AMBER]
            [NAME:ambe\x8cr]
            [RGB:255:191:0]
            [WORD:AMBER]
        ";
        let diagnostic_list = check_source(source, true);
        println!("Diagnostics: {:#?}", diagnostic_list);
        assert_eq!(diagnostic_list, vec![]);
    }
}
