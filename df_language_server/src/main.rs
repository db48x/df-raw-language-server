#![forbid(unsafe_code)]
#![deny(clippy::all)]

mod check_source;
mod combined_logger;
mod server;
mod simple_logger;

use check_source::check_source;
use log::LevelFilter;
use structopt::StructOpt;

/// The available command line parameters.
#[derive(StructOpt, Debug)]
#[structopt(
    name = "df_language_server",
    about = "A language server for Dwarf Fortress RAW files."
)]
struct Opts {
    /// Activates debug mode
    #[structopt(short, long)]
    debug: bool,

    /// Activates quiet mode, no log message in std out
    #[structopt(short, long)]
    quiet: bool,

    /// Use simple logger
    ///
    /// This logger only logs to std_out/err, not to a file.
    #[structopt(long)]
    simple_logger: bool,

    /// Verbose mode (-v, -vv)
    #[structopt(short, long, parse(from_occurrences))]
    verbose: u8,

    /// All available subcommand
    #[structopt(subcommand)]
    cmd: Commands,
}

/// All available subcommands in DF Language server.
#[derive(StructOpt, Debug)]
enum Commands {
    /// Start Language server
    Start {
        /// Specify the port to start the server.
        ///
        /// The default debug port is `2087`.
        /// If no port is specified it will use an available port requested from the OS.
        #[structopt(short, long)]
        port: Option<u16>,
    },
    /// Parse Debug file
    // #[cfg(debug_assertions)]
    Debug {},
}

#[tokio::main]
async fn main() {
    let opts = Opts::from_args();
    // Get log settings
    let log_filter: LevelFilter = if opts.debug {
        if opts.verbose >= 2 {
            LevelFilter::Trace
        } else {
            LevelFilter::Debug
        }
    } else if opts.quiet {
        LevelFilter::Off
    } else {
        match opts.verbose {
            0 => LevelFilter::Info,
            1 => LevelFilter::Debug,
            _ => LevelFilter::Trace,
        }
    };
    // Setup logger and log level
    if opts.simple_logger {
        log::set_logger(&simple_logger::LOGGER).unwrap();
        log::set_max_level(log_filter);
    } else {
        combined_logger::setup_logger(log_filter);
    }
    log::trace!("Command arguments: {:#?}", opts);

    log_system_info();

    // Check what subcommand we want to run
    match opts.cmd {
        Commands::Start { port } => {
            log::info!("Start Language server");
            server::start_language_server(port, opts.debug).await;
        }
        Commands::Debug {} => {
            log::info!("Running Debug");
            // Load source code from file
            let filename = "debug-test.txt";
            let source_code = std::fs::read(filename).expect("Could not read test file");
            check_source::check_source(&source_code, opts.debug);
        }
    }
    log::info!("Stopping application.");
}

fn log_system_info() {
    // Log basic info
    log::info!("---- System Info ----");
    // Example: `Bin: df_language_server: v0.2.0`
    log::info!(
        "Bin: {}: v{}",
        env!("CARGO_PKG_NAME"),
        env!("CARGO_PKG_VERSION")
    );
    // Example: `OS: linux`
    log::info!("OS: {}", std::env::consts::OS);
    // Example: `OS Arch: x86_64`
    log::info!("OS Arch: {}", std::env::consts::ARCH);
    log::info!("---------------------");
}
