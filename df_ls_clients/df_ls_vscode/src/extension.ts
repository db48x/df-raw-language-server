"use strict";

import * as net from "net";
import * as path from "path";
import * as os from "os";
import { ChildProcess, execFile } from "child_process";
import { ExtensionContext, workspace } from "vscode";
import { LanguageClient, LanguageClientOptions, ServerOptions } from "vscode-languageclient/node";

let client: LanguageClient | undefined;

function getClientOptions(): LanguageClientOptions {
    return {
        // Register the server for plain text documents
        documentSelector: [
            { scheme: "file", language: "df_raw" },
            { scheme: "untitled", language: "df_raw" },
        ],
        outputChannelName: "DF Raw Language Server",
        synchronize: {
            // Notify the server about file changes to '.clientrc files contain in the workspace
            fileEvents: workspace.createFileSystemWatcher("**/.clientrc"),
        },
    };
}

function getServerOptions(): ServerOptions {
    // Describe how to start the server, it will not be started here, but later
    // The `Promise` is is the prospect of what will be have started.
    // So the code inside to promise can be executed more then once after
    // the extension was activated.
    const serverOptions: ServerOptions = () => {
        return new Promise((resolve, reject) => {
            console.log("Starting Language server");
            // Start Language Server and get the port from it.
            if (!isStartedInDebugMode()) {
                // Start Language server as child process
                let server: ChildProcess | undefined = execFile(
                    get_executable_path(), ['start']
                );
                if (server.stdout === null) {
                    console.log("Could not start language server.");
                    return;
                }
                let connected_to_server = false;
                // Stream stdout data
                server.stdout.on('data', function (data) {
                    console.log(data);
                    // Check if already connected to server to prevent checking additional output
                    if (!connected_to_server) {
                        // Look for matching string and get port from string
                        const regex = /<\[SERVER available on port: `([0-9]+)`\]>/;
                        const match_found = data.match(regex);
                        if (match_found !== null && match_found.length == 2) {
                            // Parse port from string to int
                            const port = parseInt(match_found[1], 10);
                            console.log('Port found: ' + port);
                            // Start client part of the extension to connect to the server
                            const clientSocket = new net.Socket();
                            clientSocket.connect(port, "127.0.0.1", () => {
                                resolve({
                                    reader: clientSocket,
                                    writer: clientSocket,
                                    detached: true,
                                });
                            });
                            connected_to_server = true;
                        }
                    }
                });
            } else {
                // Use fixed port (`2087`) for debugging and do not start server in here
                // This functions expects you to be running
                // the server already separate on your system.
                const clientSocket = new net.Socket();
                clientSocket.connect(2087, "127.0.0.1", () => {
                    resolve({
                        reader: clientSocket,
                        writer: clientSocket,
                    });
                });
            }
        });
    };
    return serverOptions;
}

function isStartedInDebugMode(): boolean {
    return process.env.VSCODE_DEBUG_MODE === "true";
}

function get_executable_path(): string {
    let executable_file: string = '';
    switch (os.platform()) {
        case 'linux':
            executable_file = 'df_language_server-x86_64-unknown-linux-gnu';
            break;
        case 'win32':
            executable_file = 'df_language_server-x86_64-pc-windows-gnu.exe';
            break;
        case 'darwin':
            executable_file = 'df_language_server-x86_64-apple-darwin';
            break;
        default:
            console.log("This platform is not yet supported by us.");
            process.exit(1);
            break;
    }
    let executable_path: string = path.join(__dirname, '../', 'out', executable_file);
    return executable_path;
}

function startLangServer() {
    // Start whole language server client (may include starting server too)
    const client_option = getClientOptions();
    const server_option = getServerOptions();
    client = new LanguageClient(`df-raw-ls-vscode`, 'Dwarf Fortress RAW LS', server_option, client_option);
    // Start the client. This will also launch the server
    client.start();
    // context.subscriptions.push(client.start());
}


export function activate(context: ExtensionContext) {
    startLangServer();
}

export function deactivate(): Thenable<void> | undefined {
    if (!client) {
        return undefined;
    }
    // The client will shutdown the server too (using messaging)
    return client.stop();
}
