# Release Checklist

## Pre-Merge

- [ ] Commit all open changes to repo and GitLab.
- [ ] Check the milestone for any open changes and resolve them or move them to the next version.
- [ ] Commit all change to `dev` branch.
- [ ] Check for rust updates: `rustup update`.
- [ ] Update Docker container using CI pipeline from last commit.
- [ ] Check `cargo +nightly udeps` for unused packages.
- [ ] Run `cargo update` to check for minor/patch updates.
- [ ] Check `cargo outdated` for outdated packages and major updates.
- - [ ] If package where updated make sure minor/patch updates are also updated again.
- [ ] Check `cargo deny check` for security issues.
- [ ] A manual quick check if everything works as expected.
- [ ] Run `cargo clean`.
- [ ] Run `cargo clippy`.
- [ ] Run `cargo fmt`.
- [ ] Run `cargo test`, all tests should be correct.
- [ ] Clear GitLab runner caches.
- [ ] Check for files that should no longer be in repo.
- [ ] Update version number in:
- - [ ] Use `./update_version.sh` to update files.
- [ ] Create a new entry in and update date:
- - [ ] `CHANGELOG.md`
- [ ] Commit the version number changes to GitRepo.

## Merge
- [ ] Merge the `dev` branch into `master`.
- [ ] Make sure CI starts running.
- - [ ] Start LS builds (Linux, Windows, MacOS)
- - [ ] Start Client builds (VS Code, ...)
- [ ] Create git Tag: `vX.X.X` (As Release).

## Publish
- [ ] Update clients in there respective stores:
- - [ ] VS Code:
- - - [ ] Download `.vsix` file from CI.
- - - [ ] Update to marketplace.

## Post-Publish
- [ ] Create a post in Discord `#release` channel.
- [ ] Create message on DF Forums.
- [ ] Create post on Reddit.