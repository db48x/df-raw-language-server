use super::super::{
    argument_to_token_name, Argument, Token, TokenValue, TryFromArgument, TryFromArgumentGroup,
};
use super::{LoopControl, TokenDeserialize};
use df_ls_core::{ReferenceTo, Referenceable};
use df_ls_diagnostics::{hash_map, DMExtraInfo, DiagnosticsInfo};
use df_ls_lexical_analysis::TreeCursor;

/// Deserialize a token with following pattern: `[REF:REF]`
impl<T: Referenceable + Default> TokenDeserialize for ReferenceTo<T> {
    fn deserialize_tokens(
        cursor: &mut TreeCursor,
        source: &str,
        diagnostics: &mut DiagnosticsInfo,
    ) -> Result<Box<Self>, ()> {
        // Get arguments from token
        let mut token = Token::deserialize_tokens(cursor, source, diagnostics)?;
        Token::consume_token(cursor)?;
        // Arg 0
        token.check_token_arg0(source, diagnostics, true)?;
        token.consume_argument();
        // Arg 1
        let result = Self::try_from_argument_group(&mut token, source, diagnostics, true);
        if result.is_ok() {
            token.check_all_arg_consumed(source, diagnostics, true)?;
        }
        Ok(Box::new(result?))
    }

    fn deserialize_general_token(
        _cursor: &mut TreeCursor,
        _source: &str,
        _diagnostics: &mut DiagnosticsInfo,
        new_self: Box<Self>,
    ) -> (LoopControl, Box<Self>) {
        (LoopControl::DoNothing, new_self)
    }

    fn get_vec_loopcontrol() -> LoopControl {
        LoopControl::DoNothing
    }

    fn get_allowed_tokens() -> Option<Vec<TokenValue>> {
        None
    }
}

// ------------------------- Convert a group of arguments to Self -----------------------

impl<T: Referenceable> TryFromArgumentGroup for ReferenceTo<T> {
    fn try_from_argument_group(
        token: &mut Token,
        source: &str,
        diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        token.check_as_required_argument(source, diagnostics, add_diagnostics_on_err)?;
        let arg = token.get_current_arg_opt();
        let result = Self::try_from_argument(arg, source, diagnostics, add_diagnostics_on_err);
        token.consume_argument();
        result
    }
}

// -------------------------Convert one argument to Self -----------------------

impl<T: Referenceable> TryFromArgument for ReferenceTo<T> {
    fn try_from_argument(
        arg_opt: Option<&Argument>,
        _source: &str,
        diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        if let Some(arg) = arg_opt {
            match &arg.value {
                TokenValue::TVReference(v) => Ok(ReferenceTo::new(v.clone())),
                TokenValue::TVString(v) => {
                    if add_diagnostics_on_err {
                        diagnostics.add_message(
                            DMExtraInfo::new(arg.node.get_range()),
                            "reference_is_string",
                        );
                    }
                    Ok(ReferenceTo::new(v.clone()))
                }
                _ => {
                    if add_diagnostics_on_err {
                        diagnostics.add_message(
                            DMExtraInfo {
                                range: arg.node.get_range(),
                                message_template_data: hash_map! {
                                    "expected_parameters" => Self::expected_argument_types(),
                                    "found_parameters" => argument_to_token_name(&arg.value),
                                },
                            },
                            "wrong_arg_type",
                        );
                    }
                    Err(())
                }
            }
        } else {
            Err(())
        }
    }

    fn expected_argument_types() -> String {
        format!("Reference to {}", T::get_ref_type())
    }
}

// -------------------------Convert from TokenValue -----------------------

impl<T: Referenceable> From<ReferenceTo<T>> for TokenValue {
    fn from(item: ReferenceTo<T>) -> TokenValue {
        TokenValue::TVReference(item.0)
    }
}

impl<T: Referenceable> From<Option<ReferenceTo<T>>> for TokenValue {
    fn from(item: Option<ReferenceTo<T>>) -> TokenValue {
        match item {
            Some(v) => TokenValue::TVReference(v.0),
            None => TokenValue::TVEmpty,
        }
    }
}
