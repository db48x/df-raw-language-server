use super::super::{TokenDeserializeBasics, TokenValue};
use super::{LoopControl, TokenDeserialize};
use df_ls_diagnostics::DiagnosticsInfo;
use df_ls_lexical_analysis::TreeCursor;

impl TokenDeserialize for TokenValue {
    fn deserialize_tokens(
        cursor: &mut TreeCursor,
        source: &str,
        diagnostics: &mut DiagnosticsInfo,
    ) -> Result<Box<Self>, ()> {
        let mut token_value = TokenValue::default();
        let node = cursor.node();
        match node.kind().as_ref() {
            "token_value_integer" => {
                let value = TokenDeserializeBasics::deserialize_tokens(cursor, source, diagnostics);
                if let Ok(value) = value {
                    token_value = TokenValue::TVInteger(value);
                }
                // Else message should be already added to diagnostics
            }
            "token_value_character" => {
                token_value = TokenValue::TVCharacter(TokenDeserializeBasics::deserialize_tokens(
                    cursor,
                    source,
                    diagnostics,
                )?);
            }
            "token_value_string" => {
                token_value = TokenValue::TVString(TokenDeserializeBasics::deserialize_tokens(
                    cursor,
                    source,
                    diagnostics,
                )?);
            }
            "token_value_reference" => {
                token_value = TokenValue::TVReference(TokenDeserializeBasics::deserialize_tokens(
                    cursor,
                    source,
                    diagnostics,
                )?);
            }
            "token_value_empty" => {
                token_value = TokenValue::TVEmpty;
            }
            "EOF" => {}
            others => {
                log::error!("Found an unknown node of kind: {}", others);
                debug_assert!(false, "Found an unknown node of kind: {}", others);
            }
        }
        Ok(Box::new(token_value))
    }

    fn deserialize_general_token(
        _cursor: &mut TreeCursor,
        _source: &str,
        _diagnostics: &mut DiagnosticsInfo,
        new_self: Box<Self>,
    ) -> (LoopControl, Box<Self>) {
        (LoopControl::DoNothing, new_self)
    }

    fn get_allowed_tokens() -> Option<Vec<TokenValue>> {
        None
    }
}
