mod token;
mod token_deserialize;
mod token_deserialize_basic;
mod token_deserialize_unary_token;
mod token_value;
mod try_from_argument;
mod try_from_argument_group;

#[cfg(test)]
pub mod test_common;

pub use df_ls_diagnostics::{DMExtraInfo, DiagnosticsInfo};
pub use token::{Argument, Token};
pub use token_deserialize::{LoopControl, TokenDeserialize};
pub use token_deserialize_basic::TokenDeserializeBasics;
pub use token_value::*;
pub use try_from_argument::TryFromArgument;
pub use try_from_argument_group::TryFromArgumentGroup;

// TODO Remove: See `TokenValue`
pub fn argument_to_token_name(argument: &TokenValue) -> String {
    match argument {
        TokenValue::TVInteger(_) => "Integer".to_string(),
        TokenValue::TVCharacter(_) => "char".to_string(),
        TokenValue::TVString(_) => "String".to_string(),
        TokenValue::TVReference(_) => "Reference".to_string(),
        TokenValue::TVEmpty => "Empty".to_string(),
    }
}
