mod common;

use common::*;
use df_ls_core::ReferenceTo;
use df_ls_diagnostics::lsp_types::*;
use df_ls_syntax_analysis::*;
use pretty_assertions::assert_eq;

#[test]
fn on_duplicate_error() {
    let source = "
    [MAIN:TYPE2]

    [BIPEDAL:MONSTER]
        [NAME:test]
        [DEAD]
        [DEAD]
    ";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list_lexer,
        vec![Range {
            start: Position {
                line: 0,
                character: 0,
            },
            end: Position {
                line: 0,
                character: 0,
            },
        }],
    );
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            type_2: vec![Type2Token::Bipedal(HumanToken {
                reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                name: Some("test".to_owned()),
                death: Some(()),
                ..Default::default()
            })],
            ..Default::default()
        }
    );
    common::assert_diagnostic_codes(&diagnostic_list, vec!["duplicate_token_error".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![Range {
            start: Position {
                line: 6,
                character: 8,
            },
            end: Position {
                line: 6,
                character: 14,
            },
        }],
    );
}
