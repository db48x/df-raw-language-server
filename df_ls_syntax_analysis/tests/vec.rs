mod common;

use common::*;
use df_ls_core::Reference;
use df_ls_diagnostics::lsp_types::*;
use df_ls_diagnostics::DiagnosticsInfo;
use df_ls_syntax_analysis::*;
use pretty_assertions::assert_eq;

#[test]
fn test_vec_derive_struct() {
    let source = "header
        [NAME:some string]
        [NAME:some other string]
        [NAME:short]";
    // Parse Source to AST
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    println!("Lexer: {:#?}", diagnostic_list_lexer);
    assert_eq!(diagnostic_list_lexer, vec![]);
    let mut tree_cursor = tree.walk();
    // go to "header"
    tree_cursor.goto_first_child();
    // go to "comment"
    tree_cursor.goto_next_sibling();
    // go to "general_token"
    tree_cursor.goto_next_sibling();
    let mut diagnostic_info = DiagnosticsInfo::default();
    println!("{}", tree.root_node().to_sexp(0));

    // Deserialize the AST
    let test1: RefToken =
        *TokenDeserialize::deserialize_tokens(&mut tree_cursor, source, &mut diagnostic_info)
            .expect("Failed to deserialize token");
    assert_eq!(
        test1,
        RefToken {
            reference: None,
            name: vec![
                "some string".to_owned(),
                "some other string".to_owned(),
                "short".to_owned()
            ],
        }
    );

    assert_eq!(diagnostic_info.diagnostics, vec![]);
}

#[test]
fn test_vec_derive_struct_mistypes_ref() {
    let source = "header
        [NAME:some string]
        [REF:SOMEREF]
        [NAME:some more strings]
        [NAMEB:some other string]
        [NAME:short]
        [NAME:a bit longer]
        [NAME:also some (2) numbers 5]";
    // Parse Source to AST
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    println!("Lexer: {:#?}", diagnostic_list_lexer);
    assert_eq!(diagnostic_list_lexer, vec![]);
    let mut tree_cursor = tree.walk();
    // go to "header"
    tree_cursor.goto_first_child();
    // go to "comment"
    tree_cursor.goto_next_sibling();
    // go to "general_token"
    tree_cursor.goto_next_sibling();
    let mut diagnostic_info = DiagnosticsInfo::default();
    println!("{}", tree.root_node().to_sexp(0));

    // Deserialize the AST
    let test1: RefToken =
        *TokenDeserialize::deserialize_tokens(&mut tree_cursor, source, &mut diagnostic_info)
            .expect("Failed to deserialize token");
    assert_eq!(
        test1,
        RefToken {
            reference: Some(Reference("SOMEREF".to_owned())),
            name: vec!["some string".to_owned(), "some more strings".to_owned()],
        }
    );

    // The `[NAMEB:some other string]` has not been deserialized yet
    let test2: String =
        *TokenDeserialize::deserialize_tokens(&mut tree_cursor, source, &mut diagnostic_info)
            .expect("Failed to deserialize token");
    assert_eq!(test2, "some other string".to_owned());

    // skip "comment"
    tree_cursor.goto_next_sibling();
    // Continue deserialize a vec
    let test3: RefToken =
        *TokenDeserialize::deserialize_tokens(&mut tree_cursor, source, &mut diagnostic_info)
            .expect("Failed to deserialize token");
    assert_eq!(
        test3,
        RefToken {
            reference: None,
            name: vec![
                "short".to_owned(),
                "a bit longer".to_owned(),
                "also some (2) numbers 5".to_owned(),
            ],
        }
    );

    assert_eq!(diagnostic_info.diagnostics, vec![]);
}

#[test]
fn test_vec_derive_struct_with_error() {
    let source = "header
        [NAME:some string]
        [NAME:some other string]
        [NAME:short]
        [NAME:REF]
    ";
    // Parse Source to AST
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    println!("Lexer: {:#?}", diagnostic_list_lexer);
    assert_eq!(diagnostic_list_lexer, vec![]);
    let mut tree_cursor = tree.walk();
    // go to "header"
    tree_cursor.goto_first_child();
    // go to "comment"
    tree_cursor.goto_next_sibling();
    // go to "general_token"
    tree_cursor.goto_next_sibling();
    let mut diagnostic_info = DiagnosticsInfo::default();
    println!("{}", tree.root_node().to_sexp(0));

    // Deserialize the AST
    let test1: RefToken =
        *TokenDeserialize::deserialize_tokens(&mut tree_cursor, source, &mut diagnostic_info)
            .expect("Failed to deserialize token");

    println!("{:#?}", test1);
    println!("{:#?}", diagnostic_info.diagnostics);
    assert_eq!(
        test1,
        RefToken {
            reference: None,
            name: vec![
                "some string".to_owned(),
                "some other string".to_owned(),
                "short".to_owned()
            ],
        }
    );
    common::assert_diagnostic_codes(
        &diagnostic_info.diagnostics,
        vec!["wrong_arg_type".to_owned()],
    );
    common::assert_diagnostic_range(
        &diagnostic_info.diagnostics,
        vec![Range {
            start: Position {
                line: 4,
                character: 14,
            },
            end: Position {
                line: 4,
                character: 17,
            },
        }],
    );
}
