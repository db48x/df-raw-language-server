mod common;

use common::*;
use df_ls_core::Reference;
use df_ls_diagnostics::lsp_types::*;
use df_ls_syntax_analysis::*;
use pretty_assertions::assert_eq;

#[test]
fn error_in_token_ref() {
    let source = "
    [MAIN:TYPE1]

    [TYPE1:DOG]
        [ITE[M:T1]";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(
        &diagnostic_list_lexer,
        vec![
            "missing_header".to_owned(),
            "missing_end_bracket".to_owned(),
        ],
    );
    common::assert_diagnostic_range(
        &diagnostic_list_lexer,
        vec![
            Range {
                start: Position {
                    line: 0,
                    character: 0,
                },
                end: Position {
                    line: 0,
                    character: 0,
                },
            },
            Range {
                start: Position {
                    line: 4,
                    character: 12,
                },
                end: Position {
                    line: 4,
                    character: 12,
                },
            },
        ],
    );
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            type_1: vec![Type1Token {
                reference: Some(Reference("DOG".to_owned())),
                ..Default::default()
            }],
            ..Default::default()
        }
    );
    common::assert_diagnostic_codes(
        &diagnostic_list,
        vec!["unknown_token".to_owned(), "unchecked_code".to_owned()],
    );
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![
            Range {
                start: Position {
                    line: 4,
                    character: 8,
                },
                end: Position {
                    line: 4,
                    character: 12,
                },
            },
            Range {
                start: Position {
                    line: 4,
                    character: 12,
                },
                end: Position {
                    line: 4,
                    character: 18,
                },
            },
        ],
    );
}
