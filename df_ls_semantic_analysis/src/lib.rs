#![forbid(unsafe_code)]
#![deny(clippy::all)]

mod object_collection;

use df_ls_diagnostics::{DiagnosticMessageSet, DiagnosticsInfo};
use df_ls_structure::*;
use lsp_types::Diagnostic;
use object_collection::create_object_collection;

/// Do Semantic Analysis over the created structure from Syntax Analysis.
/// It returns a list of Diagnostic messages.
pub fn do_semantic_analysis(structure: &DFRaw) -> Vec<Diagnostic> {
    let mut diagnostic_info = DiagnosticsInfo::load_from_file(
        DiagnosticMessageSet::Semantic,
        Some("DF RAW Language Server".to_owned()),
    );

    // Create set of all objects in workspace and mark duplicates
    let _workspace_object_collection = create_object_collection(structure, &mut diagnostic_info);
    // Check all object references for validity

    // Check most objects/nesting tokens for presence of required child tokens.
    // If it doesn't have any specific required child tokens, check if it has ANY tokens or is just empty

    // Special per object checks:
    // in some cases duplicates of second arguments (usually references) should be warned about
    //=========================================================
    // Creature:
    // Required tokens (like BODY) have to be defined in each caste, or in the `ALL` caste once
    // duplicate BODY warning when defining in `ALL` caste AND in other castes (might apply to other tokens too)
    // duplicate `[CASTE:DUPLICATED_CASTE]`, you get the idea
    // stuff like [REMOVE_MATERIAL:HAIR] should only work if the [BODY_DETAIL_PLAN:ID] has HAIR in the ID
    // pay close attention to BODY stuff in general
    // [SELECT_*:...] must refer to a real previously mentioned/imported thing
    // COPY_TAGS_FROM, GO_TO_START and other similar tokens
    // Applying creature varations, make sure it works sanely (don't be too overzealous; they're meant to be broadly applicable)
    //=========================================================
    // Creature Variation:
    // !ARG1, !ARG2 etc; this weird syntax may need to be dealt with specially
    // Check that each token it's trying to remove, add or modify is a real token name
    // Handling token arguments/names/parameters will be tricky; look at the attack tag examples
    //=========================================================
    // Descriptor Pattern:
    // Requires PATTERN token
    // Check which arg is used for `PATTERN`, and change how many CP_COLOR are needed accordingly
    //=========================================================
    // Descriptor Shape:
    // NAME and TILE required
    //=========================================================
    // Language
    // SYMBOL requires S_WORD
    // TRANSLATION requires T_WORD
    //=========================================================

    vec![] //diagnostic_info.diagnostics
}

// TODO this might be better to put in a different file,
// and perhaps call it inside each object specific checking function
// fn check_reference_validity(
//     structure: &DFRaw,
//     workspace_object_collection: WorkspaceObjectCollection,
//     mut diagnostics: &mut DiagnosticsInfo,
// ) {

// }
