#!/bin/sh

# Install https://crates.io/crates/afl
# Need to be build with same rust version as it is running
# cargo install --force afl

# Build
# cargo afl build

# Create folder
FOLDERIN="in"
mkdir $FOLDERIN
cp -r start_in/* $FOLDERIN

# Build
cargo afl build
# Run
AFL_I_DONT_CARE_ABOUT_MISSING_CRASHES=1 AFL_SKIP_CPUFREQ=1 cargo afl fuzz -i $FOLDERIN -o out ./target/debug/afl_fuzz
