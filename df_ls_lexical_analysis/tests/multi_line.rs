mod common;

use df_ls_diagnostics::lsp_types::*;
use pretty_assertions::assert_eq;

#[test]
fn test_multi_line_1() {
    let source = "header
    [REF
    ]
    ";
    let (tree, diagnostic_list) = df_ls_lexical_analysis::do_lexical_analysis(source);
    println!("{}", tree.root_node().to_sexp(0));
    println!("Diagnostics: {:#?}", diagnostic_list);
    assert_eq!(
        tree.root_node().to_sexp(0),
        "(raw_file
  (header)
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (])
  )
  (comment)
  (comment)
  (EOF)
)
"
    );
    common::assert_diagnostic_codes(
        &diagnostic_list,
        vec![
            "missing_end_bracket".to_owned(),
            "unexpected_end_bracket".to_owned(),
        ],
    );
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![
            Range {
                start: Position {
                    line: 1,
                    character: 8,
                },
                end: Position {
                    line: 1,
                    character: 8,
                },
            },
            Range {
                start: Position {
                    line: 2,
                    character: 4,
                },
                end: Position {
                    line: 2,
                    character: 5,
                },
            },
        ],
    );
}

#[test]
fn test_multi_line_2() {
    let source = "header
    [REF:
    ]
    ";
    let (tree, diagnostic_list) = df_ls_lexical_analysis::do_lexical_analysis(source);
    println!("{}", tree.root_node().to_sexp(0));
    println!("Diagnostics: {:#?}", diagnostic_list);
    assert_eq!(
        tree.root_node().to_sexp(0),
        "(raw_file
  (header)
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_empty)
    )
    (])
  )
  (comment)
  (comment)
  (EOF)
)
"
    );
    common::assert_diagnostic_codes(
        &diagnostic_list,
        vec![
            "missing_end_bracket".to_owned(),
            "unexpected_end_bracket".to_owned(),
        ],
    );
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![
            Range {
                start: Position {
                    line: 1,
                    character: 9,
                },
                end: Position {
                    line: 1,
                    character: 9,
                },
            },
            Range {
                start: Position {
                    line: 2,
                    character: 4,
                },
                end: Position {
                    line: 2,
                    character: 5,
                },
            },
        ],
    );
}

#[test]
fn test_multi_line_3() {
    let source = "header
    [REF:string
    ]
    ";
    let (tree, diagnostic_list) = df_ls_lexical_analysis::do_lexical_analysis(source);
    println!("{}", tree.root_node().to_sexp(0));
    println!("Diagnostics: {:#?}", diagnostic_list);
    assert_eq!(
        tree.root_node().to_sexp(0),
        "(raw_file
  (header)
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_string)
    )
    (])
  )
  (comment)
  (comment)
  (EOF)
)
"
    );
    common::assert_diagnostic_codes(
        &diagnostic_list,
        vec![
            "missing_end_bracket".to_owned(),
            "unexpected_end_bracket".to_owned(),
        ],
    );
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![
            Range {
                start: Position {
                    line: 1,
                    character: 15,
                },
                end: Position {
                    line: 1,
                    character: 15,
                },
            },
            Range {
                start: Position {
                    line: 2,
                    character: 4,
                },
                end: Position {
                    line: 2,
                    character: 5,
                },
            },
        ],
    );
}

#[test]
fn test_multi_line_4() {
    let source = "header
    [REF:string
    [
    ]
    ";
    let (tree, diagnostic_list) = df_ls_lexical_analysis::do_lexical_analysis(source);
    println!("{}", tree.root_node().to_sexp(0));
    println!("Diagnostics: {:#?}", diagnostic_list);
    assert_eq!(
        tree.root_node().to_sexp(0),
        "(raw_file
  (header)
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_string)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (])
  )
  (comment)
  (comment)
  (EOF)
)
"
    );
    common::assert_diagnostic_codes(
        &diagnostic_list,
        vec![
            "missing_end_bracket".to_owned(),
            "missing_token_name".to_owned(),
            "missing_end_bracket".to_owned(),
            "unexpected_end_bracket".to_owned(),
        ],
    );
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![
            Range {
                start: Position {
                    line: 1,
                    character: 15,
                },
                end: Position {
                    line: 1,
                    character: 15,
                },
            },
            Range {
                start: Position {
                    line: 2,
                    character: 5,
                },
                end: Position {
                    line: 2,
                    character: 5,
                },
            },
            Range {
                start: Position {
                    line: 2,
                    character: 5,
                },
                end: Position {
                    line: 2,
                    character: 5,
                },
            },
            Range {
                start: Position {
                    line: 3,
                    character: 4,
                },
                end: Position {
                    line: 3,
                    character: 5,
                },
            },
        ],
    );
}

#[test]
fn test_multi_line_5() {
    let source = "header
[REF:string]
[REF][POP:9]
[TEST]
    ";
    let (tree, diagnostic_list) = df_ls_lexical_analysis::do_lexical_analysis(source);
    println!("{}", tree.root_node().to_sexp(0));
    println!("Diagnostics: {:#?}", diagnostic_list);
    assert_eq!(
        tree.root_node().to_sexp(0),
        "(raw_file
  (header)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_string)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (])
  )
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_integer)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (])
  )
  (comment)
  (EOF)
)
"
    );
    common::assert_diagnostic_codes(&diagnostic_list, vec![]);
    common::assert_diagnostic_range(&diagnostic_list, vec![]);
}
#[test]
fn test_multi_line_6() {
    let source = "header
    [REF:string][REF][POP:9]
    ";
    let (tree, diagnostic_list) = df_ls_lexical_analysis::do_lexical_analysis(source);
    println!("{}", tree.root_node().to_sexp(0));
    println!("Diagnostics: {:#?}", diagnostic_list);
    assert_eq!(
        tree.root_node().to_sexp(0),
        "(raw_file
  (header)
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_string)
    )
    (])
  )
  (general_token
    ([)
    (token_value_reference)
    (])
  )
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_integer)
    )
    (])
  )
  (comment)
  (EOF)
)
"
    );
    common::assert_diagnostic_codes(&diagnostic_list, vec![]);
    common::assert_diagnostic_range(&diagnostic_list, vec![]);
}
