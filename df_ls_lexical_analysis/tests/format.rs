mod common;

use df_ls_diagnostics::lsp_types::*;
use pretty_assertions::assert_eq;

#[test]
fn test_format_code() {
    let source = "creature_ocean_new

    [CREATURE:CRAB_MAN]
    
    Missing start bracket
    AMPHIBIOUS]

    Missing end bracket
    [COPY_TAGS_FROM:CRAB

    Missing first part of token
    [:A DF Raw Language Server developer with the head and pincers of a crab.]

    Missing token
    []

    First part of token has to be of type 'token_value_reference'
    [crab man:crab men:crab man]
    [100]
    ['c']

    Should not be able to spread tokens across multiple lines (probably)
    THIS HAS NOT BEEN IMPLEMENTED YET, SO RIGHT NOW IT'S JUST SEEN AS A NORMAL TOKEN
    [
        DESCRIPTION
        :This and above DESCRIPTION should be considered comments.
    ]
    ";
    let (tree, diagnostic_list) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let formatted_code =
        df_ls_lexical_analysis::format::format_code(tree.root_node(), source, false);
    println!("{}", formatted_code);
    println!("Diagnostics: {:#?}", diagnostic_list);
    assert_eq!(
        formatted_code,
        "creature_ocean_new
[CREATURE:CRAB_MAN]
Missing start bracket AMPHIBIOUS
Missing end bracket
[COPY_TAGS_FROM:CRAB]
Missing first part of token
[:A DF Raw Language Server developer with the head and pincers of a crab.]
Missing token
[]
First part of token has to be of type 'token_value_reference'
[]
[]
[]
Should not be able to spread tokens across multiple lines (probably) THIS HAS NOT BEEN IMPLEMENTED YET, SO RIGHT NOW IT'S JUST SEEN AS A NORMAL TOKEN
[]
DESCRIPTION :This and above DESCRIPTION should be considered comments.
".to_owned()
    );
    common::assert_diagnostic_codes(
        &diagnostic_list,
        vec![
            "unexpected_end_bracket".to_owned(),
            "missing_end_bracket".to_owned(),
            "missing_token_name".to_owned(),
            "missing_token_name".to_owned(),
            "missing_token_name".to_owned(),
            "unexpected_characters".to_owned(),
            "missing_token_name".to_owned(),
            "unexpected_characters".to_owned(),
            "missing_token_name".to_owned(),
            "unexpected_characters".to_owned(),
            "missing_token_name".to_owned(),
            "missing_end_bracket".to_owned(),
            "unexpected_end_bracket".to_owned(),
        ],
    );
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![
            Range {
                start: Position {
                    line: 5,
                    character: 14,
                },
                end: Position {
                    line: 5,
                    character: 15,
                },
            },
            Range {
                start: Position {
                    line: 8,
                    character: 24,
                },
                end: Position {
                    line: 8,
                    character: 24,
                },
            },
            Range {
                start: Position {
                    line: 11,
                    character: 5,
                },
                end: Position {
                    line: 11,
                    character: 5,
                },
            },
            Range {
                start: Position {
                    line: 14,
                    character: 5,
                },
                end: Position {
                    line: 14,
                    character: 5,
                },
            },
            Range {
                start: Position {
                    line: 17,
                    character: 5,
                },
                end: Position {
                    line: 17,
                    character: 5,
                },
            },
            Range {
                start: Position {
                    line: 17,
                    character: 5,
                },
                end: Position {
                    line: 17,
                    character: 31,
                },
            },
            Range {
                start: Position {
                    line: 18,
                    character: 5,
                },
                end: Position {
                    line: 18,
                    character: 5,
                },
            },
            Range {
                start: Position {
                    line: 18,
                    character: 5,
                },
                end: Position {
                    line: 18,
                    character: 8,
                },
            },
            Range {
                start: Position {
                    line: 19,
                    character: 5,
                },
                end: Position {
                    line: 19,
                    character: 5,
                },
            },
            Range {
                start: Position {
                    line: 19,
                    character: 5,
                },
                end: Position {
                    line: 19,
                    character: 8,
                },
            },
            Range {
                start: Position {
                    line: 23,
                    character: 5,
                },
                end: Position {
                    line: 23,
                    character: 5,
                },
            },
            Range {
                start: Position {
                    line: 23,
                    character: 5,
                },
                end: Position {
                    line: 23,
                    character: 5,
                },
            },
            Range {
                start: Position {
                    line: 26,
                    character: 4,
                },
                end: Position {
                    line: 26,
                    character: 5,
                },
            },
        ],
    );
}

#[test]
fn test_format_tree() {
    let source = "creature_ocean_new

    [CREATURE:CRAB_MAN]
    
    Missing start bracket
    AMPHIBIOUS]

    Missing end bracket
    [COPY_TAGS_FROM:CRAB

    Missing first part of token
    [:A DF Raw Language Server developer with the head and pincers of a crab.]

    Missing token
    []

    First part of token has to be of type 'token_value_reference'
    [crab man:crab men:crab man]
    [100]
    ['c']

    Should not be able to spread tokens across multiple lines (probably)
    THIS HAS NOT BEEN IMPLEMENTED YET, SO RIGHT NOW IT'S JUST SEEN AS A NORMAL TOKEN
    [
        DESCRIPTION
        :This and above DESCRIPTION should be considered comments.
    ]
    ";
    let (tree, diagnostic_list) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let formatted_tree =
        df_ls_lexical_analysis::format::format_tree(tree.root_node(), source, false);
    println!("{}", formatted_tree);
    println!("Diagnostics: {:#?}", diagnostic_list);
    assert_eq!(
        formatted_tree,
        "(raw_file
  (header: \"creature_ocean_new\")
  (comment)
  (general_token
    ([)
    (token_value_reference: \"CREATURE\")
    (general_token_values
      (:)
      (token_value_reference: \"CRAB_MAN\")
    )
    (])
  )
  (comment)
  (comment)
  (general_token
    ([)
    (token_value_reference: \"COPY_TAGS_FROM\")
    (general_token_values
      (:)
      (token_value_reference: \"CRAB\")
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference: \"\")
    (general_token_values
      (:)
      (token_value_string: \"A DF Raw Language Server developer with the head and pincers of a crab.\")
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference: \"\")
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference: \"\")
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference: \"\")
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference: \"\")
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference: \"\")
    (])
  )
  (comment)
  (comment)
  (EOF: \"\")
)
".to_owned()
    );
    common::assert_diagnostic_codes(
        &diagnostic_list,
        vec![
            "unexpected_end_bracket".to_owned(),
            "missing_end_bracket".to_owned(),
            "missing_token_name".to_owned(),
            "missing_token_name".to_owned(),
            "missing_token_name".to_owned(),
            "unexpected_characters".to_owned(),
            "missing_token_name".to_owned(),
            "unexpected_characters".to_owned(),
            "missing_token_name".to_owned(),
            "unexpected_characters".to_owned(),
            "missing_token_name".to_owned(),
            "missing_end_bracket".to_owned(),
            "unexpected_end_bracket".to_owned(),
        ],
    );
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![
            Range {
                start: Position {
                    line: 5,
                    character: 14,
                },
                end: Position {
                    line: 5,
                    character: 15,
                },
            },
            Range {
                start: Position {
                    line: 8,
                    character: 24,
                },
                end: Position {
                    line: 8,
                    character: 24,
                },
            },
            Range {
                start: Position {
                    line: 11,
                    character: 5,
                },
                end: Position {
                    line: 11,
                    character: 5,
                },
            },
            Range {
                start: Position {
                    line: 14,
                    character: 5,
                },
                end: Position {
                    line: 14,
                    character: 5,
                },
            },
            Range {
                start: Position {
                    line: 17,
                    character: 5,
                },
                end: Position {
                    line: 17,
                    character: 5,
                },
            },
            Range {
                start: Position {
                    line: 17,
                    character: 5,
                },
                end: Position {
                    line: 17,
                    character: 31,
                },
            },
            Range {
                start: Position {
                    line: 18,
                    character: 5,
                },
                end: Position {
                    line: 18,
                    character: 5,
                },
            },
            Range {
                start: Position {
                    line: 18,
                    character: 5,
                },
                end: Position {
                    line: 18,
                    character: 8,
                },
            },
            Range {
                start: Position {
                    line: 19,
                    character: 5,
                },
                end: Position {
                    line: 19,
                    character: 5,
                },
            },
            Range {
                start: Position {
                    line: 19,
                    character: 5,
                },
                end: Position {
                    line: 19,
                    character: 8,
                },
            },
            Range {
                start: Position {
                    line: 23,
                    character: 5,
                },
                end: Position {
                    line: 23,
                    character: 5,
                },
            },
            Range {
                start: Position {
                    line: 23,
                    character: 5,
                },
                end: Position {
                    line: 23,
                    character: 5,
                },
            },
            Range {
                start: Position {
                    line: 26,
                    character: 4,
                },
                end: Position {
                    line: 26,
                    character: 5,
                },
            },
        ],
    );
}
