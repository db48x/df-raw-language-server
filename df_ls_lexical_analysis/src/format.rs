use super::{Node, TreeCursor};
use colored::*;

// Format settings
static TAB_WIDTH: usize = 2;
/// Print input code correctly (indentation, no comments)
static PRINT_FORMATTED_CODE: bool = true;
/// Show comments in Formatted code (might trow things off)
static SHOW_COMMENTS: bool = true;
static REPLACE_COMMENT_WHITESPACE: bool = true;
/// Add S-expression to output
static PRINT_S_EXP: bool = true;
static SHOW_UNNAMED: bool = true;
static PRINT_TOKEN_CONTENT: bool = true;
/// Use old printing method
static USE_OLD_FORMAT: bool = false;

pub fn print_ast(node: Node, source: &str) {
    if PRINT_FORMATTED_CODE {
        println!("Code: \n{}\n---", format_code(node.clone(), source, true));
    }

    if PRINT_S_EXP {
        println!(
            "Tree: \n{}\n---",
            match USE_OLD_FORMAT {
                true => node.to_sexp(0),
                false => format_tree(node, source, true),
            }
        );
    }
}

pub fn format_code(node: Node, source: &str, color: bool) -> String {
    let mut tree_cursor = node.walk();

    // walk the tree depth-first
    format_code_node(&mut tree_cursor, 0, source, color)
}

fn format_code_node(
    tree_cursor: &mut TreeCursor,
    tree_depth: usize,
    source: &str,
    color: bool,
) -> String {
    let node = tree_cursor.node();
    if node.is_named() {
        match node.kind().as_ref() {
            "header" => format!(
                "{}\n",
                node.utf8_text(source.as_bytes())
                    .expect("Source file contains non-UTF8 characters.")
                    .to_owned()
            ),
            "token_value_integer"
            | "token_value_reference"
            | "token_value_character"
            | "token_value_string" => node
                .utf8_text(source.as_bytes())
                .expect("Source file contains non-UTF8 characters.")
                .to_owned(),
            "comment" => {
                if SHOW_COMMENTS {
                    let comment = node
                        .utf8_text(source.as_bytes())
                        .expect("Source file contains non-UTF8 characters.")
                        .to_owned();
                    if REPLACE_COMMENT_WHITESPACE {
                        let reformat_comment = comment
                            .as_bytes()
                            .split(|c| c.is_ascii_whitespace())
                            .filter(|c| !c.is_empty())
                            .map(|c| std::str::from_utf8(c).unwrap())
                            .fold("".to_owned(), |combiner, c| format!("{} {}", combiner, c));
                        let reformat_comment = reformat_comment.trim();
                        if !reformat_comment.is_empty() {
                            format!("{}\n", reformat_comment)
                        } else {
                            "".to_owned()
                        }
                    } else {
                        comment
                    }
                } else {
                    "".to_owned()
                }
            }
            _ => format_code_children(tree_cursor, tree_depth + 1, source, color),
        }
    } else {
        match node.kind().as_ref() {
            "]" => format!("{}\n", node_code_string(node, color)),
            _ => node_code_string(node, color),
        }
    }
}

fn format_code_children(
    tree_cursor: &mut TreeCursor,
    tree_depth: usize,
    source: &str,
    color: bool,
) -> String {
    let mut output = "".to_owned();
    // go to first child
    if tree_cursor.goto_first_child() {
        output = format_code_node(tree_cursor, tree_depth, source, color);
        while tree_cursor.goto_next_sibling() {
            output = format!(
                "{}{}",
                output,
                format_code_node(tree_cursor, tree_depth, source, color)
            );
        }
        tree_cursor.goto_parent();
    }

    output
}

fn node_code_string(node: Node, color: bool) -> String {
    if !color {
        return node.kind();
    }
    node.kind()
}

pub fn format_tree(node: Node, source: &str, color: bool) -> String {
    let mut tree_cursor = node.walk();

    // walk the tree depth-first
    format_node(&mut tree_cursor, 0, source, color)
}

fn format_children(
    tree_cursor: &mut TreeCursor,
    tree_depth: usize,
    source: &str,
    color: bool,
) -> String {
    let mut output = "".to_owned();
    // go to first child
    if tree_cursor.goto_first_child() {
        output = format_node(tree_cursor, tree_depth, source, color);
        while tree_cursor.goto_next_sibling() {
            output = format!(
                "{}{}",
                output,
                format_node(tree_cursor, tree_depth, source, color)
            );
        }
        tree_cursor.goto_parent();
    }

    output
}

fn format_node(
    tree_cursor: &mut TreeCursor,
    tree_depth: usize,
    source: &str,
    color: bool,
) -> String {
    let node = tree_cursor.node();
    if node.is_named() || (!node.is_named() && SHOW_UNNAMED) {
        if node.named_child_count() > 0 || (SHOW_UNNAMED && node.child_count() > 0) {
            format!(
                "{}({}\n{}{})\n",
                " ".repeat(tree_depth * TAB_WIDTH),
                node_string(&node, source, false, color),
                format_children(tree_cursor, tree_depth + 1, source, color),
                " ".repeat(tree_depth * TAB_WIDTH),
            )
        } else {
            // Has no children
            format!(
                "{}({})\n",
                " ".repeat(tree_depth * TAB_WIDTH),
                node_string(&node, source, true, color),
            )
        }
    } else {
        // Nothing to add
        "".to_owned()
    }
}

fn node_string(node: &Node, source: &str, allow_content: bool, color: bool) -> String {
    let label = node_string_label(node, source, allow_content);
    if !color {
        return label;
    }
    if node.is_named() {
        label
    } else {
        label.yellow().to_string()
    }
}

fn node_string_label(node: &Node, source: &str, allow_content: bool) -> String {
    if PRINT_TOKEN_CONTENT && allow_content && node.kind() != "comment" && node.is_named() {
        let content = node
            .utf8_text(source.as_bytes())
            .expect("Non UTF-8 Characters found");
        format!("{}: \"{}\"", node.kind(), content)
    } else {
        node.kind()
    }
}
