use std::fmt;
/// A range of positions in a multi-line text document, both in terms of bytes and of
/// rows and columns.
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct Range {
    pub start_byte: usize,
    pub end_byte: usize,
    pub start_point: Point,
    pub end_point: Point,
}

/// A position in a multi-line text document, in terms of rows and columns.
///
/// Rows and columns are zero-based.
#[derive(Clone, Copy, Debug, Default, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct Point {
    pub row: usize,
    pub column: usize,
}

/// Display Point
/// ```rust
/// use df_ls_lexical_analysis::Point;
/// let point = Point{
///     row: 26,
///     column: 12,
/// };
/// assert_eq!("(26, 12)", format!("{}", point));
/// ````
impl fmt::Display for Point {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(f, "({}, {})", self.row, self.column)
    }
}
