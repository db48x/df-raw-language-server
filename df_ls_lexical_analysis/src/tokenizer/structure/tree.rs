use super::{DataNode, Node, TreeCursor};
use std::cell::RefCell;
use std::collections::HashMap;

pub static ROOT_ID: u64 = 0;

#[derive(Clone, Default)]
pub struct Tree {
    nodes: RefCell<HashMap<u64, DataNode>>,
}

impl Tree {
    pub(crate) fn get_tsnode(&self, k: u64) -> Option<DataNode> {
        match self.nodes.borrow().get(&k) {
            Some(node) => Some(node.clone()),
            None => None,
        }
    }

    pub(crate) fn add_child_to_node(&self, parent_id: u64, child_id: u64) {
        if let Some(parent) = self.nodes.borrow_mut().get_mut(&parent_id) {
            parent.children_ids.push(child_id);
        }
    }

    pub(crate) fn add_tsnode(&self, node: DataNode) {
        self.nodes.borrow_mut().insert(node.id, node);
    }

    pub(crate) fn update_node(&self, k: u64, node_n: DataNode) {
        if let Some(node) = self.nodes.borrow_mut().get_mut(&k) {
            node.id = node_n.id;
            node.kind_id = node_n.kind_id;
            node.kind = node_n.kind;
            node.name = node_n.name;
            node.start_byte = node_n.start_byte;
            node.end_byte = node_n.end_byte;
            node.start_point = node_n.start_point;
            node.end_point = node_n.end_point;
            node.children_ids = node_n.children_ids;
            node.parent_id = node_n.parent_id;
            node.next_sibling_id = node_n.next_sibling_id;
            node.prev_sibling_id = node_n.prev_sibling_id;
            node.tree = node_n.tree;
        }
    }

    pub(crate) fn finalize_tree(&self) {
        if let Some(root) = self.get_tsnode(ROOT_ID) {
            root.finalize_tree(None, None, None);
        }
    }

    /// Get the root node of the syntax tree.
    /// Root node always needs ID = 0
    pub fn root_node(&self) -> Node {
        Node::new(
            self.nodes
                .borrow()
                .get(&ROOT_ID)
                .expect("Root node does not exist."),
        )
    }

    pub fn print_nodes(&self) {
        println!("{:#?}", self.nodes.borrow());
    }

    /// Create a new [TreeCursor] starting from the root of the tree.
    pub fn walk(&self) -> TreeCursor {
        self.root_node().walk()
    }
}

/// Display Tree
/// ```rust
/// use df_ls_lexical_analysis::Tree;
/// let tree = Tree::default();
/// assert_eq!("{Tree, size:0}", format!("{:?}", tree));
/// ````
impl std::fmt::Debug for Tree {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        write!(f, "{{Tree, size:{}}}", self.nodes.borrow().len())
    }
}

// ---------------------------- TESTS --------------------------
#[cfg(test)]
mod tests {
    use pretty_assertions::assert_eq;

    #[test]
    fn test_tree() {
        let source = "header
            [REF:name]";
        // Parse Source to AST
        let (tree, diagnostic_list_lexer) = crate::do_lexical_analysis(source);
        println!("Lexer: {:#?}", diagnostic_list_lexer);
        assert_eq!(diagnostic_list_lexer, vec![]);
        tree.print_nodes();
        assert_eq!("{Tree, size:11}", format!("{:?}", tree));
    }
}
