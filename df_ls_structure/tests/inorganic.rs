mod common;
use df_ls_core::{Choose, DFChar, Reference, ReferenceTo};
use df_ls_structure::*;
use pretty_assertions::assert_eq;

#[test]
fn test_inorganic() {
    let source = "inorganic_header

    [OBJECT:INORGANIC]

    [INORGANIC:HEMATITE]
        [USE_MATERIAL_TEMPLATE:STONE_TEMPLATE]
        [STATE_NAME_ADJ:ALL_SOLID:hematite][DISPLAY_COLOR:4:7:0][TILE:156]
        [ENVIRONMENT:SEDIMENTARY:VEIN:100]
        [ENVIRONMENT:IGNEOUS_EXTRUSIVE:VEIN:100]
        [ITEM_SYMBOL:'*']
        [METAL_ORE:IRON:100]
        [SOLID_DENSITY:5260]
        [MATERIAL_VALUE:8]
        [IS_STONE][REACTION_CLASS:SCRAP_STONE]
        [MELTING_POINT:12736]

    [INORGANIC:NATIVE_SILVER]
        [ENVIRONMENT_SPEC:GRANITE:VEIN:100]
        [ENVIRONMENT_SPEC:GNEISS:VEIN:100]

    [INORGANIC:RAW_ADAMANTINE]
        [THREAD_METAL:ADAMANTINE:100]

    [INORGANIC:ADAMANTINE]
        [WAFERS]

    [INORGANIC:CERAMIC_EARTHENWARE]
        [IS_CERAMIC]
    ";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (DFRaw, _) =
        df_ls_syntax_analysis::do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    assert_eq!(diagnostic_list_lexer, vec![]);
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(diagnostic_list, vec![]);
    assert_eq!(
        structure,
        DFRaw {
            header: "inorganic_header".to_owned(),
            object_tokens: vec![ObjectToken {
                inorganic_tokens: vec![
                    InorganicToken {
                        reference: Some(ReferenceTo::new("HEMATITE".to_owned())),
                        use_material_template: Some(ReferenceTo::new("STONE_TEMPLATE".to_owned())),
                        environment: vec![
                            (EnvClassEnum::Sedimentary, InclusionTypeEnum::Vein, 100),
                            (EnvClassEnum::IgneousExtrusive, InclusionTypeEnum::Vein, 100),
                        ],
                        metal_ore: vec![(ReferenceTo::new("IRON".to_owned()), 100)],
                        tile: Some(DFChar('£')),
                        item_symbol: Some(DFChar('*')),
                        display_color: Some((4, 7, 0)),
                        state_name_adj: vec![(
                            Choose::Choice2(AllOrAllSolidEnum::AllSolid),
                            "hematite".to_owned()
                        )],
                        material_value: Some(8),
                        melting_point: Some(Choose::Choice1(12736)),
                        solid_density: Some(Choose::Choice1(5260)),
                        reaction_class: vec![Reference("SCRAP_STONE".to_owned())],
                        is_stone: Some(()),
                        ..Default::default()
                    },
                    InorganicToken {
                        reference: Some(ReferenceTo::new("NATIVE_SILVER".to_owned())),
                        environment_spec: vec![
                            (
                                ReferenceTo::new("GRANITE".to_owned()),
                                InclusionTypeEnum::Vein,
                                100
                            ),
                            (
                                ReferenceTo::new("GNEISS".to_owned()),
                                InclusionTypeEnum::Vein,
                                100
                            ),
                        ],
                        ..Default::default()
                    },
                    InorganicToken {
                        reference: Some(ReferenceTo::new("RAW_ADAMANTINE".to_owned())),
                        thread_metal: Some((ReferenceTo::new("ADAMANTINE".to_owned()), 100)),
                        ..Default::default()
                    },
                    InorganicToken {
                        reference: Some(ReferenceTo::new("ADAMANTINE".to_owned())),
                        wafers: Some(()),
                        ..Default::default()
                    },
                    InorganicToken {
                        reference: Some(ReferenceTo::new("CERAMIC_EARTHENWARE".to_owned())),
                        is_ceramic: Some(()),
                        ..Default::default()
                    },
                ],
                ..Default::default()
            }],
        }
    );
}
