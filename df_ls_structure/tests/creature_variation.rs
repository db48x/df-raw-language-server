mod common;
use df_ls_core::{AllowEmpty, Choose, Reference, ReferenceTo};
use df_ls_structure::*;
use pretty_assertions::assert_eq;

#[test]
fn test_creature_variation() {
    let source = "creature_variation_header

    [OBJECT:CREATURE_VARIATION]

    [CREATURE_VARIATION:ANIMAL_PERSON]
        [CV_REMOVE_TAG:NAME]
        [CV_CONVERT_TAG]
            [CVCT_MASTER:BODY]
            [CVCT_TARGET:SPIDER]
            [CVCT_REPLACEMENT:HUMANOID_6ARMS:3FINGERS]
        [CV_NEW_TAG:LARGE_ROAMING]

    [CREATURE_VARIATION:ANIMAL_PERSON_OTHER]
        [CV_REMOVE_CTAG:1:YES:NAME]
        [CV_CONVERT_CTAG:4:AFFIRMATIVE]
            [CVCT_MASTER:BODY]
            [CVCT_TARGET:SPIDER]
            [CVCT_REPLACEMENT:HUMANOID_6ARMS:3FINGERS]
        [CV_NEW_CTAG:3:YEP:LARGE_ROAMING]

    [CREATURE_VARIATION:STANDARD_BIPED_GAITS]
        [CV_NEW_TAG:GAIT:WALK:Sprint:!ARG4:10:3:!ARG2:50:LAYERS_SLOW:STRENGTH:AGILITY:STEALTH_SLOWS:50]
        [CV_NEW_TAG:GAIT:WALK:Run:!ARG3:5:3:!ARG2:10:LAYERS_SLOW:STRENGTH:AGILITY:STEALTH_SLOWS:20]

    ";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (DFRaw, _) =
        df_ls_syntax_analysis::do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    assert_eq!(diagnostic_list_lexer, vec![]);
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(diagnostic_list, vec![]);
    assert_eq!(
        structure,
        DFRaw {
            header: "creature_variation_header".to_owned(),
            object_tokens: vec![ObjectToken {
                creature_variation_tokens: vec![
                    CreatureVariationToken {
                        reference: Some(ReferenceTo::new("ANIMAL_PERSON".to_owned())),
                        cv_new_tag: vec![(Reference("LARGE_ROAMING".to_owned()), None)],
                        cv_remove_tag: vec![(Reference("NAME".to_owned()), None)],
                        cv_convert_tag: vec![CvConvertTag {
                            cv_convert_tag: Some(()),
                            cvct_master: Some(AllowEmpty::Some((
                                Reference("BODY".to_owned()),
                                None
                            ))),
                            cvct_target: Some((vec![Choose::Choice2(Choose::Choice1(Reference(
                                "SPIDER".to_owned()
                            )))],)),
                            cvct_replacement: Some((vec![
                                Choose::Choice2(Choose::Choice1(Reference(
                                    "HUMANOID_6ARMS".to_owned()
                                ))),
                                Choose::Choice2(Choose::Choice1(Reference("3FINGERS".to_owned()))),
                            ],)),
                        }],
                        ..Default::default()
                    },
                    CreatureVariationToken {
                        reference: Some(ReferenceTo::new("ANIMAL_PERSON_OTHER".to_owned())),
                        cv_new_ctag: vec![(
                            3,
                            Reference("YEP".to_owned()),
                            Some((vec![Choose::Choice2(Choose::Choice1(Reference(
                                "LARGE_ROAMING".to_owned()
                            )))],)),
                        )],
                        cv_remove_ctag: vec![(
                            1,
                            Reference("YES".to_owned()),
                            Some((vec![Choose::Choice2(Choose::Choice1(Reference(
                                "NAME".to_owned()
                            )))],)),
                        )],
                        cv_convert_ctag: vec![CvConvertCTag {
                            cv_convert_ctag: Some((
                                4,
                                Choose::Choice2(Choose::Choice1(Reference(
                                    "AFFIRMATIVE".to_owned()
                                ))),
                            )),
                            cvct_master: Some(AllowEmpty::Some((
                                Reference("BODY".to_owned()),
                                None
                            ))),
                            cvct_target: Some((vec![Choose::Choice2(Choose::Choice1(Reference(
                                "SPIDER".to_owned()
                            )))],)),
                            cvct_replacement: Some((vec![
                                Choose::Choice2(Choose::Choice1(Reference(
                                    "HUMANOID_6ARMS".to_owned()
                                ))),
                                Choose::Choice2(Choose::Choice1(Reference("3FINGERS".to_owned()))),
                            ],)),
                        }],
                        ..Default::default()
                    },
                    CreatureVariationToken {
                        reference: Some(ReferenceTo::new("STANDARD_BIPED_GAITS".to_owned())),
                        cv_new_tag: vec![
                            (
                                Reference("GAIT".to_owned()),
                                Some((vec![
                                    Choose::Choice2(Choose::Choice1(Reference("WALK".to_owned()))),
                                    Choose::Choice1("Sprint".to_owned()),
                                    Choose::Choice1("!ARG4".to_owned()),
                                    Choose::Choice2(Choose::Choice2(Choose::Choice1(
                                        Choose::Choice1(10)
                                    ))),
                                    Choose::Choice2(Choose::Choice2(Choose::Choice1(
                                        Choose::Choice1(3)
                                    ))),
                                    Choose::Choice1("!ARG2".to_owned()),
                                    Choose::Choice2(Choose::Choice2(Choose::Choice1(
                                        Choose::Choice1(50)
                                    ))),
                                    Choose::Choice2(Choose::Choice1(Reference(
                                        "LAYERS_SLOW".to_owned()
                                    ))),
                                    Choose::Choice2(Choose::Choice1(Reference(
                                        "STRENGTH".to_owned()
                                    ))),
                                    Choose::Choice2(Choose::Choice1(Reference(
                                        "AGILITY".to_owned()
                                    ))),
                                    Choose::Choice2(Choose::Choice1(Reference(
                                        "STEALTH_SLOWS".to_owned()
                                    ))),
                                    Choose::Choice2(Choose::Choice2(Choose::Choice1(
                                        Choose::Choice1(50)
                                    ))),
                                ],)),
                            ),
                            (
                                Reference("GAIT".to_owned()),
                                Some((vec![
                                    Choose::Choice2(Choose::Choice1(Reference("WALK".to_owned()))),
                                    Choose::Choice1("Run".to_owned()),
                                    Choose::Choice1("!ARG3".to_owned()),
                                    Choose::Choice2(Choose::Choice2(Choose::Choice1(
                                        Choose::Choice1(5)
                                    ))),
                                    Choose::Choice2(Choose::Choice2(Choose::Choice1(
                                        Choose::Choice1(3)
                                    ))),
                                    Choose::Choice1("!ARG2".to_owned()),
                                    Choose::Choice2(Choose::Choice2(Choose::Choice1(
                                        Choose::Choice1(10)
                                    ))),
                                    Choose::Choice2(Choose::Choice1(Reference(
                                        "LAYERS_SLOW".to_owned()
                                    ))),
                                    Choose::Choice2(Choose::Choice1(Reference(
                                        "STRENGTH".to_owned()
                                    ))),
                                    Choose::Choice2(Choose::Choice1(Reference(
                                        "AGILITY".to_owned()
                                    ))),
                                    Choose::Choice2(Choose::Choice1(Reference(
                                        "STEALTH_SLOWS".to_owned()
                                    ))),
                                    Choose::Choice2(Choose::Choice2(Choose::Choice1(
                                        Choose::Choice1(20)
                                    ))),
                                ],)),
                            ),
                        ],
                        ..Default::default()
                    },
                ],
                ..Default::default()
            }],
        }
    );
}
