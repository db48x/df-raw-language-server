mod common;
use df_ls_core::{AllowEmpty, Choose, DFChar, Reference, ReferenceTo};
use df_ls_structure::*;
use pretty_assertions::assert_eq;

#[test]
fn test_creature() {
    let source = "creature_header

    [OBJECT:CREATURE]

    [CREATURE:WORM]
        [BODY:BODY_WITH_HEAD_FLAG:HEART:GUTS:BRAIN:MOUTH]
        [BODY_DETAIL_PLAN:NORMAL_STANDARD_MATERIALS]
            [REMOVE_MATERIAL:HAIR] // not actually a nesting token despite the layout
            [REMOVE_MATERIAL:BONE]
        [GAIT:WALK:Crawl:2900:NO_BUILD_UP:0:LAYERS_SLOW:STRENGTH:AGILITY]

    [CREATURE:DWARF]
        [DESCRIPTION:A short, sturdy creature fond of drink and industry.]
        [CREATURE_CLASS:MAMMAL]

        [BODY:HUMANOID_NECK:2EYES:2EARS]

        [BODY_DETAIL_PLAN:STANDARD_MATERIALS]
        [BODY_DETAIL_PLAN:VERTEBRATE_TISSUE_LAYERS:SKIN:FAT:MUSCLE:BONE:CARTILAGE]

        [USE_TISSUE_TEMPLATE:EYEBROW:EYEBROW_TEMPLATE]
        [TISSUE_LAYER:BY_CATEGORY:HEAD:EYEBROW:ABOVE:BY_CATEGORY:EYE]
        [USE_TISSUE_TEMPLATE:EYELASH:EYELASH_TEMPLATE]
        [TISSUE_LAYER:BY_CATEGORY:EYELID:EYELASH:FRONT]

        [SELECT_TISSUE_LAYER:HEART:BY_CATEGORY:HEART]
        [PLUS_TISSUE_LAYER:SKIN:BY_CATEGORY:THROAT]
            [TL_MAJOR_ARTERIES]

        [CREATURE_CLASS:GENERAL_POISON]
        [USE_MATERIAL_TEMPLATE:SWEAT:SWEAT_TEMPLATE]
        [SECRETION:LOCAL_CREATURE_MAT:SWEAT:LIQUID:BY_CATEGORY:ALL:SKIN:EXERTION]

        [CAN_DO_INTERACTION:PET_ANIMAL]
            [CDI:ADV_NAME:Pet animal]
            [CDI:USAGE_HINT:GREETING]
            [CDI:BP_REQUIRED:BY_TYPE:GRASP]

        [PHYS_ATT_RANGE:STRENGTH:450:950:1150:1250:1350:1550:2250]
        [MENT_ATT_RANGE:ANALYTICAL_ABILITY:450:950:1150:1250:1350:1550:2250]

        [BODY_APPEARANCE_MODIFIER:HEIGHT:75:95:98:100:102:105:125]
            [APP_MOD_IMPORTANCE:500]

        [SET_BP_GROUP:BY_CATEGORY:EAR]
            [BP_APPEARANCE_MODIFIER:BROADNESS:90:95:98:100:102:105:110]
                [APP_MOD_IMPORTANCE:700]
                [APP_MOD_NOUN:ears:PLURAL]
                [APP_MOD_DESC_RANGE:91:94:98:102:106:109]

        [ATTACK:PUNCH:BODYPART:BY_TYPE:GRASP]
            [ATTACK_SKILL:GRASP_STRIKE]

        [ATTACK:KICK:BODYPART:BY_TYPE:STANCE]
            [ATTACK_SKILL:STANCE_STRIKE]
        [ATTACK:SCRATCH:CHILD_TISSUE_LAYER_GROUP:BY_TYPE:GRASP:BY_CATEGORY:FINGER:NAIL]
            [ATTACK_SKILL:GRASP_STRIKE]
        [ATTACK:BITE:CHILD_BODYPART_GROUP:BY_CATEGORY:HEAD:BY_CATEGORY:TOOTH]
            [ATTACK_SKILL:BITE]

        [PROFESSION_NAME:CRAFTSMAN:craftsdwarf:craftsdwarves]
        [SPEECH:dwarf.txt]
        [HOMEOTHERM:10067]
        [ALCOHOL_DEPENDENT]
        [SYNDROME_DILUTION_FACTOR:INEBRIATION:150]
        [APPLY_CREATURE_VARIATION:STANDARD_BIPED_GAITS:900:711:521:293:1900:2900]
        [PERSONALITY:IMMODERATION:0:55:100]
        [MANNERISM_FINGERS:finger:fingers]

        [SET_TL_GROUP:BY_CATEGORY:HEAD:EYEBROW]
            [TISSUE_LAYER_APPEARANCE_MODIFIER:LENGTH:50:80:90:100:110:120:150]
                [APP_MOD_NOUN:eyebrows:PLURAL]
                [APP_MOD_DESC_RANGE:55:70:90:110:130:145]

        [SET_TL_GROUP:BY_CATEGORY:HEAD:HAIR]
         [PLUS_TL_GROUP:BY_CATEGORY:HEAD:CHEEK_WHISKERS]
            [TISSUE_LAYER_APPEARANCE_MODIFIER:LENGTH:0:0:0:0:0:0:0]
                [APP_MOD_NOUN:hair:SINGULAR]
                [APP_MOD_RATE:1:DAILY:0:1000:0:0:NO_END]
                [APP_MOD_DESC_RANGE:10:25:75:125:200:300]

        [SET_TL_GROUP:BY_CATEGORY:HEAD:HAIR]
            [TISSUE_STYLE_UNIT:HAIR:STANDARD_HAIR_SHAPINGS]
                [TSU_NOUN:hair:SINGULAR]

        [CASTE:FEMALE]
            [FEMALE]
            [MULTIPLE_LITTER_RARE]
        [CASTE:MALE]
            [MALE]
            [SET_BP_GROUP:BY_TYPE:LOWERBODY][BP_ADD_TYPE:GELDABLE]
            [BODY_DETAIL_PLAN:FACIAL_HAIR_TISSUE_LAYERS]

    [CREATURE:CRAB]
        [DESCRIPTION:A tiny shelled ocean creature with many long legs.  It has two large pincers on its front limbs.]
        [NAME:crab:crabs:crab]
        [CASTE_NAME:crab:crabs:crab]
        [CREATURE_TILE:'c'][COLOR:4:0:1]
        [NATURAL]
        [BIOME:ANY_OCEAN]
        [FREQUENCY:100]
        [LARGE_ROAMING]
        [POPULATION_NUMBER:250:500]
        [PREFSTRING:pincers]
        [PREFSTRING:sideways walk]
        [AMPHIBIOUS][UNDERSWIM]
        [ALL_ACTIVE]
        [NO_SLEEP]
        [HOMEOTHERM:10071]
        [APPLY_CREATURE_VARIATION:STANDARD_WALKING_GAITS:9000:8900:8825:8775:9500:9900] 1 kph
        [APPLY_CREATURE_VARIATION:STANDARD_CLIMBING_GAITS:9000:8900:8825:8775:9500:9900] 1 kph
        [APPLY_CREATURE_VARIATION:STANDARD_CRAWLING_GAITS:9000:8900:8825:8775:9500:9900] 1 kph
        [NATURAL_SKILL:CLIMBING:15]
        [MUNDANE]
        [NOBONES]
        [BODY:CRAB_BODY:2EYES:HEART:BRAIN:UPPERBODY_PINCERS]
        [BODY_DETAIL_PLAN:CHITIN_MATERIALS]
        [BODY_DETAIL_PLAN:CHITIN_TISSUES]
        [BODY_DETAIL_PLAN:EXOSKELETON_TISSUE_LAYERS:CHITIN:FAT:MUSCLE]
        [BODY_DETAIL_PLAN:STANDARD_HEAD_POSITIONS]
        [USE_MATERIAL_TEMPLATE:SINEW:SINEW_TEMPLATE]
        [TENDONS:LOCAL_CREATURE_MAT:SINEW:200]
        [LIGAMENTS:LOCAL_CREATURE_MAT:SINEW:200]
        [HAS_NERVES]
        [USE_MATERIAL_TEMPLATE:BLOOD:BLOOD_TEMPLATE]
            [STATE_COLOR:ALL:BLUE] copper not iron based
        [BLOOD:LOCAL_CREATURE_MAT:BLOOD:LIQUID]
        [CREATURE_CLASS:GENERAL_POISON]
        [GETS_WOUND_INFECTIONS]
        [GETS_INFECTIONS_FROM_ROT]
        [USE_MATERIAL_TEMPLATE:PUS:PUS_TEMPLATE]
        [PUS:LOCAL_CREATURE_MAT:PUS:LIQUID]
        [BODY_SIZE:0:0:1]
        [BODY_SIZE:1:0:8000]
        [APPLY_CREATURE_VARIATION:PINCER_ATTACK]
        [MAXAGE:10:20]
        [CASTE:FEMALE]
            [FEMALE]
        [CASTE:MALE]
            [MALE]
        [SELECT_CASTE:ALL]
            [SET_TL_GROUP:BY_CATEGORY:ALL:CHITIN]
                [TL_COLOR_MODIFIER:RED:1]
                    [TLCM_NOUN:chitin:SINGULAR]
            [SET_TL_GROUP:BY_CATEGORY:EYE:EYE]
                [TL_COLOR_MODIFIER:BLACK:1]
                    [TLCM_NOUN:eyes:PLURAL]
    ";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (DFRaw, _) =
        df_ls_syntax_analysis::do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    assert_eq!(diagnostic_list_lexer, vec![]);
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(diagnostic_list, vec![]);
    assert_eq!(
        structure,
        DFRaw {
            header: "creature_header".to_owned(),
            object_tokens: vec![ObjectToken {
                creature_tokens: vec![
                    CreatureToken {
                        reference: Some(ReferenceTo::new("WORM".to_owned())),
                        remove_material: vec![
                            Reference("HAIR".to_owned()),
                            Reference("BONE".to_owned()),
                        ],
                        body: Some((vec![
                            ReferenceTo::new("BODY_WITH_HEAD_FLAG".to_owned()),
                            ReferenceTo::new("HEART".to_owned()),
                            ReferenceTo::new("GUTS".to_owned()),
                            ReferenceTo::new("BRAIN".to_owned()),
                            ReferenceTo::new("MOUTH".to_owned()),
                        ],)),
                        body_detail_plan: vec![(
                            ReferenceTo::new("NORMAL_STANDARD_MATERIALS".to_owned()),
                            None,
                            None,
                            None,
                            None,
                            None,
                        )],
                        gait: vec![(
                            GaitTypeEnum::Walk,
                            "Crawl".to_owned(),
                            2900,
                            Choose::Choice1(NoBuildUpEnum::NoBuildUp),
                            0,
                            Some(GaitFlagTokenArg {
                                layers_slow: Some(()),
                                strength: Some(()),
                                agility: Some(()),
                                ..Default::default()
                            }),
                        )],
                        ..Default::default()
                    },
                    CreatureToken {
                        reference: Some(ReferenceTo::new("DWARF".to_owned())),
                        castes: vec![
                            Caste {
                                reference: Some(Reference("FEMALE".to_owned())),
                                female: Some(()),
                                multiple_litter_rare: Some(()),
                                ..Default::default()
                            },
                            Caste {
                                reference: Some(Reference("MALE".to_owned())),
                                set_bp_group: vec![SetBpGroup {
                                    set_bp_group: Some(BpCriteriaTokenArg::ByType(
                                        BodyPartTypeEnum::Lowerbody,
                                    )),
                                    bp_add_type: Some(BodyPartTypeEnum::Geldable),
                                    ..Default::default()
                                }],
                                body_detail_plan: vec![(
                                    ReferenceTo::new("FACIAL_HAIR_TISSUE_LAYERS".to_owned()),
                                    None,
                                    None,
                                    None,
                                    None,
                                    None,
                                )],
                                male: Some(()),
                                ..Default::default()
                            },
                        ],
                        use_tissue_template: vec![
                            UseTissueTemplate {
                                reference: Some((
                                    Reference("EYEBROW".to_owned()),
                                    ReferenceTo::new("EYEBROW_TEMPLATE".to_owned()),
                                )),
                                ..Default::default()
                            },
                            UseTissueTemplate {
                                reference: Some((
                                    Reference("EYELASH".to_owned()),
                                    ReferenceTo::new("EYELASH_TEMPLATE".to_owned()),
                                )),
                                ..Default::default()
                            },
                        ],
                        use_material_template: vec![UseMaterialTemplate {
                            reference: Some((
                                Reference("SWEAT".to_owned()),
                                ReferenceTo::new("SWEAT_TEMPLATE".to_owned()),
                            )),
                            ..Default::default()
                        }],
                        profession_name: vec![(
                            UnitTypeEnum::Craftsman,
                            "craftsdwarf".to_owned(),
                            "craftsdwarves".to_owned(),
                        )],
                        speech: Some("dwarf.txt".to_owned()),
                        attacks: vec![
                            Attack {
                                reference_and_bp: Some((
                                    Reference("PUNCH".to_owned()),
                                    AttackPerformerTokenArg::Bodypart(BpCriteriaTokenArg::ByType(
                                        BodyPartTypeEnum::Grasp,
                                    )),
                                )),
                                attack_skill: Some(SkillEnum::GraspStrike),
                                ..Default::default()
                            },
                            Attack {
                                reference_and_bp: Some((
                                    Reference("KICK".to_owned()),
                                    AttackPerformerTokenArg::Bodypart(BpCriteriaTokenArg::ByType(
                                        BodyPartTypeEnum::Stance,
                                    )),
                                )),
                                attack_skill: Some(SkillEnum::StanceStrike),
                                ..Default::default()
                            },
                            Attack {
                                reference_and_bp: Some((
                                    Reference("SCRATCH".to_owned()),
                                    AttackPerformerTokenArg::ChildTissueLayerGroup((
                                        BpCriteriaTokenArg::ByType(BodyPartTypeEnum::Grasp),
                                        BpCriteriaTokenArg::ByCategory(Reference(
                                            "FINGER".to_owned(),
                                        )),
                                        Reference("NAIL".to_owned()),
                                    )),
                                )),
                                attack_skill: Some(SkillEnum::GraspStrike),
                                ..Default::default()
                            },
                            Attack {
                                reference_and_bp: Some((
                                    Reference("BITE".to_owned()),
                                    AttackPerformerTokenArg::ChildBodypartGroup((
                                        BpCriteriaTokenArg::ByCategory(Reference(
                                            "HEAD".to_owned(),
                                        )),
                                        BpCriteriaTokenArg::ByCategory(Reference(
                                            "TOOTH".to_owned(),
                                        )),
                                    )),
                                )),
                                attack_skill: Some(SkillEnum::Bite),
                                ..Default::default()
                            },
                        ],
                        can_do_interaction: vec![CanDoInteraction {
                            reference: Some(Reference("PET_ANIMAL".to_owned())),
                            cdi: vec![
                                CdiTokenArg::AdvName("Pet animal".to_owned()),
                                CdiTokenArg::UsageHint(UsageHintEnum::Greeting),
                                CdiTokenArg::BpRequired(BpCriteriaTokenArg::ByType(
                                    BodyPartTypeEnum::Grasp,
                                )),
                            ],
                            ..Default::default()
                        }],
                        set_bp_group: vec![SetBpGroup {
                            set_bp_group: Some(BpCriteriaTokenArg::ByCategory(Reference(
                                "EAR".to_owned(),
                            ))),
                            bp_appearance_modifier: vec![BpAppearanceModifier {
                                bp_appearance_modifier: Some((
                                    BpAppModifersEnum::Broadness,
                                    90,
                                    95,
                                    98,
                                    100,
                                    102,
                                    105,
                                    110,
                                )),
                                app_mod_desc_range: Some((91, 94, 98, 102, 106, 109)),
                                app_mod_importance: Some(700),
                                app_mod_noun: Some((
                                    "ears".to_owned(),
                                    SingularOrPluralEnum::Plural,
                                )),
                                ..Default::default()
                            }],
                            ..Default::default()
                        }],
                        set_tl_group: vec![
                            SetTlGroup {
                                set_tl_group: Some((
                                    BpCriteriaTokenArg::ByCategory(Reference("HEAD".to_owned())),
                                    Reference("EYEBROW".to_owned()),
                                )),
                                tissue_layer_appearance_modifier: vec![
                                    TissueLayerAppearanceModifier {
                                        tissue_layer_appearance_modifier: Some((
                                            TissueModifierEnum::Length,
                                            50,
                                            80,
                                            90,
                                            100,
                                            110,
                                            120,
                                            150,
                                        )),
                                        app_mod_desc_range: Some((55, 70, 90, 110, 130, 145)),
                                        app_mod_noun: Some((
                                            "eyebrows".to_owned(),
                                            SingularOrPluralEnum::Plural,
                                        )),
                                        ..Default::default()
                                    },
                                ],
                                ..Default::default()
                            },
                            SetTlGroup {
                                set_tl_group: Some((
                                    BpCriteriaTokenArg::ByCategory(Reference("HEAD".to_owned())),
                                    Reference("HAIR".to_owned()),
                                )),
                                plus_tl_group: vec![(
                                    BpCriteriaTokenArg::ByCategory(Reference("HEAD".to_owned())),
                                    Reference("CHEEK_WHISKERS".to_owned()),
                                )],
                                tissue_layer_appearance_modifier: vec![
                                    TissueLayerAppearanceModifier {
                                        tissue_layer_appearance_modifier: Some((
                                            TissueModifierEnum::Length,
                                            0,
                                            0,
                                            0,
                                            0,
                                            0,
                                            0,
                                            0,
                                        )),
                                        app_mod_desc_range: Some((10, 25, 75, 125, 200, 300)),
                                        app_mod_noun: Some((
                                            "hair".to_owned(),
                                            SingularOrPluralEnum::Singular,
                                        )),
                                        app_mod_rate: Some((
                                            1,
                                            TimescaleEnum::Daily,
                                            0,
                                            1000,
                                            0,
                                            0,
                                            Choose::Choice1(NoEndEnum::NoEnd),
                                        )),
                                        ..Default::default()
                                    },
                                ],
                                ..Default::default()
                            },
                            SetTlGroup {
                                set_tl_group: Some((
                                    BpCriteriaTokenArg::ByCategory(Reference("HEAD".to_owned())),
                                    Reference("HAIR".to_owned()),
                                )),
                                tissue_style_unit: Some(TissueStyleUnit {
                                    tissue_style_unit: Some((
                                        Reference("HAIR".to_owned()),
                                        StylingEnum::StandardHairShapings,
                                    )),
                                    tsu_noun: Some((
                                        "hair".to_owned(),
                                        SingularOrPluralEnum::Singular,
                                    )),
                                }),
                                ..Default::default()
                            },
                        ],
                        select_tissue_layer: vec![SelectTissueLayer {
                            select_tissue_layer: Some(Choose::Choice2((
                                Reference("HEART".to_owned()),
                                BpCriteriaTokenArg::ByCategory(Reference("HEART".to_owned())),
                                None,
                            ))),
                            plus_tissue_layer: vec![(
                                Reference("SKIN".to_owned()),
                                BpCriteriaTokenArg::ByCategory(Reference("THROAT".to_owned())),
                            )],
                            tl_major_arteries: Some(()),
                            ..Default::default()
                        }],
                        tissue_layer: vec![
                            TissueLayer {
                                tissue_layer: Some((
                                    BpCriteriaTokenArg::ByCategory(Reference("HEAD".to_owned())),
                                    Reference("EYEBROW".to_owned()),
                                    Some(Choose::Choice2(Choose::Choice2((
                                        BpRelationEnum::Above,
                                        BpCriteriaTokenArg::ByCategory(Reference("EYE".to_owned()),),
                                        None,
                                    )))),
                                )),
                                ..Default::default()
                            },
                            TissueLayer {
                                tissue_layer: Some((
                                    BpCriteriaTokenArg::ByCategory(Reference("EYELID".to_owned())),
                                    Reference("EYELASH".to_owned()),
                                    Some(Choose::Choice2(Choose::Choice1(PositionEnum::Front))),
                                )),
                                ..Default::default()
                            },
                        ],
                        alcohol_dependent: Some(()),
                        apply_creature_variation: vec![(
                            ReferenceTo::new("STANDARD_BIPED_GAITS".to_owned()),
                            Some((vec![
                                AllowEmpty::Some(Choose::Choice2(Choose::Choice2(
                                    Choose::Choice1(Choose::Choice1(900,))
                                ))),
                                AllowEmpty::Some(Choose::Choice2(Choose::Choice2(
                                    Choose::Choice1(Choose::Choice1(711,))
                                ))),
                                AllowEmpty::Some(Choose::Choice2(Choose::Choice2(
                                    Choose::Choice1(Choose::Choice1(521,))
                                ))),
                                AllowEmpty::Some(Choose::Choice2(Choose::Choice2(
                                    Choose::Choice1(Choose::Choice1(293,))
                                ))),
                                AllowEmpty::Some(Choose::Choice2(Choose::Choice2(
                                    Choose::Choice1(Choose::Choice1(1900,))
                                ))),
                                AllowEmpty::Some(Choose::Choice2(Choose::Choice2(
                                    Choose::Choice1(Choose::Choice1(2900,))
                                ))),
                            ],)),
                        )],
                        body: Some((vec![
                            ReferenceTo::new("HUMANOID_NECK".to_owned()),
                            ReferenceTo::new("2EYES".to_owned()),
                            ReferenceTo::new("2EARS".to_owned()),
                        ],)),
                        body_appearance_modifier: vec![BodyAppearanceModifier {
                            body_appearance_modifier: Some((
                                AppModTypeEnum::Height,
                                75,
                                95,
                                98,
                                100,
                                102,
                                105,
                                125,
                            )),
                            app_mod_importance: Some(500),
                            ..Default::default()
                        }],
                        body_detail_plan: vec![
                            (
                                ReferenceTo::new("STANDARD_MATERIALS".to_owned()),
                                None,
                                None,
                                None,
                                None,
                                None,
                            ),
                            (
                                ReferenceTo::new("VERTEBRATE_TISSUE_LAYERS".to_owned()),
                                Some(Reference("SKIN".to_owned())),
                                Some(Reference("FAT".to_owned())),
                                Some(Reference("MUSCLE".to_owned())),
                                Some(Reference("BONE".to_owned())),
                                Some(Reference("CARTILAGE".to_owned())),
                            ),
                        ],
                        creature_class: vec![
                            Reference("MAMMAL".to_owned()),
                            Reference("GENERAL_POISON".to_owned()),
                        ],
                        description: Some(
                            "A short, sturdy creature fond of drink and industry.".to_owned(),
                        ),
                        homeotherm: Some(Choose::Choice1(10067)),
                        mannerism_fingers: Some(("finger".to_owned(), "fingers".to_owned())),
                        ment_att_range: vec![(
                            SoulAttributeEnum::AnalyticalAbility,
                            450,
                            950,
                            1150,
                            1250,
                            1350,
                            1550,
                            2250,
                        )],
                        personality: vec![(PersonalityTraitEnum::Immoderation, 0, 55, 100)],
                        phys_att_range: vec![(
                            BodyAttributeEnum::Strength,
                            450,
                            950,
                            1150,
                            1250,
                            1350,
                            1550,
                            2250,
                        )],
                        secretion: vec![(
                            MaterialTokenArg {
                                material: MaterialTypeEnum::LocalCreatureMat(Reference(
                                    "SWEAT".to_owned(),
                                )),
                            },
                            MaterialStateEnum::Liquid,
                            BpCriteriaTokenArg::ByCategory(Reference("ALL".to_owned())),
                            Reference("SKIN".to_owned()),
                            Some(SecretionTriggerEnum::Exertion),
                        )],
                        syndrome_dilution_factor: vec![(Reference("INEBRIATION".to_owned()), 150)],
                        ..Default::default()
                    },
                    CreatureToken {
                        reference: Some(ReferenceTo::new("CRAB".to_owned())),
                        castes: vec![
                            Caste {
                                reference: Some(Reference("FEMALE".to_owned())),
                                female: Some(()),
                                ..Default::default()
                            },
                            Caste {
                                reference: Some(Reference("MALE".to_owned())),
                                male: Some(()),
                                ..Default::default()
                            },
                        ],
                        select_castes: vec![SelectCaste {
                            reference: Some(Reference("ALL".to_owned())),
                            set_tl_group: vec![
                                SetTlGroup {
                                    set_tl_group: Some((
                                        BpCriteriaTokenArg::ByCategory(Reference("ALL".to_owned()),),
                                        Reference("CHITIN".to_owned()),
                                    )),
                                    tl_color_modifier: vec![TlColorModifier {
                                        tl_color_modifier: Some((vec![(
                                            ReferenceTo::new("RED".to_owned()),
                                            1
                                        )],)),
                                        tlcm_noun: Some((
                                            "chitin".to_owned(),
                                            SingularOrPluralEnum::Singular,
                                        )),
                                        ..Default::default()
                                    }],
                                    ..Default::default()
                                },
                                SetTlGroup {
                                    set_tl_group: Some((
                                        BpCriteriaTokenArg::ByCategory(Reference("EYE".to_owned()),),
                                        Reference("EYE".to_owned()),
                                    )),
                                    tl_color_modifier: vec![TlColorModifier {
                                        tl_color_modifier: Some((vec![(
                                            ReferenceTo::new("BLACK".to_owned()),
                                            1,
                                        )],)),
                                        tlcm_noun: Some((
                                            "eyes".to_owned(),
                                            SingularOrPluralEnum::Plural,
                                        )),
                                        ..Default::default()
                                    }],
                                    ..Default::default()
                                },
                            ],
                            ..Default::default()
                        }],
                        use_material_template: vec![
                            UseMaterialTemplate {
                                reference: Some((
                                    Reference("SINEW".to_owned()),
                                    ReferenceTo::new("SINEW_TEMPLATE".to_owned()),
                                )),
                                ..Default::default()
                            },
                            UseMaterialTemplate {
                                reference: Some((
                                    Reference("BLOOD".to_owned()),
                                    ReferenceTo::new("BLOOD_TEMPLATE".to_owned()),
                                )),
                                state_color: vec![(
                                    Choose::Choice2(AllOrAllSolidEnum::All),
                                    ReferenceTo::new("BLUE".to_owned()),
                                )],
                                ..Default::default()
                            },
                            UseMaterialTemplate {
                                reference: Some((
                                    Reference("PUS".to_owned()),
                                    ReferenceTo::new("PUS_TEMPLATE".to_owned()),
                                )),
                                ..Default::default()
                            },
                        ],
                        biome: vec![BiomeEnum::AnyOcean],
                        color: Some((4, 0, 1)),
                        creature_tile: Some(DFChar('c')),
                        frequency: Some(100),
                        large_roaming: Some(()),
                        mundane: Some(()),
                        name: Some(("crab".to_owned(), "crabs".to_owned(), "crab".to_owned())),
                        prefstring: vec!["pincers".to_owned(), "sideways walk".to_owned()],
                        all_active: Some(()),
                        amphibious: Some(()),
                        apply_creature_variation: vec![
                            (
                                ReferenceTo::new("STANDARD_WALKING_GAITS".to_owned()),
                                Some((vec![
                                    AllowEmpty::Some(Choose::Choice2(Choose::Choice2(
                                        Choose::Choice1(Choose::Choice1(9000),)
                                    ))),
                                    AllowEmpty::Some(Choose::Choice2(Choose::Choice2(
                                        Choose::Choice1(Choose::Choice1(8900),)
                                    ))),
                                    AllowEmpty::Some(Choose::Choice2(Choose::Choice2(
                                        Choose::Choice1(Choose::Choice1(8825),)
                                    ))),
                                    AllowEmpty::Some(Choose::Choice2(Choose::Choice2(
                                        Choose::Choice1(Choose::Choice1(8775),)
                                    ))),
                                    AllowEmpty::Some(Choose::Choice2(Choose::Choice2(
                                        Choose::Choice1(Choose::Choice1(9500),)
                                    ))),
                                    AllowEmpty::Some(Choose::Choice2(Choose::Choice2(
                                        Choose::Choice1(Choose::Choice1(9900),)
                                    ))),
                                ],)),
                            ),
                            (
                                ReferenceTo::new("STANDARD_CLIMBING_GAITS".to_owned()),
                                Some((vec![
                                    AllowEmpty::Some(Choose::Choice2(Choose::Choice2(
                                        Choose::Choice1(Choose::Choice1(9000),)
                                    ))),
                                    AllowEmpty::Some(Choose::Choice2(Choose::Choice2(
                                        Choose::Choice1(Choose::Choice1(8900),)
                                    ))),
                                    AllowEmpty::Some(Choose::Choice2(Choose::Choice2(
                                        Choose::Choice1(Choose::Choice1(8825),)
                                    ))),
                                    AllowEmpty::Some(Choose::Choice2(Choose::Choice2(
                                        Choose::Choice1(Choose::Choice1(8775),)
                                    ))),
                                    AllowEmpty::Some(Choose::Choice2(Choose::Choice2(
                                        Choose::Choice1(Choose::Choice1(9500),)
                                    ))),
                                    AllowEmpty::Some(Choose::Choice2(Choose::Choice2(
                                        Choose::Choice1(Choose::Choice1(9900),)
                                    ))),
                                ],)),
                            ),
                            (
                                ReferenceTo::new("STANDARD_CRAWLING_GAITS".to_owned()),
                                Some((vec![
                                    AllowEmpty::Some(Choose::Choice2(Choose::Choice2(
                                        Choose::Choice1(Choose::Choice1(9000),)
                                    ))),
                                    AllowEmpty::Some(Choose::Choice2(Choose::Choice2(
                                        Choose::Choice1(Choose::Choice1(8900),)
                                    ))),
                                    AllowEmpty::Some(Choose::Choice2(Choose::Choice2(
                                        Choose::Choice1(Choose::Choice1(8825),)
                                    ))),
                                    AllowEmpty::Some(Choose::Choice2(Choose::Choice2(
                                        Choose::Choice1(Choose::Choice1(8775),)
                                    ))),
                                    AllowEmpty::Some(Choose::Choice2(Choose::Choice2(
                                        Choose::Choice1(Choose::Choice1(9500),)
                                    ))),
                                    AllowEmpty::Some(Choose::Choice2(Choose::Choice2(
                                        Choose::Choice1(Choose::Choice1(9900),)
                                    ))),
                                ],)),
                            ),
                            (ReferenceTo::new("PINCER_ATTACK".to_owned()), None,),
                        ],
                        blood: Some((
                            MaterialTokenArg {
                                material: MaterialTypeEnum::LocalCreatureMat(Reference(
                                    "BLOOD".to_owned(),
                                )),
                            },
                            MaterialStateEnum::Liquid,
                        )),
                        body: Some((vec![
                            ReferenceTo::new("CRAB_BODY".to_owned()),
                            ReferenceTo::new("2EYES".to_owned()),
                            ReferenceTo::new("HEART".to_owned()),
                            ReferenceTo::new("BRAIN".to_owned()),
                            ReferenceTo::new("UPPERBODY_PINCERS".to_owned()),
                        ],)),
                        body_detail_plan: vec![
                            (
                                ReferenceTo::new("CHITIN_MATERIALS".to_owned()),
                                None,
                                None,
                                None,
                                None,
                                None,
                            ),
                            (
                                ReferenceTo::new("CHITIN_TISSUES".to_owned()),
                                None,
                                None,
                                None,
                                None,
                                None,
                            ),
                            (
                                ReferenceTo::new("EXOSKELETON_TISSUE_LAYERS".to_owned()),
                                Some(Reference("CHITIN".to_owned())),
                                Some(Reference("FAT".to_owned())),
                                Some(Reference("MUSCLE".to_owned())),
                                None,
                                None,
                            ),
                            (
                                ReferenceTo::new("STANDARD_HEAD_POSITIONS".to_owned()),
                                None,
                                None,
                                None,
                                None,
                                None,
                            ),
                        ],
                        body_size: vec![(0, 0, 1), (1, 0, 8000)],
                        caste_name: Some((
                            "crab".to_owned(),
                            "crabs".to_owned(),
                            "crab".to_owned(),
                        )),
                        creature_class: vec![Reference("GENERAL_POISON".to_owned())],
                        description: Some(
                            "A tiny shelled ocean creature with many long legs.  \
                            It has two large pincers on its front limbs."
                                .to_owned(),
                        ),
                        gets_infections_from_rot: Some(()),
                        gets_wound_infections: Some(()),
                        has_nerves: Some(()),
                        homeotherm: Some(Choose::Choice1(10071)),
                        ligaments: Some((
                            MaterialTokenArg {
                                material: MaterialTypeEnum::LocalCreatureMat(Reference(
                                    "SINEW".to_owned(),
                                )),
                            },
                            200,
                        )),
                        maxage: Some((10, 20)),
                        natural: Some(()),
                        natural_skill: vec![(SkillEnum::Climbing, 15)],
                        no_sleep: Some(()),
                        nobones: Some(()),
                        population_number: Some((250, 500)),
                        pus: Some((
                            MaterialTokenArg {
                                material: MaterialTypeEnum::LocalCreatureMat(Reference(
                                    "PUS".to_owned(),
                                )),
                            },
                            MaterialStateEnum::Liquid,
                        )),
                        tendons: Some((
                            MaterialTokenArg {
                                material: MaterialTypeEnum::LocalCreatureMat(Reference(
                                    "SINEW".to_owned(),
                                )),
                            },
                            200,
                        )),
                        underswim: Some(()),
                        ..Default::default()
                    },
                ],
                ..Default::default()
            }],
        }
    );
}
