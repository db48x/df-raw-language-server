use df_ls_core::{AllowEmpty, ReferenceTo};
use df_ls_structure::*;
use pretty_assertions::assert_eq;

#[test]
fn simple_test() {
    let source = "language_words

    [OBJECT:LANGUAGE]
    
    [WORD:TARNISH]
        [NOUN:tarnish:]
            [FRONT_COMPOUND_NOUN_SING]
            [REAR_COMPOUND_NOUN_SING]
            [THE_NOUN_SING]
            [OF_NOUN_SING]
        [VERB:tarnish:tarnishes:tarnished:tarnished:tarnishing]
            [STANDARD_VERB]

    [SYMBOL:FLOWERY]
        [S_WORD:BERRY]
        [S_WORD:BLOSSOM]
        [S_WORD:BRIDE]
        [S_WORD:BUTTERFLY]
    
    ";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (DFRaw, _) =
        df_ls_syntax_analysis::do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    assert_eq!(diagnostic_list_lexer, vec![]);
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(diagnostic_list, vec![]);
    assert_eq!(
        structure,
        DFRaw {
            header: "language_words".to_owned(),
            object_tokens: vec![ObjectToken {
                language_tokens: vec![
                    LanguageToken::WordToken(WordToken {
                        reference: Some(ReferenceTo::new("TARNISH".to_owned())),
                        nouns: vec![NounToken {
                            words: Some(("tarnish".to_owned(), AllowEmpty::None)),
                            front_compound_noun_sing: Some(()),
                            read_compound_noun_sing: Some(()),
                            the_compound_noun_sing: None,
                            the_noun_sing: Some(()),
                            of_noun_sing: Some(()),
                            front_compound_noun_plur: None,
                            read_compound_noun_plur: None,
                            the_compound_noun_plur: None,
                            the_noun_plur: None,
                            of_noun_plur: None,
                        }],
                        adj: vec![],
                        verb: vec![VerbToken {
                            words: Some((
                                "tarnish".to_owned(),
                                "tarnishes".to_owned(),
                                "tarnished".to_owned(),
                                "tarnished".to_owned(),
                                "tarnishing".to_owned(),
                            )),
                            standard_verb: Some(()),
                            front_compound_adj: None,
                            the_compound_adj: None,
                            rear_compound_adj: None,
                        }],
                        prefix: vec![],
                    }),
                    LanguageToken::SymbolToken(SymbolToken {
                        reference: Some(ReferenceTo::new("FLOWERY".to_owned())),
                        s_word: vec![
                            ReferenceTo::new("BERRY".to_owned()),
                            ReferenceTo::new("BLOSSOM".to_owned()),
                            ReferenceTo::new("BRIDE".to_owned()),
                            ReferenceTo::new("BUTTERFLY".to_owned()),
                        ],
                    }),
                ],
                ..Default::default()
            },],
        }
    );
}
