mod common;
use df_ls_core::{Choose, DFChar, Reference, ReferenceTo};
use df_ls_structure::*;
use pretty_assertions::assert_eq;

#[test]
fn test_interaction() {
    let source = "interaction_header

    [OBJECT:INTERACTION]

    [INTERACTION:EXAMPLE_SECRET]
        [I_SOURCE:SECRET]
            [IS_NAME:the secrets of life and death]
            [IS_SPHERE:DEATH]
            [IS_SECRET_GOAL:IMMORTALITY]
            [IS_SECRET:SUPERNATURAL_LEARNING_POSSIBLE]
            [IS_SECRET:MUNDANE_RESEARCH_POSSIBLE]
            [IS_SECRET:MUNDANE_TEACHING_POSSIBLE]
            [IS_SECRET:MUNDANE_RECORDING_POSSIBLE:objects/text/book_instruction.txt:objects/text/secret_death.txt]
        [I_TARGET:A:CREATURE]
            [IT_LOCATION:CONTEXT_CREATURE]
            [IT_REQUIRES:MORTAL]
            [IT_REQUIRES:CAN_LEARN]
            [IT_REQUIRES:CAN_SPEAK]
        [I_EFFECT:ADD_SYNDROME]
            [IE_TARGET:A]
            [IE_IMMEDIATE]
            [IE_ARENA_NAME:Necromancer]
            [SYNDROME]
                [CE_DISPLAY_TILE:TILE:165:5:0:1:START:0]
                [CE_DISPLAY_NAME:NAME:necromancer:necromancers:necromantic:START:0]
                [CE_PHYS_ATT_CHANGE:STRENGTH:300:1000:TOUGHNESS:300:1000:START:0]
                [CE_MENT_ATT_CHANGE:KINESTHETIC_SENSE:400:0:START:0:END:2505]
                [CE_SWELLING:SEV:50:PROB:100:BP:BY_CATEGORY:MOUTH:ALL:START:0:PEAK:10:END:20:DWF_STRETCH:40]
                [CE_DIZZINESS:SEV:50:PROB:100:SIZE_DILUTES:START:1:PEAK:12:END:24:DWF_STRETCH:40]
                [CE_CHANGE_PERSONALITY:FACET:POLITENESS:-100:START:0]

                [CE_BODY_MAT_INTERACTION:MAT_TOKEN:RESERVED_BLOOD:START:0]
                    [CE:INTERACTION:MAGIC_ANIMALS_2]
                    [CE:SYNDROME_TAG:SYN_INGESTED]
                    [CE:SYNDROME_TAG:SYN_INJECTED]
                [CE_BODY_TRANSFORMATION:START:0:PEAK:0:END:5:ABRUPT:DWF_STRETCH:144]
                    [CE:FORBIDDEN_CREATURE_FLAG:SMALL_RACE]
                    [CE:CREATURE:DWARF:ALL]
                    [CE:CREATURE_CASTE_FLAG:LARGE_PREDATOR]
                    [CE:FORBIDDEN_CREATURE_CASTE_FLAG:CANNOT_BREATHE_AIR]
                [CE_ADD_TAG:NOEXERT:NO_AGING:NO_EAT:NO_DRINK:NO_SLEEP:NO_PHYS_ATT_GAIN:NO_PHYS_ATT_RUST:START:0]
                [CE_CAN_DO_INTERACTION:START:0]
                    [CDI:ADV_NAME:Animate corpse]
                    [CDI:INTERACTION:EXAMPLE_RAISE]
                    [CDI:TARGET:A:LINE_OF_SIGHT]
                    [CDI:TARGET_RANGE:A:10]
                    [CDI:VERB:gesture:gestures:NA]
                    [CDI:TARGET_VERB:shudder and begin to move:shudders and begins to move]
                    [CDI:WAIT_PERIOD:10]
    ";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (DFRaw, _) =
        df_ls_syntax_analysis::do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    assert_eq!(diagnostic_list_lexer, vec![]);
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(diagnostic_list, vec![]);
    assert_eq!(
        structure,
        DFRaw {
            header: "interaction_header".to_owned(),
            object_tokens: vec![ObjectToken {
                interaction_tokens: vec![InteractionToken {
                    reference: Some(ReferenceTo::new("EXAMPLE_SECRET".to_owned())),
                    i_source: vec![ISource {
                        reference: Some(SourceEnum::Secret),
                        is_name: Some("the secrets of life and death".to_owned()),
                        is_sphere: Some(SphereEnum::Death),
                        is_secret_goal: vec![SecretGoalEnum::Immortality],
                        is_secret: vec![
                            (SecretLearnMethodEnum::SupernaturalLearningPossible, None),
                            (SecretLearnMethodEnum::MundaneResearchPossible, None),
                            (SecretLearnMethodEnum::MundaneTeachingPossible, None),
                            (
                                SecretLearnMethodEnum::MundaneRecordingPossible,
                                Some((
                                    "objects/text/book_instruction.txt".to_owned(),
                                    "objects/text/secret_death.txt".to_owned(),
                                )),
                            ),
                        ],
                        ..Default::default()
                    }],
                    i_target: vec![ITarget {
                        reference: Some((Reference("A".to_owned()), TargetTypeEnum::Creature)),
                        it_location: Some(TargetLocationEnum::ContextCreature),
                        it_requires: vec![
                            (vec![TargetPropertyEnum::Mortal],),
                            (vec![TargetPropertyEnum::CanLearn],),
                            (vec![TargetPropertyEnum::CanSpeak],)
                        ],
                        ..Default::default()
                    }],
                    i_effect: vec![IEffect {
                        reference: Some(EffectEnum::AddSyndrome),
                        syndrome: vec![SyndromeToken {
                            syndrome: Some(()),
                            ce_display_name: vec![CeDisplayName {
                                ce_display_name: Some((
                                    NameEnum::Name,
                                    "necromancer".to_owned(),
                                    "necromancers".to_owned(),
                                    "necromantic".to_owned(),
                                    CeXNoSevTokenArg {
                                        start: Some(0),
                                        ..Default::default()
                                    },
                                )),
                                ..Default::default()
                            }],
                            ce_display_tile: vec![CeDisplayTile {
                                ce_display_tile: Some((
                                    TileEnum::Tile,
                                    DFChar('Ñ'),
                                    5,
                                    0,
                                    1,
                                    CeXNoSevTokenArg {
                                        start: Some(0),
                                        ..Default::default()
                                    },
                                )),
                                ..Default::default()
                            }],
                            ce_swelling: vec![CeSwelling {
                                ce_swelling: Some(CeXTokenArg {
                                    sev: Some(50),
                                    prob: Some(100),
                                    bp: vec![(
                                        BpCriteriaTokenArg::ByCategory(Reference(
                                            "MOUTH".to_owned()
                                        )),
                                        Reference("ALL".to_owned())
                                    )],
                                    dwf_stretch: Some(40),
                                    start: Some(0),
                                    end: Some(20),
                                    peak: Some(10),
                                    ..Default::default()
                                }),
                                ..Default::default()
                            }],
                            ce_dizziness: vec![CeDizziness {
                                ce_dizziness: Some(CeXNoTargetTokenArg {
                                    sev: Some(50),
                                    prob: Some(100),
                                    size_dilutes: Some(()),
                                    dwf_stretch: Some(40),
                                    start: Some(1),
                                    end: Some(24),
                                    peak: Some(12),
                                    ..Default::default()
                                }),
                                ..Default::default()
                            }],
                            ce_add_tag: vec![CeAddTag {
                                ce_add_tag: Some(CeTagsTokenArg {
                                    tags: vec![
                                        TargetPropertyEnum::NoExert,
                                        TargetPropertyEnum::NoAging,
                                        TargetPropertyEnum::NoEat,
                                        TargetPropertyEnum::NoDrink,
                                        TargetPropertyEnum::NoSleep,
                                        TargetPropertyEnum::NoPhysAttGain,
                                        TargetPropertyEnum::NoPhysAttRust,
                                    ],
                                    general_cex: Some(CeXNoSevTokenArg {
                                        start: Some(0),
                                        ..Default::default()
                                    }),
                                }),
                                ..Default::default()
                            }],
                            ce_phys_att_change: vec![CePhysAttChange {
                                ce_phys_att_change: Some(CePhysAttChangeTokenArg {
                                    body_attributes: vec![
                                        (BodyAttributeEnum::Strength, 300, 1000),
                                        (BodyAttributeEnum::Toughness, 300, 1000),
                                    ],
                                    general_cex: Some(CeXNoSevTokenArg {
                                        start: Some(0),
                                        ..Default::default()
                                    }),
                                }),
                                ..Default::default()
                            }],
                            ce_ment_att_change: vec![CeMentAttChange {
                                ce_ment_att_change: Some(CeMentAttChangeTokenArg {
                                    soul_attributes: vec![(
                                        SoulAttributeEnum::KinestheticSense,
                                        400,
                                        0
                                    )],
                                    general_cex: Some(CeXNoSevTokenArg {
                                        start: Some(0),
                                        end: Some(2505),
                                        ..Default::default()
                                    }),
                                }),
                                ..Default::default()
                            }],
                            ce_body_transformation: vec![CeBodyTransformation {
                                ce_body_transformation: Some(CeXNoSevTokenArg {
                                    dwf_stretch: Some(144),
                                    abrupt: Some(()),
                                    start: Some(0),
                                    end: Some(5),
                                    peak: Some(0),
                                    ..Default::default()
                                }),
                                ce: vec![
                                    Choose::Choice2(
                                        CeBodyTransformationTokenArg::ForbiddenCreatureFlag(
                                            CreatureFlagEnum::SmallRace
                                        )
                                    ),
                                    Choose::Choice2(CeBodyTransformationTokenArg::Creature((
                                        ReferenceTo::new("DWARF".to_owned()),
                                        Reference("ALL".to_owned()),
                                    ))),
                                    Choose::Choice2(
                                        CeBodyTransformationTokenArg::CreatureCasteFlag(
                                            CasteFlagEnum::LargePredator
                                        )
                                    ),
                                    Choose::Choice2(
                                        CeBodyTransformationTokenArg::ForbiddenCreatureCasteFlag(
                                            CasteFlagEnum::CannotBreatheAir
                                        )
                                    ),
                                ],
                            }],
                            ce_can_do_interaction: vec![CeCanDoInteraction {
                                ce_can_do_interaction: Some(CeXNoSevTokenArg {
                                    start: Some(0),
                                    ..Default::default()
                                }),
                                cdi: vec![
                                    CdiTokenArg::AdvName("Animate corpse".to_owned()),
                                    CdiTokenArg::Interaction(ReferenceTo::new(
                                        "EXAMPLE_RAISE".to_owned()
                                    )),
                                    CdiTokenArg::Target((
                                        Reference("A".to_owned()),
                                        vec![CdiTargetTypeEnum::LineOfSight],
                                    )),
                                    CdiTokenArg::TargetRange((Reference("A".to_owned()), 10)),
                                    CdiTokenArg::Verb((
                                        "gesture".to_owned(),
                                        "gestures".to_owned(),
                                        Choose::Choice2(NotApplicableEnum::NotApplicable)
                                    )),
                                    CdiTokenArg::TargetVerb((
                                        "shudder and begin to move".to_owned(),
                                        "shudders and begins to move".to_owned(),
                                    )),
                                    CdiTokenArg::WaitPeriod(10),
                                ],
                            }],
                            ce_body_mat_interaction: vec![CeBodyMatInteraction {
                                ce_body_mat_interaction: Some((
                                    MatTokenEnum::MatToken,
                                    Choose::Choice1(ReservedBloodEnum::ReservedBlood),
                                    CeXNoSevTokenArg {
                                        start: Some(0),
                                        ..Default::default()
                                    },
                                )),
                                ce: vec![
                                    CeBodyMatInteractionTokenArg::Interaction(ReferenceTo::new(
                                        "MAGIC_ANIMALS_2".to_owned()
                                    )),
                                    CeBodyMatInteractionTokenArg::SyndromeTag(
                                        SynTransmittionMethodEnum::SynIngested
                                    ),
                                    CeBodyMatInteractionTokenArg::SyndromeTag(
                                        SynTransmittionMethodEnum::SynInjected
                                    ),
                                ],
                            }],
                            ce_change_personality: vec![CeChangePersonality {
                                ce_change_personality: Some(CeChangePersonalityTokenArg {
                                    facets: vec![(
                                        FacetEnum::Facet,
                                        PersonalityTraitEnum::Politeness,
                                        -100
                                    )],
                                    general_cex: Some(CeXNoSevTokenArg {
                                        start: Some(0),
                                        ..Default::default()
                                    }),
                                }),
                                ..Default::default()
                            }],
                            ..Default::default()
                        }],
                        ie_arena_name: Some("Necromancer".to_owned()),
                        ie_target: vec![Reference("A".to_owned())],
                        ie_immediate: Some(()),
                        ..Default::default()
                    }],
                    ..Default::default()
                }],
                ..Default::default()
            }],
        }
    );
}
