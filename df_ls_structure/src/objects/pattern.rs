use df_ls_core::{ReferenceTo, Referenceable};
use df_ls_syntax_analysis::TokenDeserialize;
use serde::{Deserialize, Serialize};

#[derive(
    Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Referenceable,
)]
pub struct PatternToken {
    /// Argument 1 of `[COLOR_PATTERN:...]`
    #[token_de(token = "COLOR_PATTERN", on_duplicate_to_parent, primary_token)]
    #[referenceable(self_reference)]
    pub reference: Option<ReferenceTo<Self>>,
    #[token_de(token = "PATTERN")]
    pub pattern: Option<PatternEnum>,
    #[token_de(token = "CP_COLOR")]
    pub cp_color: Vec<ReferenceTo<crate::ColorToken>>,
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
pub enum PatternEnum {
    #[token_de(token = "SPOTS")]
    Spots,
    #[token_de(token = "STRIPES")]
    Stripes,
    #[token_de(token = "MOTTLED")]
    Mottled,
    #[token_de(token = "IRIS_EYE")]
    IrisEye,
    #[token_de(token = "PUPIL_EYE")]
    PupilEye,
}

impl Default for PatternEnum {
    fn default() -> Self {
        Self::Spots
    }
}
