use crate::{MaterialStateEnum, MaterialTokenArg, PluralEnum};
use df_ls_core::{Choose, ReferenceTo, Referenceable};
use df_ls_syntax_analysis::TokenDeserialize;
use serde::{Deserialize, Serialize};

#[derive(
    Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Referenceable,
)]
pub struct TissueToken {
    /// Argument 1 of `[TISSUE_TEMPLATE:...]`
    #[token_de(token = "TISSUE_TEMPLATE", on_duplicate_to_parent, primary_token)]
    #[referenceable(self_reference)]
    pub reference: Option<ReferenceTo<Self>>,
    // region: Tissue Tokens ======================================================================
    /// Name of the tissue.
    ///
    /// Arguments are: `<name>:<plural>`
    ///
    /// Plural can alternatively be `NP` (No Plural) or `STP` (Standard Plural,
    /// adds an 's' on the end).
    #[token_de(token = "TISSUE_NAME")]
    pub name: Option<(String, Choose<PluralEnum, String>)>,
    /// Defines the tissue material.
    #[token_de(token = "TISSUE_MATERIAL")]
    pub material: Option<MaterialTokenArg>,
    /// The relative thickness of the tissue.
    /// A higher thickness is harder to penetrate, but raising a tissue's relative thickness
    /// decreases the thickness of all other tissues.
    #[token_de(token = "RELATIVE_THICKNESS")]
    pub relative_thickness: Option<u32>,
    /// Speed at which the tissue heals itself; lower is faster. Common values are `100` and `1000`
    ///
    /// Omitting the token will result in a tissue that never heals.
    #[token_de(token = "HEALING_RATE")]
    pub healing_rate: Option<u32>,
    /// How many arteries/veins are in the tissue.
    /// Related to how much said tissue bleeds.
    ///
    /// Higher = More bleeding (Which is why the heart has the highest value.)
    /// - Default heart = `10`
    /// - Default skin = `1`
    ///
    /// Also see: `MAJOR_ARTERIES` and `ARTERIES`
    #[token_de(token = "VASCULAR")]
    pub vascular: Option<u8>,
    /// Related to how much pain your character will suffer when said tissue is damaged.
    /// Higher = More pain when damaged (which is why the bone tissue has a much higher value
    /// than other tissues; a broken bone hurts a lot more than a flesh cut).
    /// - Default bones = `50`
    /// - Default skin = `5`
    #[token_de(token = "PAIN_RECEPTORS")]
    pub pain_receptors: Option<u8>,
    /// The thickness of the tissue increases when character strength increases.
    /// Used for muscles in vanilla.
    #[token_de(token = "THICKENS_ON_STRENGTH")]
    pub thickness_on_strength: Option<()>,
    /// Thickness of said tissue increases when the
    /// character eats and doesn't exercise sufficiently.
    /// Used for fat in vanilla.
    #[token_de(token = "THICKENS_ON_ENERGY_STORAGE")]
    pub thickness_on_energy_storage: Option<()>,
    /// The tissue contains arteries.
    /// Edged attacks have the chance to break an artery, increasing blood loss.
    /// Used for muscles in vanilla.
    ///
    /// Also see: `MAJOR_ARTERIES` and `VASCULAR`
    #[token_de(token = "ARTERIES")]
    pub arteries: Option<()>,
    /// Denotes whether or not the tissue will be scarred once healed.
    #[token_de(token = "SCARS")]
    pub scars: Option<()>,
    /// Holds the body part together.
    /// A cut or a fracture will disable the body part it's in.
    #[token_de(token = "STRUCTURAL")]
    pub structural: Option<()>,
    /// Any ligaments or tendons are part of this tissue.
    /// Vulnerable to edged attacks, damage disables the limb.
    ///
    /// Used for bones and chitin in vanilla.
    #[token_de(token = "CONNECTIVE_TISSUE_ANCHOR")]
    pub connective_tissue_anchor: Option<()>,
    /// The tissue will not heal, or heals slower, until it is set by a bone doctor.
    /// Used for bones, shell and chitin in vanilla.
    #[token_de(token = "SETTABLE")]
    pub settable: Option<()>,
    /// The broken tissue can be fixed with a cast or a splint to restore function while it heals.
    /// Used for bones, shell and chitin in vanilla.
    #[token_de(token = "SPLINTABLE")]
    pub splintable: Option<()>,
    /// The tissue performs some sort of special function (e.g. sight, hearing, breathing, etc.)
    /// An organ with such a function will stop working if a sufficient amount of damage is
    /// sustained by its `FUNCTIONAL` tissues. If an organ has no `FUNCTIONAL` tissues,
    /// it will stop working only if it is severed or destroyed entirely by heat or cold.
    #[token_de(token = "FUNCTIONAL")]
    pub functional: Option<()>,
    /// Nervous function - not used.
    /// This token is used in `[OBJECT:BODY]` tokens.
    #[token_de(token = "NERVOUS")]
    pub nervous: Option<()>,
    /// If a creature has no functioning parts with the `THOUGHT` token, it will be unable to move
    /// or breathe; `NO_THOUGHT_CENTER_FOR_MOVEMENT` bypasses this limitation.
    /// Mostly used in `[OBJECT:BODY]`.
    #[token_de(token = "THOUGHT")]
    pub though: Option<()>,
    /// Seems to affect where sensory or motor nerves are located,
    /// and whether damage to this tissue will render a limb useless.
    #[token_de(token = "MUSCULAR")]
    pub muscular: Option<()>,
    /// Used to smell - not used.
    /// This token is used in `[OBJECT:BODY]` tokens.
    #[token_de(token = "SMELL")]
    pub smell: Option<()>,
    /// Used to hearing - not used.
    /// This token is used in `[OBJECT:BODY]` tokens.
    #[token_de(token = "HEAR")]
    pub hear: Option<()>,
    /// Unknown - not used.
    /// Most likely related to flying.
    #[token_de(token = "FLIGHT")]
    pub flight: Option<()>,
    /// Used to breathing - not used.
    /// This token is used in `[OBJECT:BODY]` tokens.
    #[token_de(token = "BREATHE")]
    pub breathe: Option<()>,
    /// Used to seeing - not used.
    /// This token is used in `[OBJECT:BODY]` tokens.
    #[token_de(token = "SIGHT")]
    pub sight: Option<()>,
    /// Holds body parts together.
    /// A body part will not be severed unless all of its component tissues with the
    /// `CONNECTS` tag are severed.
    #[token_de(token = "CONNECTS")]
    pub connects: Option<()>,
    /// Causes tissue to sometimes severely bleed when damaged.
    /// This is independent of its `VASCULAR` value.
    ///
    /// Also see: `ARTERIES`
    #[token_de(token = "MAJOR_ARTERIES")]
    pub major_arteries: Option<()>,
    /// Tissue supplies the creature with heat insulation.
    /// Higher values result in more insulation.
    #[token_de(token = "INSULATION")]
    pub insulation: Option<u32>,
    /// Unknown - not used.
    /// Maybe makes the tissue have no direct purpose?
    ///
    /// Also see: `STYLEABLE`
    #[token_de(token = "COSMETIC")]
    pub cosmetic: Option<()>,
    /// The tissue can be styled as per a tissue style (defined in an entity entry)
    ///
    /// Also see: `COSMETIC`
    #[token_de(token = "STYLEABLE")]
    pub styleable: Option<()>,
    /// The shape of the tissue, like if it is a layer or feathers.
    #[token_de(token = "TISSUE_SHAPE")]
    pub tissue_shape: Option<TissueShapeEnum>,
    /// Tissue is implicitly attached to another tissue and will fall off if that tissue
    /// layer is destroyed. Used for hair and feathers in vanilla, which are subordinate to skin.
    #[token_de(token = "SUBORDINATE_TO_TISSUE")]
    pub subordinate_to_tissue: Option<ReferenceTo<TissueToken>>,
    /// Sets/forces a default material state for the selected tissue.
    #[token_de(token = "TISSUE_MAT_STATE")]
    pub tissue_mat_state: Option<MaterialStateEnum>,
    /// The selected tissue leaks out of the creature when the layers above it are pierced.
    #[token_de(token = "TISSUE_LEAKS")]
    pub tissue_leaks: Option<()>,
    // endregion ==================================================================================
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
/// The shape of the tissue
pub enum TissueShapeEnum {
    /// Regular layer tissue.
    #[token_de(token = "LAYER")]
    Layer,
    /// Can be spun into thread at a farmer's workshop.
    /// Edge attacks will pass right through the tissue.
    #[token_de(token = "STRANDS")]
    Strands,
    /// Edge attacks will pass right through the tissue.
    #[token_de(token = "FEATHERS")]
    Feathers,
    /// Unknown effect.
    #[token_de(token = "SCALES")]
    Scales,
    /// Custom shape. Unknown effect.
    #[token_de(token = "CUSTOM")]
    Custom,
}
impl Default for TissueShapeEnum {
    fn default() -> Self {
        Self::Layer
    }
}
