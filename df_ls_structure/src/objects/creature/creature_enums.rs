use df_ls_core::{Reference, ReferenceTo};
use df_ls_diagnostics::DiagnosticsInfo;
use df_ls_syntax_analysis::{Token, TokenDeserialize, TryFromArgumentGroup};
use serde::{Deserialize, Serialize};

use crate::{CreatureToken, PlantToken};

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
pub enum SingularOrPluralEnum {
    #[token_de(token = "SINGULAR")]
    Singular,
    #[token_de(token = "PLURAL")]
    Plural,
}
impl Default for SingularOrPluralEnum {
    fn default() -> Self {
        Self::Singular
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
pub enum AlertOrPeacefulIntermittentEnum {
    #[token_de(token = "ALERT")]
    Alert,
    #[token_de(token = "PEACEFUL_INTERMITTENT")]
    PeacefulIntermittent,
}
impl Default for AlertOrPeacefulIntermittentEnum {
    fn default() -> Self {
        Self::Alert
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
pub enum VocalizationEnum {
    #[token_de(token = "VOCALIZATION")]
    Vocalization,
}
impl Default for VocalizationEnum {
    fn default() -> Self {
        Self::Vocalization
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
pub enum TissueModifierEnum {
    #[token_de(token = "LENGTH")]
    Length,
    #[token_de(token = "DENSE")]
    Dense,
    #[token_de(token = "HIGH_POSITION")]
    HighPosition,
    #[token_de(token = "CURLY")]
    Curly,
    #[token_de(token = "GREASY")]
    Greasy,
    #[token_de(token = "WRINKLY")]
    Wrinkly,
}
impl Default for TissueModifierEnum {
    fn default() -> Self {
        Self::Length
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
pub enum AppGeneticModelEnum {
    #[token_de(token = "DOMINANT_MORE")]
    DominantMore,
    #[token_de(token = "DOMINANT_LESS")]
    DominantLess,
    #[token_de(token = "MIX")]
    Mix,
}
impl Default for AppGeneticModelEnum {
    fn default() -> Self {
        Self::DominantMore
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
pub enum SecretionTriggerEnum {
    /// Secretion occurs once every 40 ticks in fortress mode, and every tick in adventurer mode.
    #[token_de(token = "CONTINUOUS")]
    Continuous,
    /// Secretion occurs continuously (every 40 ticks in fortress mode, and every tick in adventurer
    /// mode) whilst the creature is at minimum `Tired` following physical exertion. Note that this
    /// cannot occur if the creature has `[NOEXERT]`.
    #[token_de(token = "EXERTION")]
    Exertion,
    /// Secretion occurs continuously (every 40 ticks in fortress mode, and every tick in adventurer
    /// mode) whilst the creature is distressed. Cannot occur in creatures with `[NOEMOTION]`.
    #[token_de(token = "EXTREME_EMOTION")]
    ExtremeEmotion,
}
impl Default for SecretionTriggerEnum {
    fn default() -> Self {
        Self::Continuous
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
pub enum LairCharacteristicEnum {
    #[token_de(token = "HAS_DOORS")]
    HasDoors,
}
impl Default for LairCharacteristicEnum {
    fn default() -> Self {
        Self::HasDoors
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
pub enum LairTypeEnum {
    #[token_de(token = "SIMPLE_BURROW")]
    SimpleBurrow,
    #[token_de(token = "SIMPLE_MOUND")]
    SimpleMound,
    #[token_de(token = "WILDERNESS_LOCATION")]
    WildernessLocation,
    #[token_de(token = "SHRINE")]
    Shrine,
    #[token_de(token = "LABYRINTH")]
    Labyrinth,
}
impl Default for LairTypeEnum {
    fn default() -> Self {
        Self::SimpleBurrow
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
pub enum TestAllEnum {
    #[token_de(token = "TEST_ALL")]
    TestAll,
}
impl Default for TestAllEnum {
    fn default() -> Self {
        Self::TestAll
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
pub enum HabitTypeEnum {
    #[token_de(token = "COLLECT_TROPHIES")]
    CollectTrophies,
    #[token_de(token = "COOK_PEOPLE")]
    CookPeople,
    #[token_de(token = "COOK_VERMIN")]
    CookVermin,
    #[token_de(token = "GRIND_VERMIN")]
    GrindVermin,
    #[token_de(token = "COOK_BLOOD")]
    CookBlood,
    #[token_de(token = "GRIND_BONE_MEAL")]
    GrindBoneMeal,
    #[token_de(token = "EAT_BONE_PORRIDGE")]
    EatBonePorridge,
    #[token_de(token = "USE_ANY_MELEE_WEAPON")]
    UseAnyMeleeWeapon,
    #[token_de(token = "GIANT_NEST")]
    GiantNest,
    #[token_de(token = "COLLECT_WEALTH")]
    CollectWealth,
}
impl Default for HabitTypeEnum {
    fn default() -> Self {
        Self::CollectTrophies
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
pub enum AnyHardStoneEnum {
    #[token_de(token = "ANY_HARD_STONE")]
    AnyHardStone,
}
impl Default for AnyHardStoneEnum {
    fn default() -> Self {
        Self::AnyHardStone
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
pub enum RootEnum {
    #[token_de(token = "ROOT")]
    Root,
}
impl Default for RootEnum {
    fn default() -> Self {
        Self::Root
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
pub enum TimescaleEnum {
    #[token_de(token = "DAILY")]
    Daily,
    #[token_de(token = "YEARLY")]
    Yearly,
}
impl Default for TimescaleEnum {
    fn default() -> Self {
        Self::Daily
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
pub enum NoEndEnum {
    #[token_de(token = "NO_END")]
    NoEnd,
}
impl Default for NoEndEnum {
    fn default() -> Self {
        Self::NoEnd
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
pub enum NormalEnum {
    /// _"Normal just skips the same was as none does. Could be an old compatibility thing? No idea."_
    ///
    /// -- [Toady](http://www.bay12forums.com/smf/index.php?topic=169696.msg8292042#msg8292042)
    #[token_de(token = "NORMAL")]
    Normal,
}
impl Default for NormalEnum {
    fn default() -> Self {
        Self::Normal
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub enum PlantOrCreatureTokenArg {
    Plant(ReferenceTo<PlantToken>),
    Creature(ReferenceTo<CreatureToken>),
}
impl Default for PlantOrCreatureTokenArg {
    fn default() -> Self {
        Self::Plant(ReferenceTo::new(String::default()))
    }
}

// Deserialize a token with following pattern: `[REF:token_args:...]`
df_ls_syntax_analysis::token_deserialize_unary_token!(PlantOrCreatureTokenArg);

impl TryFromArgumentGroup for PlantOrCreatureTokenArg {
    fn try_from_argument_group(
        token: &mut Token,
        source: &str,
        diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        // Safe first argument for error case
        let arg0 = match token.get_current_arg() {
            Ok(arg) => Ok(arg.clone()),
            Err(err) => Err(err),
        };
        let reference_arg0 =
            Reference::try_from_argument_group(token, source, diagnostics, add_diagnostics_on_err)?;
        let plant_or_creature = match reference_arg0.0.as_ref() {
            "PLANT" => {
                let plant_reference = ReferenceTo::<PlantToken>::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                PlantOrCreatureTokenArg::Plant(plant_reference)
            }
            "CREATURE" => {
                let creature_reference = ReferenceTo::<CreatureToken>::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                PlantOrCreatureTokenArg::Creature(creature_reference)
            }
            _ => {
                Self::diagnostics_wrong_enum_type(
                    &arg0?,
                    vec!["PLANT", "CREATURE"],
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                );
                return Err(());
            }
        };
        Ok(plant_or_creature)
    }
}
