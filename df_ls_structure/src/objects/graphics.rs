use crate::CreatureToken;
use df_ls_core::{ReferenceTo, Referenceable};
use df_ls_diagnostics::{hash_map, DMExtraInfo, DiagnosticsInfo};
use df_ls_syntax_analysis::{LoopControl, Token, TokenDeserialize, TokenValue, TreeCursor};
use indexmap::{map::Entry, IndexMap};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
#[allow(clippy::large_enum_variant)]
pub enum GraphicsToken {
    #[token_de(token = "TILE_PAGE")]
    TilePage(TilePageToken),
    #[token_de(token = "CREATURE_GRAPHICS")]
    CreatureGraphics(CreatureGraphicsToken),
}
impl Default for GraphicsToken {
    fn default() -> Self {
        Self::TilePage(TilePageToken::default())
    }
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Referenceable,
)]
pub struct TilePageToken {
    /// Argument 1 of `[TILE_PAGE:...]`
    #[token_de(token = "TILE_PAGE", on_duplicate_to_parent, primary_token)]
    #[referenceable(self_reference)]
    pub reference: Option<ReferenceTo<Self>>,
    /// The relative path to the image.
    /// This file is should be relative to the current file and should include the extension.
    /// Allowed extensions are: `png`, `bmp`, ...
    // TODO Filepath, unix encoded (`/`), relative path from current file and with extension.
    #[token_de(token = "FILE")]
    pub file: Option<String>,
    /// The dimensions or size of a tile (1 character).
    /// For a 32x32 tileset this is `32:32`.
    ///
    /// Arguments: `[TILE_DIM:height:width]`
    #[token_de(token = "TILE_DIM")]
    pub tile_dimensions: Option<(u32, u32)>,
    /// The dimensions or size of the page.
    /// For a 32x32 tileset with 10 rows and 12 columns this is `10:12`.
    /// So in this case the actual image should be: 32*12 and 32*10 = `384x320px`
    ///
    /// Arguments: `[PAGE_DIM:width:height]` (NOTE: flipped compared to `TILE_DIM`)
    #[token_de(token = "PAGE_DIM")]
    pub page_dimensions: Option<(u32, u32)>,
}

type CreatureGraphicsTokenArg = (
    ReferenceTo<TilePageToken>,
    u32,
    u32,
    ColorTypeEnum,
    TextureTypeEnum,
);
type TextureGraphicsTokenArg = (
    ReferenceTo<TilePageToken>,
    u32,
    u32,
    ColorTypeEnum,
    Option<TextureTypeEnum>,
);

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
pub enum TextureTypeEnum {
    #[token_de(token = "DEFAULT")]
    Default,
    #[token_de(token = "ADVENTURER")]
    Adventurer,
    /// Deprecated: Please user`Default`
    // TODO: issue #83
    #[token_de(token = "GUARD")]
    Guard,
    /// Deprecated: Please use `Default`
    // TODO: issue #83
    #[token_de(token = "ROYALGUARD")]
    RoyalGuard,
    /// Deprecated: Please use `Default`
    // TODO: issue #83
    #[token_de(token = "ANIMATED")]
    Animated,
    /// Deprecated: Please use `Default`
    // TODO: issue #83
    #[token_de(token = "GHOST")]
    Ghost,
}
impl Default for TextureTypeEnum {
    fn default() -> Self {
        Self::Default
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
pub enum ColorTypeEnum {
    #[token_de(token = "ADD_COLOR")]
    AddColor,
    #[token_de(token = "AS_IS")]
    AsIs,
}
impl Default for ColorTypeEnum {
    fn default() -> Self {
        Self::AddColor
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, Default, PartialEq)]
pub struct CreatureGraphicsToken {
    /// Argument 1 of `[CREATURE_GRAPHICS:...]`
    // #[token_de(token = "CREATURE_GRAPHICS", on_duplicate_to_parent, primary_token)]
    pub reference: Option<ReferenceTo<CreatureToken>>,

    // All tokens "DEFAULT", "ADVENTURER", "GUARD", "ROYALGUARD", "ANIMATED", "GHOST".
    pub main_texture_tokens: IndexMap<String, Vec<TextureGraphicsTokenArg>>,

    // All others, including professions
    pub other_graphics_tokens: IndexMap<String, Vec<CreatureGraphicsTokenArg>>,
}

// Implement custom because of the large amount of tokens allowed here.
impl TokenDeserialize for CreatureGraphicsToken {
    fn deserialize_general_token(
        cursor: &mut TreeCursor,
        source: &str,
        diagnostics: &mut DiagnosticsInfo,
        mut new_self: Box<Self>,
    ) -> (LoopControl, Box<Self>) {
        let node = cursor.node();
        let primary_token_name = "CREATURE_GRAPHICS";

        let mut token = Token::deserialize_tokens(cursor, source, diagnostics).unwrap();
        // No `Token::consume_token(&mut cursor)?;` here because the token will be consumed when
        // all arguments can be stored inside of object
        let primary_token_filled = new_self.reference.is_some();
        let primary_token_ref: Option<String> = Some(primary_token_name.to_owned());
        // Arg 0
        if token.check_token_arg0(source, diagnostics, true).is_err() {
            return (LoopControl::Break, new_self);
        }
        let arg0 = match token.checked_get_current_arg(source, diagnostics, true) {
            Ok(arg) => arg,
            Err(_) => {
                return (LoopControl::ErrBreak, new_self);
            }
        };
        token.consume_argument();
        // match on first parameter
        match &arg0.value {
            TokenValue::TVReference(value) => {
                log::debug!("Matching {} in {}", value, "CreatureGraphicsToken");
                match value.as_ref() as &str {
                    "CREATURE_GRAPHICS" => {
                        if new_self.reference.is_some() {
                            // on_duplicate_to_parent
                            return (LoopControl::Break, new_self);
                        }

                        let value = TokenDeserialize::deserialize_tokens(
                            cursor,
                            source,
                            diagnostics,
                        );
                        if let Ok(value) = value {
                            new_self.reference = Some(*value);
                        }
                    }
                    // All other tokens
                    // Default: Used when no graphic for a profession is found
                    // 
                    // Arguments: `[DEFAULT:tile_page_ref:tile_page_x:tile_page_y:color_type:(optional)texture_type]`
                    token_ref @ ("DEFAULT"
                    | "ADVENTURER"
                    | "GUARD"
                    | "ROYALGUARD"
                    | "ANIMATED"
                    | "GHOST") => {
                        // primary_token_check
                        if !primary_token_filled {
                            if let Some(primary_token_ref) = primary_token_ref {
                                diagnostics.add_message(
                                    DMExtraInfo {
                                        range: node.get_range(),
                                        message_template_data: hash_map! {
                                            "expected_tokens" => format!("`{}`", primary_token_ref),
                                        },
                                    },
                                    "token_is_missing",
                                );
                            } else {
                                diagnostics.add_message(
                                    DMExtraInfo {
                                        range: node.get_range(),
                                        message_template_data: hash_map! {
                                            "expected_tokens" => "<Unknown>".to_owned(),
                                        },
                                    },
                                    "token_is_missing",
                                );
                            }
                            return (LoopControl::ErrBreak, new_self);
                        }

                        let value: Result<Box<TextureGraphicsTokenArg>, _> = TokenDeserialize::deserialize_tokens(
                            cursor,
                            source,
                            diagnostics,
                        );
                        if let Ok(value) = value {
                            // Add to hashmap
                            // Check if already in hashmap
                            match new_self.main_texture_tokens.entry(token_ref.to_owned()) {
                                Entry::Occupied(mut occupied_entry) => {
                                    // Add value to list of values
                                    let entry = occupied_entry.get_mut();
                                    entry.push(*value);
                                },
                                Entry::Vacant(empty_entry) => {
                                    empty_entry.insert(vec![*value]);
                                }
                            }
                        }
                        // Don't go to next token, we are still matching this token.
                        return (LoopControl::Continue, new_self);
                    }
                    // All other tokens
                    token_ref @ ("STANDARD"
                    | "CHILD"
                    | "BABY"
                    | "DRUNK"
                    | "ADMINISTRATOR"
                    | "ALCHEMIST"
                    | "ANIMAL_CARETAKER"
                    | "ANIMAL_DISSECTOR"
                    | "ANIMAL_TRAINER"
                    | "ARCHITECT"
                    | "ARMORER"
                    | "BEEKEEPER"
                    | "BLACKSMITH"
                    | "BONE_CARVER"
                    | "BONE_SETTER"
                    | "BOWYER"
                    | "BREWER"
                    | "BUTCHER"
                    | "CARPENTER"
                    | "CHEESE_MAKER"
                    | "CLERK"
                    | "CLOTHIER"
                    | "COOK"
                    | "CRAFTSMAN"
                    | "DIAGNOSER"
                    | "DOCTOR"
                    | "DYER"
                    | "ENGINEER"
                    | "ENGRAVER"
                    | "FARMER"
                    | "FISHERMAN"
                    | "FISHERY_WORKER"
                    | "FISH_CLEANER"
                    | "FISH_DISSECTOR"
                    | "FURNACE_OPERATOR"
                    | "GELDER"
                    | "GEM_CUTTER"
                    | "GEM_SETTER"
                    | "GLASSMAKER"
                    | "GLAZER"
                    | "GUILDREP"
                    | "HERBALIST"
                    | "HUNTER"
                    | "JEWELER"
                    | "LEATHERWORKER"
                    | "LYE_MAKER"
                    | "MASON"
                    | "MECHANIC"
                    | "MERCHANT"
                    | "MERCHANTBARON"
                    | "MERCHANT_NOBILITY"
                    | "MERCHANTPRINCE"
                    | "METALCRAFTER"
                    | "METALSMITH"
                    | "MILKER"
                    | "MILLER"
                    | "MINER"
                    | "OUTPOSTLIAISON"
                    | "PLANTER"
                    | "POTASH_MAKER"
                    | "POTTER"
                    | "PRESSER"
                    | "PUMP_OPERATOR"
                    | "RANGER"
                    | "SHEARER"
                    | "SIEGE_ENGINEER"
                    | "SIEGE_OPERATOR"
                    | "SOAP_MAKER"
                    | "SPINNER"
                    | "STONECRAFTER"
                    | "STONEWORKER"
                    | "STRAND_EXTRACTOR"
                    | "SURGEON"
                    | "SUTURER"
                    | "TANNER"
                    | "TAX_COLLECTOR"
                    | "THRESHER"
                    | "TRADER"
                    | "TRAPPER"
                    | "WAX_WORKER"
                    | "WEAPONSMITH"
                    | "WEAVER"
                    | "WOODCRAFTER"
                    | "WOODCUTTER"
                    | "WOODWORKER"
                    | "WOOD_BURNER"
                    //-------- Military ---------
                    | "AXEMAN"
                    | "BLOWGUNMAN"
                    | "BOWMAN"
                    | "CHAMPION"
                    | "CROSSBOWMAN"
                    | "HAMMERMAN"
                    | "LASHER"
                    | "MACEMAN"
                    | "MASTER_AXEMAN"
                    | "MASTER_BLOWGUNMAN"
                    | "MASTER_BOWMAN"
                    | "MASTER_CROSSBOWMAN"
                    | "MASTER_HAMMERMAN"
                    | "MASTER_LASHER"
                    | "MASTER_MACEMAN"
                    | "MASTER_PIKEMAN"
                    | "MASTER_SPEARMAN"
                    | "MASTER_SWORDSMAN"
                    | "MASTER_THIEF"
                    | "MASTER_WRESTLER"
                    | "PIKEMAN"
                    | "RECRUIT"
                    | "SPEARMAN"
                    | "SWORDSMAN"
                    | "THIEF"
                    | "TRAINED_HUNTER"
                    | "TRAINED_WAR"
                    | "WRESTLER"
                    //-------- Positions ---------
                    | "ACOLYTE"
                    | "ADVISOR"
                    | "BARON"
                    | "BARONESS"
                    | "BARONESS_CONSORT"
                    | "BARON_CONSORT"
                    | "BEAST_HUNTER"
                    | "BOOKKEEPER"
                    | "BROKER"
                    | "CAPTAIN"
                    | "CAPTAIN_OF_THE_GUARD"
                    | "CHIEF_MEDICAL_DWARF"
                    | "COUNT"
                    | "COUNTESS"
                    | "COUNTESS_CONSORT"
                    | "COUNT_CONSORT"
                    | "CRIMINAL"
                    | "DIPLOMAT"
                    | "DRUID"
                    | "DUCHESS"
                    | "DUCHESS_CONSORT"
                    | "DUKE"
                    | "DUKE_CONSORT"
                    | "EXECUTIONER"
                    | "EXPEDITION_LEADER"
                    | "GENERAL"
                    | "HAMMERER"
                    | "HIGH_PRIEST"
                    | "HOARDMASTER"
                    | "KING"
                    | "KING_CONSORT"
                    | "LEADER"
                    | "LIEUTENANT"
                    | "MANAGER"
                    | "MAYOR"
                    | "MILITIA_CAPTAIN"
                    | "MILITIA_COMMANDER"
                    | "MONARCH"
                    | "MONARCH_CONSORT"
                    | "MONSTER_SLAYER"
                    | "OUTPOST_LIAISON"
                    | "PRIEST"
                    | "PRISONER"
                    | "QUEEN"
                    | "QUEEN_CONSORT"
                    | "RANGER_CAPTAIN"
                    | "SCOUT"
                    | "SHERIFF"
                    | "SLAVE"
                    | "SNATCHER"
                    | "TREASURER"
                    //-------- Added in DF 0.42 & 0.44 ---------
                    | "ASTRONOMER"
                    | "BARD"
                    | "BOOKBINDER"
                    | "CHEMIST"
                    | "DANCER"
                    | "GEOGRAPHER"
                    | "HISTORIAN"
                    | "MATHEMATICIAN"
                    | "MESSENGER"
                    | "MONK"
                    | "NATURALIST"
                    | "PAPERMAKER"
                    | "PEDDLER"
                    | "PERFORMER"
                    | "PHILOSOPHER"
                    | "PILGRIM"
                    | "POET"
                    | "PROPHET"
                    | "SAGE"
                    | "SCHOLAR"
                    | "SCRIBE"
                    | "TAVERN_KEEPER"
                    //-------- Custom Officials ---------
                    // http://www.bay12forums.com/smf/index.php?topic=175434.msg8082510#msg8082510
                    // Chancellor
                    | "CUSTOM_OFFICIAL_0"
                    // Justiciar
                    | "CUSTOM_OFFICIAL_1"
                    // Treasurer
                    | "CUSTOM_OFFICIAL_2"
                    // Counselor
                    | "CUSTOM_OFFICIAL_3"
                    // Chamberlain
                    | "CUSTOM_OFFICIAL_4"
                    // Master of beasts
                    | "CUSTOM_OFFICIAL_5"
                    // Butler
                    | "CUSTOM_OFFICIAL_6"
                    // Doctor
                    | "CUSTOM_OFFICIAL_7"
                    // Executioner
                    | "CUSTOM_OFFICIAL_8"
                    // Chef
                    | "CUSTOM_OFFICIAL_9"
                    // Housekeeper
                    | "CUSTOM_OFFICIAL_10"
                    //-------- Custom Market Officials ---------
                    // Sewer official
                    | "CUSTOM_MARKET_OFFICIAL_0"
                    // Grain official
                    | "CUSTOM_MARKET_OFFICIAL_1"
                    // Fire official
                    | "CUSTOM_MARKET_OFFICIAL_2"
                    // Judge
                    | "CUSTOM_MARKET_OFFICIAL_3"
                    // Building official
                    | "CUSTOM_MARKET_OFFICIAL_4"
                    // Road official
                    | "CUSTOM_MARKET_OFFICIAL_5"
                    //-------- Other ---------
                    | "DUNGEONMASTER"
                    | "FORCED_ADMINISTRATOR"
                    // TODO: Can be used?
                    | "FORMER_MEMBER"
                    // TODO: Can be used?
                    | "FORMER_MERCENARY"
                    // TODO: Can be used?
                    | "FORMER_PRISONER"
                    // TODO: Can be used?
                    | "FORMER_SLAVE"
                    // TODO: Can be used?
                    | "HANGOUT"
                    // TODO: Can be used?
                    | "HOME"
                    // TODO: Can be used?
                    | "MERCENARY"
                    // TODO: Can be used?
                    | "MEMBER"
                    // TODO: Can be used?
                    | "ENEMY"
                    | "SEAT_OF_POWER"
                    | "SHOPKEEPER"
                    | "WANDERER") => {
                        // primary_token_check
                        if !primary_token_filled {
                            if let Some(primary_token_ref) = primary_token_ref {
                                diagnostics.add_message(
                                    DMExtraInfo {
                                        range: node.get_range(),
                                        message_template_data: hash_map! {
                                            "expected_tokens" => format!("`{}`", primary_token_ref),
                                        },
                                    },
                                    "token_is_missing",
                                );
                            } else {
                                diagnostics.add_message(
                                    DMExtraInfo {
                                        range: node.get_range(),
                                        message_template_data: hash_map! {
                                            "expected_tokens" => "<Unknown>".to_owned(),
                                        },
                                    },
                                    "token_is_missing",
                                );
                            }
                            return (LoopControl::ErrBreak, new_self);
                        }

                        let value: Result<Box<CreatureGraphicsTokenArg>, _> = TokenDeserialize::deserialize_tokens(
                            cursor,
                            source,
                            diagnostics,
                        );
                        if let Ok(value) = value {
                            // Add to hashmap
                            // Check if already in hashmap
                            match new_self.other_graphics_tokens.entry(token_ref.to_owned()) {
                                Entry::Occupied(mut occupied_entry) => {
                                    // Add value to list of values
                                    let entry = occupied_entry.get_mut();
                                    entry.push(*value);
                                },
                                Entry::Vacant(empty_entry) => {
                                    empty_entry.insert(vec![*value]);
                                }
                            }
                        }
                        // Don't go to next token, we are still matching this token.
                        return (LoopControl::Continue, new_self);
                    }
                    _ => {
                        // If nothing changed
                        if *new_self == Self::default() {
                            return (LoopControl::ErrBreak, new_self);
                        }
                        // Go back up to parent
                        return (LoopControl::Break, new_self);
                    }
                }
            }
            _ => {
                // This should already be handled by lexer, but in case of
                diagnostics.add_message(DMExtraInfo {
                    range: node.get_range(),
                    message_template_data: hash_map! {
                        "found_type" => df_ls_syntax_analysis::argument_to_token_name(&arg0.value),
                    },
                }, "first_argument_not_a_ref");
            }
        }
        (LoopControl::DoNothing, new_self)
    }

    fn get_allowed_tokens() -> Option<Vec<TokenValue>> {
        Some(vec![
            TokenValue::TVReference("CREATURE_GRAPHICS".to_owned()),
            TokenValue::TVReference("DEFAULT".to_owned()),
            TokenValue::TVReference("ADVENTURER".to_owned()),
            TokenValue::TVReference("GUARD".to_owned()),
            TokenValue::TVReference("ROYALGUARD".to_owned()),
            TokenValue::TVReference("ANIMATED".to_owned()),
            TokenValue::TVReference("GHOST".to_owned()),
            TokenValue::TVReference("STANDARD".to_owned()),
            TokenValue::TVReference("CHILD".to_owned()),
            TokenValue::TVReference("BABY".to_owned()),
            TokenValue::TVReference("DRUNK".to_owned()),
            TokenValue::TVReference("ADMINISTRATOR".to_owned()),
            TokenValue::TVReference("ALCHEMIST".to_owned()),
            TokenValue::TVReference("ANIMAL_CARETAKER".to_owned()),
            TokenValue::TVReference("ANIMAL_DISSECTOR".to_owned()),
            TokenValue::TVReference("ANIMAL_TRAINER".to_owned()),
            TokenValue::TVReference("ARCHITECT".to_owned()),
            TokenValue::TVReference("ARMORER".to_owned()),
            TokenValue::TVReference("BEEKEEPER".to_owned()),
            TokenValue::TVReference("BLACKSMITH".to_owned()),
            TokenValue::TVReference("BONE_CARVER".to_owned()),
            TokenValue::TVReference("BONE_SETTER".to_owned()),
            TokenValue::TVReference("BOWYER".to_owned()),
            TokenValue::TVReference("BREWER".to_owned()),
            TokenValue::TVReference("BUTCHER".to_owned()),
            TokenValue::TVReference("CARPENTER".to_owned()),
            TokenValue::TVReference("CHEESE_MAKER".to_owned()),
            TokenValue::TVReference("CLERK".to_owned()),
            TokenValue::TVReference("CLOTHIER".to_owned()),
            TokenValue::TVReference("COOK".to_owned()),
            TokenValue::TVReference("CRAFTSMAN".to_owned()),
            TokenValue::TVReference("DIAGNOSER".to_owned()),
            TokenValue::TVReference("DOCTOR".to_owned()),
            TokenValue::TVReference("DYER".to_owned()),
            TokenValue::TVReference("ENGINEER".to_owned()),
            TokenValue::TVReference("ENGRAVER".to_owned()),
            TokenValue::TVReference("FARMER".to_owned()),
            TokenValue::TVReference("FISHERMAN".to_owned()),
            TokenValue::TVReference("FISHERY_WORKER".to_owned()),
            TokenValue::TVReference("FISH_CLEANER".to_owned()),
            TokenValue::TVReference("FISH_DISSECTOR".to_owned()),
            TokenValue::TVReference("FURNACE_OPERATOR".to_owned()),
            TokenValue::TVReference("GELDER".to_owned()),
            TokenValue::TVReference("GEM_CUTTER".to_owned()),
            TokenValue::TVReference("GEM_SETTER".to_owned()),
            TokenValue::TVReference("GLASSMAKER".to_owned()),
            TokenValue::TVReference("GLAZER".to_owned()),
            TokenValue::TVReference("GUILDREP".to_owned()),
            TokenValue::TVReference("HERBALIST".to_owned()),
            TokenValue::TVReference("HUNTER".to_owned()),
            TokenValue::TVReference("JEWELER".to_owned()),
            TokenValue::TVReference("LEATHERWORKER".to_owned()),
            TokenValue::TVReference("LYE_MAKER".to_owned()),
            TokenValue::TVReference("MASON".to_owned()),
            TokenValue::TVReference("MECHANIC".to_owned()),
            TokenValue::TVReference("MERCHANT".to_owned()),
            TokenValue::TVReference("MERCHANTBARON".to_owned()),
            TokenValue::TVReference("MERCHANT_NOBILITY".to_owned()),
            TokenValue::TVReference("MERCHANTPRINCE".to_owned()),
            TokenValue::TVReference("METALCRAFTER".to_owned()),
            TokenValue::TVReference("METALSMITH".to_owned()),
            TokenValue::TVReference("MILKER".to_owned()),
            TokenValue::TVReference("MILLER".to_owned()),
            TokenValue::TVReference("MINER".to_owned()),
            TokenValue::TVReference("OUTPOSTLIAISON".to_owned()),
            TokenValue::TVReference("PLANTER".to_owned()),
            TokenValue::TVReference("POTASH_MAKER".to_owned()),
            TokenValue::TVReference("POTTER".to_owned()),
            TokenValue::TVReference("PRESSER".to_owned()),
            TokenValue::TVReference("PUMP_OPERATOR".to_owned()),
            TokenValue::TVReference("RANGER".to_owned()),
            TokenValue::TVReference("SHEARER".to_owned()),
            TokenValue::TVReference("SIEGE_ENGINEER".to_owned()),
            TokenValue::TVReference("SIEGE_OPERATOR".to_owned()),
            TokenValue::TVReference("SOAP_MAKER".to_owned()),
            TokenValue::TVReference("SPINNER".to_owned()),
            TokenValue::TVReference("STONECRAFTER".to_owned()),
            TokenValue::TVReference("STONEWORKER".to_owned()),
            TokenValue::TVReference("STRAND_EXTRACTOR".to_owned()),
            TokenValue::TVReference("SURGEON".to_owned()),
            TokenValue::TVReference("SUTURER".to_owned()),
            TokenValue::TVReference("TANNER".to_owned()),
            TokenValue::TVReference("TAX_COLLECTOR".to_owned()),
            TokenValue::TVReference("THRESHER".to_owned()),
            TokenValue::TVReference("TRADER".to_owned()),
            TokenValue::TVReference("TRAPPER".to_owned()),
            TokenValue::TVReference("WAX_WORKER".to_owned()),
            TokenValue::TVReference("WEAPONSMITH".to_owned()),
            TokenValue::TVReference("WEAVER".to_owned()),
            TokenValue::TVReference("WOODCRAFTER".to_owned()),
            TokenValue::TVReference("WOODCUTTER".to_owned()),
            TokenValue::TVReference("WOODWORKER".to_owned()),
            TokenValue::TVReference("WOOD_BURNER".to_owned()),
            TokenValue::TVReference("AXEMAN".to_owned()),
            TokenValue::TVReference("BLOWGUNMAN".to_owned()),
            TokenValue::TVReference("BOWMAN".to_owned()),
            TokenValue::TVReference("CHAMPION".to_owned()),
            TokenValue::TVReference("CROSSBOWMAN".to_owned()),
            TokenValue::TVReference("HAMMERMAN".to_owned()),
            TokenValue::TVReference("LASHER".to_owned()),
            TokenValue::TVReference("MACEMAN".to_owned()),
            TokenValue::TVReference("MASTER_AXEMAN".to_owned()),
            TokenValue::TVReference("MASTER_BLOWGUNMAN".to_owned()),
            TokenValue::TVReference("MASTER_BOWMAN".to_owned()),
            TokenValue::TVReference("MASTER_CROSSBOWMAN".to_owned()),
            TokenValue::TVReference("MASTER_HAMMERMAN".to_owned()),
            TokenValue::TVReference("MASTER_LASHER".to_owned()),
            TokenValue::TVReference("MASTER_MACEMAN".to_owned()),
            TokenValue::TVReference("MASTER_PIKEMAN".to_owned()),
            TokenValue::TVReference("MASTER_SPEARMAN".to_owned()),
            TokenValue::TVReference("MASTER_SWORDSMAN".to_owned()),
            TokenValue::TVReference("MASTER_THIEF".to_owned()),
            TokenValue::TVReference("MASTER_WRESTLER".to_owned()),
            TokenValue::TVReference("PIKEMAN".to_owned()),
            TokenValue::TVReference("RECRUIT".to_owned()),
            TokenValue::TVReference("SPEARMAN".to_owned()),
            TokenValue::TVReference("SWORDSMAN".to_owned()),
            TokenValue::TVReference("THIEF".to_owned()),
            TokenValue::TVReference("TRAINED_HUNTER".to_owned()),
            TokenValue::TVReference("TRAINED_WAR".to_owned()),
            TokenValue::TVReference("WRESTLER".to_owned()),
            TokenValue::TVReference("ACOLYTE".to_owned()),
            TokenValue::TVReference("ADVISOR".to_owned()),
            TokenValue::TVReference("BARON".to_owned()),
            TokenValue::TVReference("BARONESS".to_owned()),
            TokenValue::TVReference("BARONESS_CONSORT".to_owned()),
            TokenValue::TVReference("BARON_CONSORT".to_owned()),
            TokenValue::TVReference("BEAST_HUNTER".to_owned()),
            TokenValue::TVReference("BOOKKEEPER".to_owned()),
            TokenValue::TVReference("BROKER".to_owned()),
            TokenValue::TVReference("CAPTAIN".to_owned()),
            TokenValue::TVReference("CAPTAIN_OF_THE_GUARD".to_owned()),
            TokenValue::TVReference("CHIEF_MEDICAL_DWARF".to_owned()),
            TokenValue::TVReference("COUNT".to_owned()),
            TokenValue::TVReference("COUNTESS".to_owned()),
            TokenValue::TVReference("COUNTESS_CONSORT".to_owned()),
            TokenValue::TVReference("COUNT_CONSORT".to_owned()),
            TokenValue::TVReference("CRIMINAL".to_owned()),
            TokenValue::TVReference("DIPLOMAT".to_owned()),
            TokenValue::TVReference("DRUID".to_owned()),
            TokenValue::TVReference("DUCHESS".to_owned()),
            TokenValue::TVReference("DUCHESS_CONSORT".to_owned()),
            TokenValue::TVReference("DUKE".to_owned()),
            TokenValue::TVReference("DUKE_CONSORT".to_owned()),
            TokenValue::TVReference("EXECUTIONER".to_owned()),
            TokenValue::TVReference("EXPEDITION_LEADER".to_owned()),
            TokenValue::TVReference("GENERAL".to_owned()),
            TokenValue::TVReference("HAMMERER".to_owned()),
            TokenValue::TVReference("HIGH_PRIEST".to_owned()),
            TokenValue::TVReference("HOARDMASTER".to_owned()),
            TokenValue::TVReference("KING".to_owned()),
            TokenValue::TVReference("KING_CONSORT".to_owned()),
            TokenValue::TVReference("LEADER".to_owned()),
            TokenValue::TVReference("LIEUTENANT".to_owned()),
            TokenValue::TVReference("MANAGER".to_owned()),
            TokenValue::TVReference("MAYOR".to_owned()),
            TokenValue::TVReference("MILITIA_CAPTAIN".to_owned()),
            TokenValue::TVReference("MILITIA_COMMANDER".to_owned()),
            TokenValue::TVReference("MONARCH".to_owned()),
            TokenValue::TVReference("MONARCH_CONSORT".to_owned()),
            TokenValue::TVReference("MONSTER_SLAYER".to_owned()),
            TokenValue::TVReference("OUTPOST_LIAISON".to_owned()),
            TokenValue::TVReference("PRIEST".to_owned()),
            TokenValue::TVReference("PRISONER".to_owned()),
            TokenValue::TVReference("QUEEN".to_owned()),
            TokenValue::TVReference("QUEEN_CONSORT".to_owned()),
            TokenValue::TVReference("RANGER_CAPTAIN".to_owned()),
            TokenValue::TVReference("SCOUT".to_owned()),
            TokenValue::TVReference("SHERIFF".to_owned()),
            TokenValue::TVReference("SLAVE".to_owned()),
            TokenValue::TVReference("SNATCHER".to_owned()),
            TokenValue::TVReference("TREASURER".to_owned()),
            TokenValue::TVReference("ASTRONOMER".to_owned()),
            TokenValue::TVReference("BARD".to_owned()),
            TokenValue::TVReference("BOOKBINDER".to_owned()),
            TokenValue::TVReference("CHEMIST".to_owned()),
            TokenValue::TVReference("DANCER".to_owned()),
            TokenValue::TVReference("GEOGRAPHER".to_owned()),
            TokenValue::TVReference("HISTORIAN".to_owned()),
            TokenValue::TVReference("MATHEMATICIAN".to_owned()),
            TokenValue::TVReference("MESSENGER".to_owned()),
            TokenValue::TVReference("MONK".to_owned()),
            TokenValue::TVReference("NATURALIST".to_owned()),
            TokenValue::TVReference("PAPERMAKER".to_owned()),
            TokenValue::TVReference("PEDDLER".to_owned()),
            TokenValue::TVReference("PERFORMER".to_owned()),
            TokenValue::TVReference("PHILOSOPHER".to_owned()),
            TokenValue::TVReference("PILGRIM".to_owned()),
            TokenValue::TVReference("POET".to_owned()),
            TokenValue::TVReference("PROPHET".to_owned()),
            TokenValue::TVReference("SAGE".to_owned()),
            TokenValue::TVReference("SCHOLAR".to_owned()),
            TokenValue::TVReference("SCRIBE".to_owned()),
            TokenValue::TVReference("TAVERN_KEEPER".to_owned()),
            TokenValue::TVReference("CUSTOM_OFFICIAL_0".to_owned()),
            TokenValue::TVReference("CUSTOM_OFFICIAL_1".to_owned()),
            TokenValue::TVReference("CUSTOM_OFFICIAL_2".to_owned()),
            TokenValue::TVReference("CUSTOM_OFFICIAL_3".to_owned()),
            TokenValue::TVReference("CUSTOM_OFFICIAL_4".to_owned()),
            TokenValue::TVReference("CUSTOM_OFFICIAL_5".to_owned()),
            TokenValue::TVReference("CUSTOM_OFFICIAL_6".to_owned()),
            TokenValue::TVReference("CUSTOM_OFFICIAL_7".to_owned()),
            TokenValue::TVReference("CUSTOM_OFFICIAL_8".to_owned()),
            TokenValue::TVReference("CUSTOM_OFFICIAL_9".to_owned()),
            TokenValue::TVReference("CUSTOM_OFFICIAL_10".to_owned()),
            TokenValue::TVReference("CUSTOM_MARKET_OFFICIAL_0".to_owned()),
            TokenValue::TVReference("CUSTOM_MARKET_OFFICIAL_1".to_owned()),
            TokenValue::TVReference("CUSTOM_MARKET_OFFICIAL_2".to_owned()),
            TokenValue::TVReference("CUSTOM_MARKET_OFFICIAL_3".to_owned()),
            TokenValue::TVReference("CUSTOM_MARKET_OFFICIAL_4".to_owned()),
            TokenValue::TVReference("CUSTOM_MARKET_OFFICIAL_5".to_owned()),
            TokenValue::TVReference("DUNGEONMASTER".to_owned()),
            TokenValue::TVReference("FORCED_ADMINISTRATOR".to_owned()),
            TokenValue::TVReference("FORMER_MEMBER".to_owned()),
            TokenValue::TVReference("FORMER_MERCENARY".to_owned()),
            TokenValue::TVReference("FORMER_PRISONER".to_owned()),
            TokenValue::TVReference("FORMER_SLAVE".to_owned()),
            TokenValue::TVReference("HANGOUT".to_owned()),
            TokenValue::TVReference("HOME".to_owned()),
            TokenValue::TVReference("MERCENARY".to_owned()),
            TokenValue::TVReference("MEMBER".to_owned()),
            TokenValue::TVReference("ENEMY".to_owned()),
            TokenValue::TVReference("SEAT_OF_POWER".to_owned()),
            TokenValue::TVReference("SHOPKEEPER".to_owned()),
            TokenValue::TVReference("WANDERER".to_owned()),
        ])
    }
}
