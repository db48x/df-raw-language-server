use df_ls_core::{AllowEmpty, ReferenceTo, Referenceable};
use df_ls_syntax_analysis::TokenDeserialize;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
pub enum LanguageToken {
    #[token_de(token = "WORD")]
    WordToken(WordToken),
    #[token_de(token = "SYMBOL")]
    SymbolToken(SymbolToken),
    #[token_de(token = "TRANSLATION")]
    TranslationToken(TranslationToken),
}

impl Default for LanguageToken {
    fn default() -> Self {
        Self::WordToken(WordToken::default())
    }
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Referenceable,
)]
pub struct TranslationToken {
    /// Argument 1 of `TRANSLATION`
    /// The reference for a Translation in other RAW files and tokens
    #[token_de(token = "TRANSLATION", on_duplicate_to_parent, primary_token)]
    #[referenceable(self_reference)]
    pub reference: Option<ReferenceTo<Self>>,
    ///
    #[token_de(token = "T_WORD")]
    pub t_word: Vec<(ReferenceTo<WordToken>, String)>,
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Referenceable,
)]
pub struct SymbolToken {
    /// Argument 1 of `SYMBOL`
    /// The reference for a Symbol in other RAW files and tokens
    #[token_de(token = "SYMBOL", on_duplicate_to_parent, primary_token)]
    #[referenceable(self_reference)]
    pub reference: Option<ReferenceTo<Self>>,
    ///
    #[token_de(token = "S_WORD")]
    pub s_word: Vec<ReferenceTo<WordToken>>,
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Referenceable,
)]
pub struct WordToken {
    /// Argument 1 of `WORD`
    /// The reference for a Word in other RAW files and tokens
    #[token_de(token = "WORD", on_duplicate_to_parent, primary_token)]
    #[referenceable(self_reference)]
    pub reference: Option<ReferenceTo<Self>>,
    ///
    #[token_de(token = "NOUN")]
    pub nouns: Vec<NounToken>,
    ///
    #[token_de(token = "ADJ")]
    pub adj: Vec<AdjToken>,
    ///
    #[token_de(token = "VERB")]
    pub verb: Vec<VerbToken>,
    ///
    #[token_de(token = "PREFIX")]
    pub prefix: Vec<PrefixToken>,
}

#[derive(Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq)]
pub struct NounToken {
    /// Argument 1 of `NOUN`
    ///
    #[token_de(token = "NOUN", on_duplicate_to_parent, primary_token)]
    pub words: Option<(String, AllowEmpty<String>)>,
    ///
    #[token_de(token = "FRONT_COMPOUND_NOUN_SING")]
    pub front_compound_noun_sing: Option<()>,
    ///
    #[token_de(token = "REAR_COMPOUND_NOUN_SING")]
    pub read_compound_noun_sing: Option<()>,
    ///
    #[token_de(token = "THE_COMPOUND_NOUN_SING")]
    pub the_compound_noun_sing: Option<()>,
    ///
    #[token_de(token = "THE_NOUN_SING")]
    pub the_noun_sing: Option<()>,
    ///
    #[token_de(token = "OF_NOUN_SING")]
    pub of_noun_sing: Option<()>,
    ///
    #[token_de(token = "FRONT_COMPOUND_NOUN_PLUR")]
    pub front_compound_noun_plur: Option<()>,
    ///
    #[token_de(token = "REAR_COMPOUND_NOUN_PLUR")]
    pub read_compound_noun_plur: Option<()>,
    ///
    #[token_de(token = "THE_COMPOUND_NOUN_PLUR")]
    pub the_compound_noun_plur: Option<()>,
    ///
    #[token_de(token = "THE_NOUN_PLUR")]
    pub the_noun_plur: Option<()>,
    ///
    #[token_de(token = "OF_NOUN_PLUR")]
    pub of_noun_plur: Option<()>,
}

#[derive(Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq)]
pub struct VerbToken {
    /// Argument 1 of `VERB`
    #[token_de(token = "VERB", on_duplicate_to_parent, primary_token)]
    pub words: Option<(String, String, String, String, String)>,
    ///
    #[token_de(token = "STANDARD_VERB")]
    pub standard_verb: Option<()>,
    ///
    #[token_de(token = "FRONT_COMPOUND_ADJ")]
    pub front_compound_adj: Option<()>,
    ///
    #[token_de(token = "THE_COMPOUND_ADJ")]
    pub the_compound_adj: Option<()>,
    ///
    #[token_de(token = "REAR_COMPOUND_ADJ")]
    pub rear_compound_adj: Option<()>,
}

#[derive(Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq)]
pub struct AdjToken {
    /// Argument 1 of `ADJ`
    #[token_de(token = "ADJ", on_duplicate_to_parent, primary_token)]
    pub words: Option<String>,
    ///
    #[token_de(token = "ADJ_DIST")]
    pub adj_dist: Option<i32>,
    ///
    #[token_de(token = "FRONT_COMPOUND_ADJ")]
    pub front_compound_adj: Option<()>,
    ///
    #[token_de(token = "THE_COMPOUND_ADJ")]
    pub the_compound_adj: Option<()>,
    ///
    #[token_de(token = "REAR_COMPOUND_ADJ")]
    pub rear_compound_adj: Option<()>,
}

#[derive(Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq)]
pub struct PrefixToken {
    /// Argument 1 of `PREFIX`
    #[token_de(token = "PREFIX", on_duplicate_to_parent, primary_token)]
    pub words: Option<String>,
    ///
    #[token_de(token = "FRONT_COMPOUND_PREFIX")]
    pub front_compound_prefix: Option<()>,
    ///
    #[token_de(token = "THE_COMPOUND_PREFIX")]
    pub the_compound_prefix: Option<()>,
}
