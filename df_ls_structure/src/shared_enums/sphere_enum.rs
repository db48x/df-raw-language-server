use df_ls_syntax_analysis::TokenDeserialize;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
pub enum SphereEnum {
    /// Sphere: `AGRICULTURE`
    /// - Friend Spheres: `FOOD`, `FERTILITY`, `RAIN`
    #[token_de(token = "AGRICULTURE")]
    Agriculture,
    /// Sphere: `ANIMALS`
    /// - Friend Spheres: `PLANTS`
    /// - Parent/Child Spheres: `NATURE`
    #[token_de(token = "ANIMALS")]
    Animals,
    /// Sphere: `ART`
    /// - Friend Spheres: `INSPIRATION`, `BEAUTY`
    /// - Parent/Child Spheres: `DANCE`, `MUSIC`, `PAINTING`, `POETRY`, `SONG`
    #[token_de(token = "ART")]
    Art,
    /// Sphere: `BALANCE`
    #[token_de(token = "BALANCE")]
    Balance,
    /// Sphere: `BEAUTY`
    /// - Friend Spheres: `ART`
    /// - Precluded Spheres: `BLIGHT`, `DEFORMITY`, `DISEASE`, `MUCK`  
    #[token_de(token = "BEAUTY")]
    Beauty,
    /// Sphere: `BIRTH`
    /// - Friend Spheres: `CHILDREN`, `CREATION`, `FAMILY`, `MARRIAGE`, `PREGNANCY`, `REBIRTH`, `YOUTH`
    #[token_de(token = "BIRTH")]
    Birth,
    /// Sphere: `BLIGHT`
    /// - Friend Spheres: `DISEASE`, `DEATH`
    /// - Precluded Spheres: `BEAUTY`, `FOOD`, `FERTILITY`, `HEALING`  
    #[token_de(token = "BLIGHT")]
    Blight,
    /// Sphere: `BOUNDARIES`
    /// - Parent/Child Spheres: `COASTS`
    #[token_de(token = "BOUNDARIES")]
    Boundaries,
    /// Sphere: `CAVERNS`
    /// - Friend Spheres: `MOUNTAINS`, `EARTH`
    #[token_de(token = "CAVERNS")]
    Caverns,
    /// Sphere: `CHAOS`
    /// - Friend Spheres: `WAR`
    /// - Precluded Spheres: `DISCIPLINE`, `ORDER`, `LAWS`  
    #[token_de(token = "CHAOS")]
    Chaos,
    /// Sphere: `CHARITY`
    /// - Friend Spheres: `GENEROSITY`, `SACRIFICE`
    /// - Precluded Spheres: `JEALOUSY`  
    #[token_de(token = "CHARITY")]
    Charity,
    /// Sphere: `CHILDREN`
    /// - Friend Spheres: `BIRTH`, `FAMILY`, `YOUTH`, `PREGNANCY`
    #[token_de(token = "CHILDREN")]
    Children,
    /// Sphere: `COASTS`
    /// - Friend Spheres: `LAKES`, `OCEANS`
    /// - Parent/Child Spheres: `BOUNDARIES`
    #[token_de(token = "COASTS")]
    Coasts,
    /// Sphere: `CONSOLATION`
    /// - Precluded Spheres: `MISERY`  
    #[token_de(token = "CONSOLATION")]
    Consolation,
    /// Sphere: `COURAGE`
    /// - Parent/Child Spheres: `VALOR`
    #[token_de(token = "COURAGE")]
    Courage,
    /// Sphere: `CRAFTS`
    /// - Friend Spheres: `CREATION`, `LABOR`, `METALS`
    #[token_de(token = "CRAFTS")]
    Crafts,
    /// Sphere: `CREATION`
    /// - Friend Spheres: `CRAFTS`, `BIRTH`, `PREGNANCY`, `REBIRTH`
    #[token_de(token = "CREATION")]
    Creation,
    /// Sphere: `DANCE`
    /// - Friend Spheres: `FESTIVALS`, `MUSIC`, `REVELRY`
    /// - Parent/Child Spheres: `ART`
    #[token_de(token = "DANCE")]
    Dance,
    /// Sphere: `DARKNESS`
    /// - Friend Spheres: `NIGHT`
    /// - Precluded Spheres: `DAWN`, `DAY`, `LIGHT`, `TWILIGHT`, `SUN`  
    #[token_de(token = "DARKNESS")]
    Darkness,
    /// Sphere: `DAWN`
    /// - Friend Spheres: `SUN`, `TWILIGHT`
    /// - Precluded Spheres: `NIGHT`, `DAY`, `DARKNESS`  
    #[token_de(token = "DAWN")]
    Dawn,
    /// Sphere: `DAY`
    /// - Friend Spheres: `LIGHT`, `SUN`
    /// - Precluded Spheres: `DARKNESS`, `NIGHT`, `DAWN`, `DUSK`, `DREAMS`, `NIGHTMARES`, `TWILIGHT`  
    #[token_de(token = "DAY")]
    Day,
    /// Sphere: `DEATH`
    /// - Friend Spheres: `BLIGHT`, `DISEASE`, `MURDER`, `REBIRTH`, `SUICIDE`, `WAR`
    /// - Precluded Spheres: `HEALING`, `LONGEVITY`, `YOUTH`  
    #[token_de(token = "DEATH")]
    Death,
    /// Sphere: `DEFORMITY`
    /// - Friend Spheres: `DISEASE`
    /// - Precluded Spheres: `BEAUTY`  
    #[token_de(token = "DEFORMITY")]
    Deformity,
    /// Sphere: `DEPRAVITY`
    /// - Friend Spheres: `LUST`
    /// - Precluded Spheres: `LAWS`  
    #[token_de(token = "DEPRAVITY")]
    Depravity,
    /// Sphere: `DISCIPLINE`
    /// - Friend Spheres: `LAWS`, `ORDER`
    /// - Precluded Spheres: `CHAOS`  
    #[token_de(token = "DISCIPLINE")]
    Discipline,
    /// Sphere: `DISEASE`
    /// - Friend Spheres: `BLIGHT`, `DEATH`, `DEFORMITY`
    /// - Precluded Spheres: `BEAUTY`, `HEALING`  
    #[token_de(token = "DISEASE")]
    Disease,
    /// Sphere: `DREAMS`
    /// - Friend Spheres: `NIGHT`, `NIGHTMARES`
    /// - Precluded Spheres: `DAY`  
    #[token_de(token = "DREAMS")]
    Dreams,
    /// Sphere: `DUSK`
    /// - Friend Spheres: `TWILIGHT`
    /// - Precluded Spheres: `NIGHT`, `DAY`  
    #[token_de(token = "DUSK")]
    Dusk,
    /// Sphere: `DUTY`
    /// - Friend Spheres: `ORDER`
    #[token_de(token = "DUTY")]
    Duty,
    /// Sphere: `EARTH`
    /// - Friend Spheres: `CAVERNS`, `MOUNTAINS`, `VOLCANOS`
    /// - Parent/Child Spheres: `METALS`, `MINERALS`, `SALT`
    #[token_de(token = "EARTH")]
    Earth,
    /// Sphere: `FAMILY`
    /// - Friend Spheres: `BIRTH`, `CHILDREN`, `MARRIAGE`, `PREGNANCY`
    #[token_de(token = "FAMILY")]
    Family,
    /// Sphere: `FAME`
    /// - Friend Spheres: `RUMORS`
    /// - Precluded Spheres: `SILENCE`  
    #[token_de(token = "FAME")]
    Fame,
    /// Sphere: `FATE`
    /// - Precluded Spheres: `LUCK`  
    #[token_de(token = "FATE")]
    Fate,
    /// Sphere: `FERTILITY`
    /// - Friend Spheres: `AGRICULTURE`, `FOOD`, `RAIN`
    /// - Precluded Spheres: `BLIGHT`  
    #[token_de(token = "FERTILITY")]
    Fertility,
    /// Sphere: `FESTIVALS`
    /// - Friend Spheres: `DANCE`, `MUSIC`, `REVELRY`, `SONG`
    /// - Precluded Spheres: `MISERY`  
    #[token_de(token = "FESTIVALS")]
    Festivals,
    /// Sphere: `FIRE`
    /// - Friend Spheres: `METALS`, `SUN`, `VOLCANOS`
    /// - Precluded Spheres: `WATER`, `OCEANS`, `LAKES`, `RIVERS`  
    #[token_de(token = "FIRE")]
    Fire,
    /// Sphere: `FISH`
    /// - Friend Spheres: `OCEANS`, `LAKES`, `RIVERS`, `WATER`
    /// - Parent/Child Spheres: `NATURE`, `ANIMALS`
    #[token_de(token = "FISH")]
    Fish,
    /// Sphere: `FISHING`
    /// - Friend Spheres: `FISH`, `HUNTING`
    #[token_de(token = "FISHING")]
    Fishing,
    /// Sphere: `FOOD`
    /// - Friend Spheres: `AGRICULTURE`, `FERTILITY`
    /// - Precluded Spheres: `BLIGHT`  
    #[token_de(token = "FOOD")]
    Food,
    /// Sphere: `FORGIVENESS`
    /// - Friend Spheres: `MERCY`
    /// - Precluded Spheres: `REVENGE`  
    #[token_de(token = "FORGIVENESS")]
    Forgiveness,
    /// Sphere: `FORTRESSES`
    /// - Friend Spheres: `WAR`
    #[token_de(token = "FORTRESSES")]
    Fortresses,
    /// Sphere: `FREEDOM`
    /// - Precluded Spheres: `ORDER`  
    #[token_de(token = "FREEDOM")]
    Freedom,
    /// Sphere: `GAMBLING`
    /// - Friend Spheres: `GAMES`, `LUCK`
    #[token_de(token = "GAMBLING")]
    Gambling,
    /// Sphere: `GAMES`
    /// - Friend Spheres: `GAMBLING`, `LUCK`
    #[token_de(token = "GAMES")]
    Games,
    /// Sphere: `GENEROSITY`
    /// - Friend Spheres: `CHARITY`, `SACRIFICE`
    #[token_de(token = "GENEROSITY")]
    Generosity,
    /// Sphere: `HAPPINESS`
    /// - Friend Spheres: `REVELRY`
    /// - Precluded Spheres: `MISERY`  
    #[token_de(token = "HAPPINESS")]
    Happiness,
    /// Sphere: `HEALING`
    /// - Precluded Spheres: `DISEASE`, `BLIGHT`, `DEATH`  
    #[token_de(token = "HEALING")]
    Healing,
    /// Sphere: `HOSPITALITY`
    #[token_de(token = "HOSPITALITY")]
    Hospitality,
    /// Sphere: `HUNTING`
    /// - Friend Spheres: `FISHING`
    #[token_de(token = "HUNTING")]
    Hunting,
    /// Sphere: `INSPIRATION`
    /// - Friend Spheres: `ART`, `PAINTING`, `POETRY`
    #[token_de(token = "INSPIRATION")]
    Inspiration,
    /// Sphere: `JEALOUSY`
    /// - Precluded Spheres: `CHARITY`  
    #[token_de(token = "JEALOUSY")]
    Jealousy,
    /// Sphere: `JEWELS`
    /// - Friend Spheres: `MINERALS`, `WEALTH`
    #[token_de(token = "JEWELS")]
    Jewels,
    /// Sphere: `JUSTICE`
    /// - Friend Spheres: `LAWS`
    #[token_de(token = "JUSTICE")]
    Justice,
    /// Sphere: `LABOR`
    /// - Friend Spheres: `CRAFTS`
    #[token_de(token = "LABOR")]
    Labor,
    /// Sphere: `LAKES`
    /// - Friend Spheres: `COASTS`, `FISH`, `OCEANS`, `RIVERS`
    /// - Parent/Child Spheres: `WATER`
    /// - Precluded Spheres: `FIRE`  
    #[token_de(token = "LAKES")]
    Lakes,
    /// Sphere: `LAWS`
    /// - Friend Spheres: `DISCIPLINE`, `JUSTICE`, `OATHS`, `ORDER`
    /// - Precluded Spheres: `CHAOS`, `DEPRAVITY`, `MURDER`, `THEFT`  
    #[token_de(token = "LAWS")]
    Laws,
    /// Sphere: `LIES`
    /// - Friend Spheres: `TREACHERY`, `TRICKERY`
    /// - Precluded Spheres: `TRUTH`  
    #[token_de(token = "LIES")]
    Lies,
    /// Sphere: `LIGHT`
    /// - Friend Spheres: `DAY`, `RAINBOWS`, `SUN`
    /// - Precluded Spheres: `DARKNESS`, `TWILIGHT`  
    #[token_de(token = "LIGHT")]
    Light,
    /// Sphere: `LIGHTNING`
    /// - Friend Spheres: `RAIN`, `STORMS`, `THUNDER`
    /// - Parent/Child Spheres: `WEATHER`  
    #[token_de(token = "LIGHTNING")]
    Lightning,
    /// Sphere: `LONGEVITY`
    /// - Friend Spheres: `YOUTH`
    /// - Precluded Spheres: `DEATH`  
    #[token_de(token = "LONGEVITY")]
    Longevity,
    /// Sphere: `LOVE`
    #[token_de(token = "LOVE")]
    Love,
    /// Sphere: `LOYALTY`
    /// - Friend Spheres: `OATHS`
    /// - Precluded Spheres: `TREACHERY`  
    #[token_de(token = "LOYALTY")]
    Loyalty,
    /// Sphere: `LUCK`
    /// - Friend Spheres: `GAMBLING`, `GAMES`
    /// - Precluded Spheres: `FATE`  
    #[token_de(token = "LUCK")]
    Luck,
    /// Sphere: `LUST`
    /// - Friend Spheres: `DEPRAVITY`
    #[token_de(token = "LUST")]
    Lust,
    /// Sphere: `MARRIAGE`
    /// - Friend Spheres: `BIRTH`, `FAMILY`, `OATHS`, `PREGNANCY`
    #[token_de(token = "MARRIAGE")]
    Marriage,
    /// Sphere: `MERCY`
    /// - Friend Spheres: `FORGIVENESS`
    /// - Precluded Spheres: `REVENGE`  
    #[token_de(token = "MERCY")]
    Mercy,
    /// Sphere: `METALS`
    /// - Friend Spheres: `CRAFTS`, `FIRE`, `MINERALS`
    /// - Parent/Child Spheres: `EARTH`
    #[token_de(token = "METALS")]
    Metals,
    /// Sphere: `MINERALS`
    /// - Friend Spheres: `JEWELS`, `METALS`
    /// - Parent/Child Spheres: `EARTH`
    #[token_de(token = "MINERALS")]
    Minerals,
    /// Sphere: `MISERY`
    /// - Friend Spheres: `TORTURE`
    /// - Precluded Spheres: `CONSOLATION`, `FESTIVALS`, `REVELRY`, `HAPPINESS`  
    #[token_de(token = "MISERY")]
    Misery,
    /// Sphere: `MIST`
    #[token_de(token = "MIST")]
    Mist,
    /// Sphere: `MOON`
    /// - Friend Spheres: `NIGHT`, `SKY`
    #[token_de(token = "MOON")]
    Moon,
    /// Sphere: `MOUNTAINS`
    /// - Friend Spheres: `CAVERNS`, `EARTH`, `VOLCANOS`
    #[token_de(token = "MOUNTAINS")]
    Mountains,
    /// Sphere: `MUCK`
    /// - Precluded Spheres: `BEAUTY`  
    #[token_de(token = "MUCK")]
    Muck,
    /// Sphere: `MURDER`
    /// - Friend Spheres: `DEATH`
    /// - Precluded Spheres: `LAWS`  
    #[token_de(token = "MURDER")]
    Murder,
    /// Sphere: `MUSIC`
    /// - Friend Spheres: `DANCE`, `FESTIVALS`, `REVELRY`, `SONG`
    /// - Parent/Child Spheres: `ART`
    /// - Precluded Spheres: `SILENCE`  
    #[token_de(token = "MUSIC")]
    Music,
    /// Sphere: `NATURE`
    /// - Friend Spheres: `RAIN`, `SUN`, `WATER`, `WEATHER`
    /// - Parent/Child Spheres: `ANIMALS`, `FISH`, `PLANTS`, `TREES`
    #[token_de(token = "NATURE")]
    Nature,
    /// Sphere: `NIGHT`
    /// - Friend Spheres: `DARKNESS`, `DREAMS`, `MOON`, `NIGHTMARES`, `STARS`
    /// - Precluded Spheres: `DAY`, `DAWN`, `DUSK`, `TWILIGHT`  
    #[token_de(token = "NIGHT")]
    Night,
    /// Sphere: `NIGHTMARES`
    /// - Friend Spheres: `DREAMS`, `NIGHT`
    /// - Precluded Spheres: `DAY`  
    #[token_de(token = "NIGHTMARES")]
    Nightmares,
    /// Sphere: `OATHS`
    /// - Friend Spheres: `LAWS`, `LOYALTY`, `MARRIAGE`
    /// - Precluded Spheres: `TREACHERY`  
    #[token_de(token = "OATHS")]
    Oaths,
    /// Sphere: `OCEANS`
    /// - Friend Spheres: `COASTS`, `FISH`, `LAKES`, `RIVERS`, `SALT`
    /// - Parent/Child Spheres: `WATER`
    /// - Precluded Spheres: `FIRE`  
    #[token_de(token = "OCEANS")]
    Oceans,
    /// Sphere: `ORDER`
    /// - Friend Spheres: `DISCIPLINE`, `DUTY`, `LAWS`
    /// - Precluded Spheres: `CHAOS`, `FREEDOM`  
    #[token_de(token = "ORDER")]
    Order,
    /// Sphere: `PAINTING`
    /// - Friend Spheres: `INSPIRATION`
    /// - Parent/Child Spheres: `ART`
    #[token_de(token = "PAINTING")]
    Painting,
    /// Sphere: `PEACE`
    #[token_de(token = "PEACE")]
    Peace,
    /// Sphere: `PERSUASION`
    /// - Friend Spheres: `POETRY`, `SPEECH`
    #[token_de(token = "PERSUASION")]
    Persuasion,
    /// Sphere: `PLANTS`
    /// - Friend Spheres: `ANIMALS`, `RAIN`
    /// - Parent/Child Spheres: `NATURE`
    #[token_de(token = "PLANTS")]
    Plants,
    /// Sphere: `POETRY`
    /// - Friend Spheres: `INSPIRATION`, `PERSUASION`, `SONG`, `WRITING`
    /// - Parent/Child Spheres: `ART`
    #[token_de(token = "POETRY")]
    Poetry,
    /// Sphere: `PREGNANCY`
    /// - Friend Spheres: `BIRTH`, `CHILDREN`, `CREATION`, `FAMILY`, `MARRIAGE`
    #[token_de(token = "PREGNANCY")]
    Pregnancy,
    /// Sphere: `RAIN`
    /// - Friend Spheres: `AGRICULTURE`, `FERTILITY`, `LIGHTNING`, `NATURE`, `PLANTS`, `RAINBOWS`, `STORMS`, `THUNDER`, `TREES`
    /// - Parent/Child Spheres: `WATER`, `WEATHER`
    #[token_de(token = "RAIN")]
    Rain,
    /// Sphere: `RAINBOWS`
    /// - Friend Spheres: `LIGHT`, `RAIN`, `SKY`
    /// - Parent/Child Spheres: `WEATHER`
    #[token_de(token = "RAINBOWS")]
    Rainbows,
    /// Sphere: `REBIRTH`
    /// - Friend Spheres: `BIRTH`, `CREATION`, `DEATH`
    #[token_de(token = "REBIRTH")]
    Rebirth,
    /// Sphere: `REVELRY`
    /// - Friend Spheres: `DANCE`, `FESTIVALS`, `HAPPINESS`, `MUSIC`, `SONG`
    /// - Precluded Spheres: `MISERY`  
    #[token_de(token = "REVELRY")]
    Revelry,
    /// Sphere: `REVENGE`
    /// - Precluded Spheres: `FORGIVENESS`, `MERCY`  
    #[token_de(token = "REVENGE")]
    Revenge,
    /// Sphere: `RIVERS`
    /// - Friend Spheres: `FISH`, `LAKES`, `OCEANS`
    /// - Parent/Child Spheres: `WATER`
    /// - Precluded Spheres: `FIRE`  
    #[token_de(token = "RIVERS")]
    Rivers,
    /// Sphere: `RULERSHIP`
    #[token_de(token = "RULERSHIP")]
    Rulership,
    /// Sphere: `RUMORS`
    /// - Friend Spheres: `FAME`
    #[token_de(token = "RUMORS")]
    Rumors,
    /// Sphere: `SACRIFICE`
    /// - Friend Spheres: `CHARITY`, `GENEROSITY`
    /// - Precluded Spheres: `WEALTH`  
    #[token_de(token = "SACRIFICE")]
    Sacrifice,
    /// Sphere: `SALT`
    /// - Friend Spheres: `OCEANS`
    /// - Parent/Child Spheres: `EARTH`
    #[token_de(token = "SALT")]
    Salt,
    /// Sphere: `SCHOLARSHIP`
    /// - Friend Spheres: `WISDOM`, `WRITING`
    #[token_de(token = "SCHOLARSHIP")]
    Scholarship,
    /// Sphere: `SEASONS`
    #[token_de(token = "SEASONS")]
    Seasons,
    /// Sphere: `SILENCE`
    /// - Precluded Spheres: `FAME`, `MUSIC`  
    #[token_de(token = "SILENCE")]
    Silence,
    /// Sphere: `SKY`
    /// - Friend Spheres: `MOON`, `RAINBOWS`, `SUN`, `STARS`, `WEATHER`, `WIND`
    #[token_de(token = "SKY")]
    Sky,
    /// Sphere: `SONG`
    /// - Friend Spheres: `FESTIVALS`, `MUSIC`, `POETRY`, `REVELRY`
    /// - Parent/Child Spheres: `ART`
    #[token_de(token = "SONG")]
    Song,
    /// Sphere: `SPEECH`
    /// - Friend Spheres: `PERSUASION`
    #[token_de(token = "SPEECH")]
    Speech,
    /// Sphere: `STARS`
    /// - Friend Spheres: `NIGHT`, `SKY`
    #[token_de(token = "STARS")]
    Stars,
    /// Sphere: `STORMS`
    /// - Friend Spheres: `LIGHTNING`, `RAIN`, `THUNDER`
    /// - Parent/Child Spheres: `WEATHER`
    #[token_de(token = "STORMS")]
    Storms,
    /// Sphere: `STRENGTH`
    #[token_de(token = "STRENGTH")]
    Strength,
    /// Sphere: `SUICIDE`
    /// - Friend Spheres: `DEATH`
    #[token_de(token = "SUICIDE")]
    Suicide,
    /// Sphere: `SUN`
    /// - Friend Spheres: `DAWN`, `DAY`, `FIRE`, `LIGHT`, `NATURE`, `SKY`
    /// - Precluded Spheres: `DARKNESS`  
    #[token_de(token = "SUN")]
    Sun,
    /// Sphere: `THEFT`
    /// - Precluded Spheres: `LAWS`, `TRADE`  
    #[token_de(token = "THEFT")]
    Theft,
    /// Sphere: `THRALLDOM`
    #[token_de(token = "THRALLDOM")]
    Thralldom,
    /// Sphere: `THUNDER`
    /// - Friend Spheres: `LIGHTNING`, `RAIN`, `STORMS`
    /// - Parent/Child Spheres: `WEATHER`
    #[token_de(token = "THUNDER")]
    Thunder,
    /// Sphere: `TORTURE`
    /// - Friend Spheres: `MISERY`
    #[token_de(token = "TORTURE")]
    Torture,
    /// Sphere: `TRADE`
    /// - Friend Spheres: `WEALTH`
    /// - Precluded Spheres: `THEFT`  
    #[token_de(token = "TRADE")]
    Trade,
    /// Sphere: `TRAVELERS`
    #[token_de(token = "TRAVELERS")]
    Travelers,
    /// Sphere: `TREACHERY`
    /// - Friend Spheres: `LIES`, `TRICKERY`
    /// - Precluded Spheres: `LOYALTY`, `OATHS`  
    #[token_de(token = "TREACHERY")]
    Treachery,
    /// Sphere: `TREES`
    /// - Friend Spheres: `RAIN`
    /// - Parent/Child Spheres: `NATURE`, `PLANTS`
    #[token_de(token = "TREES")]
    Trees,
    /// Sphere: `TRICKERY`
    /// - Friend Spheres: `LIES`, `TREACHERY`
    /// - Precluded Spheres: `TRUTH`  
    #[token_de(token = "TRICKERY")]
    Trickery,
    /// Sphere: `TRUTH`
    /// - Precluded Spheres: `LIES`, `TRICKERY`  
    #[token_de(token = "TRUTH")]
    Truth,
    /// Sphere: `TWILIGHT`
    /// - Friend Spheres: `DAWN`, `DUSK`
    /// - Precluded Spheres: `LIGHT`, `DARKNESS`, `DAY`, `NIGHT`  
    #[token_de(token = "TWILIGHT")]
    Twilight,
    /// Sphere: `VALOR`
    /// - Friend Spheres: `WAR`
    /// - Parent/Child Spheres: `COURAGE`
    #[token_de(token = "VALOR")]
    Valor,
    /// Sphere: `VICTORY`
    /// - Friend Spheres: `WAR`
    #[token_de(token = "VICTORY")]
    Victory,
    /// Sphere: `VOLCANOS`
    /// - Friend Spheres: `EARTH`, `FIRE`, `MOUNTAINS`
    #[token_de(token = "VOLCANOS")]
    Volcanos,
    /// Sphere: `WAR`
    /// - Friend Spheres: `CHAOS`, `DEATH`, `FORTRESSES`, `VALOR`, `VICTORY`
    #[token_de(token = "WAR")]
    War,
    /// Sphere: `WATER`
    /// - Friend Spheres: `FISH`, `NATURE`
    /// - Parent/Child Spheres: `LAKES`, `OCEANS`, `RIVERS`, `RAIN`
    /// - Precluded Spheres: `FIRE`  
    #[token_de(token = "WATER")]
    Water,
    /// Sphere: `WEALTH`
    /// - Friend Spheres: `JEWELS`, `TRADE`
    /// - Precluded Spheres: `SACRIFICE`  
    #[token_de(token = "WEALTH")]
    Wealth,
    /// Sphere: `WEATHER`
    /// - Friend Spheres: `NATURE`, `SKY`
    /// - Parent/Child Spheres: `LIGHTNING`, `RAIN`, `RAINBOWS`, `STORMS`, `THUNDER`, `WIND`
    #[token_de(token = "WEATHER")]
    Weather,
    /// Sphere: `WIND`
    /// - Friend Spheres: `SKY`
    /// - Parent/Child Spheres: `WEATHER`
    #[token_de(token = "WIND")]
    Wind,
    /// Sphere: `WISDOM`
    /// - Friend Spheres: `SCHOLARSHIP`
    #[token_de(token = "WISDOM")]
    Wisdom,
    /// Sphere: `WRITING`
    /// - Friend Spheres: `POETRY`, `SCHOLARSHIP`
    #[token_de(token = "WRITING")]
    Writing,
    /// Sphere: `YOUTH`
    /// - Friend Spheres: `BIRTH`, `CHILDREN`, `LONGEVITY`
    /// - Precluded Spheres: `DEATH`  
    #[token_de(token = "YOUTH")]
    Youth,
}

impl Default for SphereEnum {
    fn default() -> Self {
        Self::Agriculture
    }
}
