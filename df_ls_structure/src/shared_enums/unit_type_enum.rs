use df_ls_syntax_analysis::TokenDeserialize;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
pub enum UnitTypeEnum {
    /// https://dwarffortresswiki.org/index.php/Miner
    #[token_de(token = "MINER")]
    Miner,
    /// https://dwarffortresswiki.org/index.php/Woodworker
    #[token_de(token = "WOODWORKER")]
    Woodworker,
    /// https://dwarffortresswiki.org/index.php/Carpenter
    #[token_de(token = "CARPENTER")]
    Carpenter,
    /// https://dwarffortresswiki.org/index.php/Bowyer
    #[token_de(token = "BOWYER")]
    Bowyer,
    /// https://dwarffortresswiki.org/index.php/Woodcutter
    #[token_de(token = "WOODCUTTER")]
    Woodcutter,
    /// https://dwarffortresswiki.org/index.php/Stoneworker
    #[token_de(token = "STONEWORKER")]
    Stoneworker,
    /// https://dwarffortresswiki.org/index.php/Engraver
    #[token_de(token = "ENGRAVER")]
    Engraver,
    /// https://dwarffortresswiki.org/index.php/Mason
    #[token_de(token = "MASON")]
    Mason,
    /// https://dwarffortresswiki.org/index.php/Ranger
    #[token_de(token = "RANGER")]
    Ranger,
    /// https://dwarffortresswiki.org/index.php/Animal_caretaker
    #[token_de(token = "ANIMAL_CARETAKER")]
    AnimalCaretaker,
    /// https://dwarffortresswiki.org/index.php/Animal_trainer
    #[token_de(token = "ANIMAL_TRAINER")]
    AnimalTrainer,
    /// https://dwarffortresswiki.org/index.php/Hunter
    #[token_de(token = "HUNTER")]
    Hunter,
    /// https://dwarffortresswiki.org/index.php/Trapper
    #[token_de(token = "TRAPPER")]
    Trapper,
    /// https://dwarffortresswiki.org/index.php/Animal_dissector
    #[token_de(token = "ANIMAL_DISSECTOR")]
    AnimalDissector,
    /// https://dwarffortresswiki.org/index.php/Metalsmith
    #[token_de(token = "METALSMITH")]
    Metalsmith,
    /// https://dwarffortresswiki.org/index.php/Furnace_operator
    #[token_de(token = "FURNACE_OPERATOR")]
    FurnaceOperator,
    /// https://dwarffortresswiki.org/index.php/Weaponsmith
    #[token_de(token = "WEAPONSMITH")]
    Weaponsmith,
    /// https://dwarffortresswiki.org/index.php/Armorsmith
    #[token_de(token = "ARMORER")]
    Armorer,
    /// https://dwarffortresswiki.org/index.php/Blacksmith
    #[token_de(token = "BLACKSMITH")]
    Blacksmith,
    /// https://dwarffortresswiki.org/index.php/Metalcrafter
    #[token_de(token = "METALCRAFTER")]
    Metalcrafter,
    /// https://dwarffortresswiki.org/index.php/Jeweler
    #[token_de(token = "JEWELER")]
    Jeweler,
    /// https://dwarffortresswiki.org/index.php/Gem_cutter
    #[token_de(token = "GEM_CUTTER")]
    GemCutter,
    /// https://dwarffortresswiki.org/index.php/Gem_setter
    #[token_de(token = "GEM_SETTER")]
    GemSetter,
    /// https://dwarffortresswiki.org/index.php/Craftsdwarf
    #[token_de(token = "CRAFTSMAN")]
    Craftsman,
    /// https://dwarffortresswiki.org/index.php/Woodcrafter
    #[token_de(token = "WOODCRAFTER")]
    Woodcrafter,
    /// https://dwarffortresswiki.org/index.php/Stonecrafter
    #[token_de(token = "STONECRAFTER")]
    Stonecrafter,
    /// https://dwarffortresswiki.org/index.php/Leatherworker
    #[token_de(token = "LEATHERWORKER")]
    Leatherworker,
    /// https://dwarffortresswiki.org/index.php/Bone_carver
    #[token_de(token = "BONE_CARVER")]
    BoneCarver,
    /// https://dwarffortresswiki.org/index.php/Weaver
    #[token_de(token = "WEAVER")]
    Weaver,
    /// https://dwarffortresswiki.org/index.php/Clothier
    #[token_de(token = "CLOTHIER")]
    Clothier,
    /// https://dwarffortresswiki.org/index.php/Glassmaker
    #[token_de(token = "GLASSMAKER")]
    Glassmaker,
    /// https://dwarffortresswiki.org/index.php/Potter
    #[token_de(token = "POTTER")]
    Potter,
    /// https://dwarffortresswiki.org/index.php/Glazer
    #[token_de(token = "GLAZER")]
    Glazer,
    /// https://dwarffortresswiki.org/index.php/Wax_worker
    #[token_de(token = "WAX_WORKER")]
    WaxWorker,
    /// https://dwarffortresswiki.org/index.php/Strand_extractor
    #[token_de(token = "STRAND_EXTRACTOR")]
    StrandExtractor,
    /// https://dwarffortresswiki.org/index.php/Fishery_worker
    #[token_de(token = "FISHERY_WORKER")]
    FisheryWorker,
    /// https://dwarffortresswiki.org/index.php/Fisherdwarf
    #[token_de(token = "FISHERMAN")]
    Fisherman,
    /// https://dwarffortresswiki.org/index.php/Fish_dissector
    #[token_de(token = "FISH_DISSECTOR")]
    FishDissector,
    /// https://dwarffortresswiki.org/index.php/Fish_cleaner
    #[token_de(token = "FISH_CLEANER")]
    FishCleaner,
    /// https://dwarffortresswiki.org/index.php/Farmer
    #[token_de(token = "FARMER")]
    Farmer,
    /// https://dwarffortresswiki.org/index.php/Cheese_maker
    #[token_de(token = "CHEESE_MAKER")]
    CheeseMaker,
    /// https://dwarffortresswiki.org/index.php/Milker
    #[token_de(token = "MILKER")]
    Milker,
    /// https://dwarffortresswiki.org/index.php/Cook
    #[token_de(token = "COOK")]
    Cook,
    /// https://dwarffortresswiki.org/index.php/Thresher
    #[token_de(token = "THRESHER")]
    Thresher,
    /// https://dwarffortresswiki.org/index.php/Miller
    #[token_de(token = "MILLER")]
    Miller,
    /// https://dwarffortresswiki.org/index.php/Butcher
    #[token_de(token = "BUTCHER")]
    Butcher,
    /// https://dwarffortresswiki.org/index.php/Tanner
    #[token_de(token = "TANNER")]
    Tanner,
    /// https://dwarffortresswiki.org/index.php/Dyer
    #[token_de(token = "DYER")]
    Dyer,
    /// https://dwarffortresswiki.org/index.php/Grower
    #[token_de(token = "PLANTER")]
    Planter,
    /// https://dwarffortresswiki.org/index.php/Herbalist
    #[token_de(token = "HERBALIST")]
    Herbalist,
    /// https://dwarffortresswiki.org/index.php/Brewer
    #[token_de(token = "BREWER")]
    Brewer,
    /// https://dwarffortresswiki.org/index.php/Soaper
    #[token_de(token = "SOAP_MAKER")]
    SoapMaker,
    /// https://dwarffortresswiki.org/index.php/Potash_maker
    #[token_de(token = "POTASH_MAKER")]
    PotashMaker,
    /// https://dwarffortresswiki.org/index.php/Lye_maker
    #[token_de(token = "LYE_MAKER")]
    LyeMaker,
    /// https://dwarffortresswiki.org/index.php/Wood_burner
    #[token_de(token = "WOOD_BURNER")]
    WoodBurner,
    /// https://dwarffortresswiki.org/index.php/Shearer
    #[token_de(token = "SHEARER")]
    Shearer,
    /// https://dwarffortresswiki.org/index.php/Spinner
    #[token_de(token = "SPINNER")]
    Spinner,
    /// https://dwarffortresswiki.org/index.php/Presser
    #[token_de(token = "PRESSER")]
    Presser,
    /// https://dwarffortresswiki.org/index.php/Beekeeper
    #[token_de(token = "BEEKEEPER")]
    Beekeeper,
    /// https://dwarffortresswiki.org/index.php/Engineer
    #[token_de(token = "ENGINEER")]
    Engineer,
    /// https://dwarffortresswiki.org/index.php/Mechanic
    #[token_de(token = "MECHANIC")]
    Mechanic,
    /// https://dwarffortresswiki.org/index.php/Siege_engineer
    #[token_de(token = "SIEGE_ENGINEER")]
    SiegeEngineer,
    /// https://dwarffortresswiki.org/index.php/Siege_operator
    #[token_de(token = "SIEGE_OPERATOR")]
    SiegeOperator,
    /// https://dwarffortresswiki.org/index.php/Pump_operator
    #[token_de(token = "PUMP_OPERATOR")]
    PumpOperator,
    /// https://dwarffortresswiki.org/index.php/Clerk
    #[token_de(token = "CLERK")]
    Clerk,
    /// https://dwarffortresswiki.org/index.php/Administrator
    #[token_de(token = "ADMINISTRATOR")]
    Administrator,
    /// https://dwarffortresswiki.org/index.php/Trader
    #[token_de(token = "TRADER")]
    Trader,
    /// https://dwarffortresswiki.org/index.php/Architect
    #[token_de(token = "ARCHITECT")]
    Architect,
    /// https://dwarffortresswiki.org/index.php/Alchemist
    #[token_de(token = "ALCHEMIST")]
    Alchemist,
    /// https://dwarffortresswiki.org/index.php/Doctor
    #[token_de(token = "DOCTOR")]
    Doctor,
    /// https://dwarffortresswiki.org/index.php/Diagnostician
    #[token_de(token = "DIAGNOSER")]
    Diagnoser,
    /// https://dwarffortresswiki.org/index.php/Bone_doctor
    #[token_de(token = "BONE_SETTER")]
    BoneSetter,
    /// https://dwarffortresswiki.org/index.php/Suturer
    #[token_de(token = "SUTURER")]
    Suturer,
    /// https://dwarffortresswiki.org/index.php/Surgeon
    #[token_de(token = "SURGEON")]
    Surgeon,
    /// https://dwarffortresswiki.org/index.php/Merchant
    #[token_de(token = "MERCHANT")]
    Merchant,
    /// https://dwarffortresswiki.org/index.php/Hammerman
    #[token_de(token = "HAMMERMAN")]
    Hammerman,
    /// https://dwarffortresswiki.org/index.php/Hammer_lord
    #[token_de(token = "MASTER_HAMMERMAN")]
    MasterHammerman,
    /// https://dwarffortresswiki.org/index.php/Spearman
    #[token_de(token = "SPEARMAN")]
    Spearman,
    /// https://dwarffortresswiki.org/index.php/Spearmaster
    #[token_de(token = "MASTER_SPEARMAN")]
    MasterSpearman,
    /// https://dwarffortresswiki.org/index.php/Crossbowman
    #[token_de(token = "CROSSBOWMAN")]
    Crossbowman,
    /// https://dwarffortresswiki.org/index.php/Elite_crossbowman
    #[token_de(token = "MASTER_CROSSBOWMAN")]
    MasterCrossbowman,
    /// https://dwarffortresswiki.org/index.php/Wrestler
    #[token_de(token = "WRESTLER")]
    Wrestler,
    /// https://dwarffortresswiki.org/index.php/Elite_wrestler
    #[token_de(token = "MASTER_WRESTLER")]
    MasterWrestler,
    /// https://dwarffortresswiki.org/index.php/Axeman
    #[token_de(token = "AXEMAN")]
    Axeman,
    /// https://dwarffortresswiki.org/index.php/Axe_lord
    #[token_de(token = "MASTER_AXEMAN")]
    MasterAxeman,
    /// https://dwarffortresswiki.org/index.php/Swordsman
    #[token_de(token = "SWORDSMAN")]
    Swordsman,
    /// https://dwarffortresswiki.org/index.php/Swordmaster
    #[token_de(token = "MASTER_SWORDSMAN")]
    MasterSwordsman,
    /// https://dwarffortresswiki.org/index.php/Maceman
    #[token_de(token = "MACEMAN")]
    Maceman,
    /// https://dwarffortresswiki.org/index.php/Mace_lord
    #[token_de(token = "MASTER_MACEMAN")]
    MasterMaceman,
    /// https://dwarffortresswiki.org/index.php/Pikeman
    #[token_de(token = "PIKEMAN")]
    Pikeman,
    /// https://dwarffortresswiki.org/index.php/Pikemaster
    #[token_de(token = "MASTER_PIKEMAN")]
    MasterPikeman,
    /// https://dwarffortresswiki.org/index.php/Bowman
    #[token_de(token = "BOWMAN")]
    Bowman,
    /// https://dwarffortresswiki.org/index.php/Elite_bowman
    #[token_de(token = "MASTER_BOWMAN")]
    MasterBowman,
    /// https://dwarffortresswiki.org/index.php/Blowgunner
    #[token_de(token = "BLOWGUNMAN")]
    Blowgunman,
    /// https://dwarffortresswiki.org/index.php/Master_blowgunner
    #[token_de(token = "MASTER_BLOWGUNMAN")]
    MasterBlowgunman,
    /// https://dwarffortresswiki.org/index.php/Lasher
    #[token_de(token = "LASHER")]
    Lasher,
    /// https://dwarffortresswiki.org/index.php/Master_lasher
    #[token_de(token = "MASTER_LASHER")]
    MasterLasher,
    /// https://dwarffortresswiki.org/index.php/Recruit
    #[token_de(token = "RECRUIT")]
    Recruit,
    /// https://dwarffortresswiki.org/index.php/Hunting_animal
    #[token_de(token = "TRAINED_HUNTER")]
    TrainedHunter,
    /// https://dwarffortresswiki.org/index.php/War_animal
    #[token_de(token = "TRAINED_WAR")]
    TrainedWar,
    /// https://dwarffortresswiki.org/index.php/Master_thief
    #[token_de(token = "MASTER_THIEF")]
    MasterThief,
    /// https://dwarffortresswiki.org/index.php/Thief
    #[token_de(token = "THIEF")]
    Thief,
    /// https://dwarffortresswiki.org/index.php/Peasant
    #[token_de(token = "STANDARD")]
    Standard,
    /// https://dwarffortresswiki.org/index.php/Child
    #[token_de(token = "CHILD")]
    Child,
    /// https://dwarffortresswiki.org/index.php/Baby
    #[token_de(token = "BABY")]
    Baby,
    /// https://dwarffortresswiki.org/index.php/Drunk
    #[token_de(token = "DRUNK")]
    Drunk,
    /// https://dwarffortresswiki.org/index.php/Monster_slayer
    #[token_de(token = "MONSTER_SLAYER")]
    MonsterSlayer,
    /// https://dwarffortresswiki.org/index.php/Scout
    #[token_de(token = "SCOUT")]
    Scout,
    /// https://dwarffortresswiki.org/index.php/Beast_hunter
    #[token_de(token = "BEAST_HUNTER")]
    BeastHunter,
    /// https://dwarffortresswiki.org/index.php/Snatcher
    #[token_de(token = "SNATCHER")]
    Snatcher,
    /// https://dwarffortresswiki.org/index.php/Mercenary
    #[token_de(token = "MERCENARY")]
    Mercenary,
    /// https://dwarffortresswiki.org/index.php/Gelder
    #[token_de(token = "GELDER")]
    Gelder,
    /// https://dwarffortresswiki.org/index.php/Performer
    #[token_de(token = "PERFORMER")]
    Performer,
    /// https://dwarffortresswiki.org/index.php/Poet
    #[token_de(token = "POET")]
    Poet,
    /// https://dwarffortresswiki.org/index.php/Bard
    #[token_de(token = "BARD")]
    Bard,
    /// https://dwarffortresswiki.org/index.php/Dancer
    #[token_de(token = "DANCER")]
    Dancer,
    /// https://dwarffortresswiki.org/index.php/Sage
    #[token_de(token = "SAGE")]
    Sage,
    /// https://dwarffortresswiki.org/index.php/Scholar
    #[token_de(token = "SCHOLAR")]
    Scholar,
    /// https://dwarffortresswiki.org/index.php/Philosopher
    #[token_de(token = "PHILOSOPHER")]
    Philosopher,
    /// https://dwarffortresswiki.org/index.php/Mathematician
    #[token_de(token = "MATHEMATICIAN")]
    Mathematician,
    /// https://dwarffortresswiki.org/index.php/Historian
    #[token_de(token = "HISTORIAN")]
    Historian,
    /// https://dwarffortresswiki.org/index.php/Astronomer
    #[token_de(token = "ASTRONOMER")]
    Astronomer,
    /// https://dwarffortresswiki.org/index.php/Naturalist
    #[token_de(token = "NATURALIST")]
    Naturalist,
    /// https://dwarffortresswiki.org/index.php/Chemist
    #[token_de(token = "CHEMIST")]
    Chemist,
    /// https://dwarffortresswiki.org/index.php/Geographer
    #[token_de(token = "GEOGRAPHER")]
    Geographer,
    /// https://dwarffortresswiki.org/index.php/Scribe
    #[token_de(token = "SCRIBE")]
    Scribe,
    /// https://dwarffortresswiki.org/index.php/Papermaker
    #[token_de(token = "PAPERMAKER")]
    Papermaker,
    /// https://dwarffortresswiki.org/index.php/Bookbinder
    #[token_de(token = "BOOKBINDER")]
    Bookbinder,
    /// https://dwarffortresswiki.org/index.php/Tavern_keeper
    #[token_de(token = "TAVERN_KEEPER")]
    TavernKeeper,
    /// https://dwarffortresswiki.org/index.php/Criminal
    #[token_de(token = "CRIMINAL")]
    Criminal,
    /// https://dwarffortresswiki.org/index.php/Peddler
    #[token_de(token = "PEDDLER")]
    Peddler,
    /// https://dwarffortresswiki.org/index.php/Prophet
    #[token_de(token = "PROPHET")]
    Prophet,
    /// https://dwarffortresswiki.org/index.php/Pilgrim
    #[token_de(token = "PILGRIM")]
    Pilgrim,
    /// https://dwarffortresswiki.org/index.php/Monk
    #[token_de(token = "MONK")]
    Monk,
    /// https://dwarffortresswiki.org/index.php/Messenger
    #[token_de(token = "MESSENGER")]
    Messenger,
}

impl Default for UnitTypeEnum {
    fn default() -> Self {
        Self::Miner
    }
}
