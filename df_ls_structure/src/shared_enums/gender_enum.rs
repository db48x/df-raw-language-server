use df_ls_syntax_analysis::TokenDeserialize;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
pub enum MaleOrFemaleEnum {
    #[token_de(token = "MALE")]
    Male,
    #[token_de(token = "FEMALE")]
    Female,
}

impl Default for MaleOrFemaleEnum {
    fn default() -> Self {
        Self::Male
    }
}
